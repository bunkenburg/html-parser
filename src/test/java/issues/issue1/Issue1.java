package issues.issue1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import javax.script.ScriptException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import cat.inspiracio.html.AbstractTest;
import cat.inspiracio.html.HTMLDocument;
import cat.inspiracio.html.HTMLDocumentBuilder;
import cat.inspiracio.html.HTMLElement;
import cat.inspiracio.html.HTMLUnknownElement;

public class Issue1 extends AbstractTest{

	private HTMLDocument document;

	
	@Before public void before() throws SAXException, IOException{
		HTMLDocumentBuilder builder=new HTMLDocumentBuilder();
		builder.register(HTMLFultonElement.class);

		String s="<html><body><fulton x='12'>bla</fulton></body></html>";
		InputSource source=input(s);
		document=(HTMLDocument)builder.parse(source);
	}

	@Test public void tRegister(){
		HTMLElement e=document.getElementByTagName("fulton");
		assertNotNull(e);
		
		String x=e.getAttribute("x");
		assertEquals("12", x);
		
		String text=e.getTextContent();
		assertEquals("bla", text);
		
		assertTrue(e instanceof HTMLFultonElement);
	}
	
	@Test public void tcreateElement_class(){
		HTMLFultonElement e=document.createElement(HTMLFultonElement.class);
		assertTrue(e instanceof HTMLFultonElement);
	}
	
	@Test public void tcreateElement_tag(){
		HTMLElement e=document.createElement("fulton");
		assertTrue(e instanceof HTMLFultonElement);
	}
	
	@Test public void tCreateUnknownElement() throws ScriptException{
		String program=""
				+ "d=new Document();"
				+ "e=d.createElement('fulton');"
				+ "e.id='e';"
				+ "d.body.appendChild(e);"
				+ "e";
		Object o=eval(program);//instance of HTMLUnknownElementImp
		assertType(o, HTMLUnknownElement.class);
	}
	
	@Test public void tCreateKnownElement() throws ScriptException{
		String program=""
				+ "d=new Document();"
				
				//registering the element class right inside the javascript program
				+ "i=d.getImplementation();"
				+ "HTMLFultonElement=Packages.issues.issue1.HTMLFultonElement;"
				+ "i.register(HTMLFultonElement.class, 'fulton');"
				
				+ "e=d.createElement('fulton');"
				+ "e.id='e';"
				+ "d.body.appendChild(e);"
				+ "e";
		Object o=eval(program);
		assertType(o, HTMLFultonElement.class);
	}
	
	// helpers ---------------------------
	
	private InputSource input(String s){
		Reader r=new StringReader(s);
		InputSource source=new InputSource(r);
		return source;
	}
}
