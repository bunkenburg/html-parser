package issues.issue1;

import cat.inspiracio.html.HTMLDocumentImp;
import cat.inspiracio.html.HTMLElementImp;

public class HTMLFultonElement extends HTMLElementImp {
	private static final long serialVersionUID = -6629234213401296836L;

	public HTMLFultonElement(HTMLDocumentImp owner){
	    super(owner, "fulton");
	}

	 @Override public String getTagName(){
		 return "fulton";
	 }
}
