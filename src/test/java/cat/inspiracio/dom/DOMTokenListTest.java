/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.dom;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import cat.inspiracio.html.AbstractTest;
import cat.inspiracio.html.HTMLLinkElement;
import cat.inspiracio.html.host.Document;

/** Tests for DOMTokenListImp. */
public class DOMTokenListTest extends AbstractTest{

	private HTMLLinkElement link;
	
	@Override @Before public void setUp(){
		super.setUp();
		Document d=new Document();
		link=d.createElement(HTMLLinkElement.class);
	}
	
	/** getLength() */
	@Test public void tlength(){
		DOMTokenList rel=link.getRelList();
		assertEquals(0, rel.getLength());
		
		link.setRel("bla");
		rel=link.getRelList();
		assertEquals(1, rel.getLength());
		
		link.setRel("bla di blub");
		rel=link.getRelList();
		assertEquals(3, rel.getLength());
	}
	
	/** item(index) */
	@Test public void titem(){
		link.setRel("bla di blub");
		DOMTokenList rel=link.getRelList();
		assertEquals("di", rel.item(1));
	}

	/** contains(value) */
	@Test public void contains(){
		link.setRel("bla di blub");
		DOMTokenList rel=link.getRelList();
		assertEquals(true, rel.contains("di"));
		assertEquals(false, rel.contains("xx"));
	}
	
	/** add(tokens...) */
	@Test public void add(){
		DOMTokenList rel=link.getRelList();
		rel.add("bla", "di", "blub");
		assertEquals("bla di blub", rel.toString());
	}
	
	/** remove(tokens...) */
	@Test public void remove(){
		link.setRel("bla di blub");
		DOMTokenList rel=link.getRelList();
		assertEquals(true, rel.contains("bla"));
		assertEquals(true, rel.contains("di"));
		assertEquals(true, rel.contains("blub"));
		rel.remove("bla", "blub");
		assertEquals(false, rel.contains("bla"));
		assertEquals(true, rel.contains("di"));
		assertEquals(false, rel.contains("blub"));
	}
	
	/** toggle(token) */
	@Test public void toggle(){
		DOMTokenList rel=link.getRelList();
		assertEquals(false, rel.contains("di"));
		rel.toggle("di");
		assertEquals(true, rel.contains("di"));
		rel.toggle("di");
		assertEquals(false, rel.contains("di"));
	}
	
	/** toggle(toggle, force) */
	@Test public void toggleforce(){
		DOMTokenList rel=link.getRelList();
		assertEquals(false, rel.contains("di"));
		rel.toggle("di", true);
		assertEquals(true, rel.contains("di"));
		rel.toggle("di", true);
		assertEquals(true, rel.contains("di"));
		rel.toggle("di", false);
		assertEquals(false, rel.contains("di"));
		rel.toggle("di", false);
		assertEquals(false, rel.contains("di"));
	}
	
}
