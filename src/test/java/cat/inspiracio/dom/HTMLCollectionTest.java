/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.dom;

import static org.junit.Assert.*;

import org.junit.Test;

import cat.inspiracio.html.host.Document;
import cat.inspiracio.html.AbstractTest;
import cat.inspiracio.html.HTMLDivElement;
import cat.inspiracio.html.HTMLElement;

public class HTMLCollectionTest extends AbstractTest {

	@Test public void tfor(){
		HTMLCollectionImp<HTMLElement>collection=new HTMLCollectionImp<>();
		
		Document d=new Document();
		final int N=3;
		HTMLElement[] es=new HTMLElement[3];
		for(int i=0; i<N; i++){
			es[i]=d.createElement(HTMLDivElement.class);
			collection.add(es[i]);
		}
		
		HTMLCollection<HTMLElement>c=collection;

		assertEquals(3, c.getLength());
		
		int i=0;
		for(HTMLElement e : c){
		    assertEquals(es[i], e);
		    i++;
		}
		
	}
}
