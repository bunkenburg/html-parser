/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.dom;

import static org.junit.Assert.*;

import org.junit.Test;

import cat.inspiracio.html.AbstractTest;
import cat.inspiracio.html.HTMLDocument;
import cat.inspiracio.html.HTMLTableCellElement;

public class DOMSettableTokenListTest extends AbstractTest{

	@Test public void t0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<table>"
				+ "			<tr>"
				+ " 			<td id='x'>alpha</td>"
				+ "			</tr>"
				+ "		</table>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLTableCellElement td=(HTMLTableCellElement)d.getElementById("x");
		DOMSettableTokenList tokens=td.getHeaders();
		
		assertEquals(0, tokens.getLength());
		assertEquals("", tokens.getValue());
		assertFalse(tokens.contains("bla"));
		
		tokens.add("bla");
		assertEquals(1, tokens.getLength());
		assertEquals("bla", tokens.getValue());
		assertTrue(tokens.contains("bla"));

		tokens.add("bla");
		assertEquals(1, tokens.getLength());
		assertEquals("bla", tokens.getValue());
		assertTrue(tokens.contains("bla"));

		tokens.add("di");
		assertEquals(2, tokens.getLength());
		assertEquals("bla di", tokens.getValue());
		assertTrue(tokens.contains("bla"));
		assertEquals("bla", tokens.item(0));
		assertEquals("di", tokens.item(1));

		tokens.remove("blub");
		assertEquals(2, tokens.getLength());
		assertEquals("bla di", tokens.getValue());
		assertTrue(tokens.contains("bla"));
		assertEquals("bla", tokens.item(0));
		assertEquals("di", tokens.item(1));

		tokens.remove("bla");
		assertEquals(1, tokens.getLength());
		assertEquals("di", tokens.getValue());
		assertFalse(tokens.contains("bla"));
		assertEquals("di", tokens.item(0));

		tokens.add("bla");
		boolean b=tokens.toggle("di");
		assertFalse(b);
		assertFalse(tokens.contains("di"));
		
		b=tokens.toggle("di");
		assertTrue(b);
		assertTrue(tokens.contains("di"));
		
		//toggle(token, force)
		tokens.add("bla");
		b=tokens.toggle("di", false);
		assertFalse(b);
		assertFalse(tokens.contains("di"));
		
		b=tokens.toggle("di", true);
		assertTrue(b);
		assertTrue(tokens.contains("di"));
		
		tokens.setValue("bla di blub");
		assertEquals("bla di blub", tokens.getValue());
	}

}
