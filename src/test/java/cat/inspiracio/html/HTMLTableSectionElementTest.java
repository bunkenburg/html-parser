/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;

import javax.script.ScriptException;

import org.junit.Test;
import org.w3c.dom.Node;

import cat.inspiracio.dom.HTMLCollection;

public class HTMLTableSectionElementTest extends AbstractTest{

	@Test public void tclone() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "e=document.createElement('tbody');"
				+ "e.id='e';"
				+ "document.body.appendChild(e);"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;

		HTMLElement e=d.getElementById("e");
		Node n=e.cloneNode(true);
		assertType(n, HTMLTableSectionElement.class);
	}

	@Test public void tclone1() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "e=document.createElement('thead');"
				+ "e.id='e';"
				+ "document.body.appendChild(e);"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;

		HTMLElement e=d.getElementById("e");
		Node n=e.cloneNode(true);
		assertType(n, HTMLTableSectionElement.class);
	}

	@Test public void tclone2() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "e=document.createElement('tfoot');"
				+ "e.id='e';"
				+ "document.body.appendChild(e);"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;

		HTMLElement e=d.getElementById("e");
		Node n=e.cloneNode(true);
		assertType(n, HTMLTableSectionElement.class);
	}

	@Test public void tinsertRow0()throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<table id='table'>"
				+ "			<tbody id='section'/>"
				+ "		</table>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLTableSectionElement section=(HTMLTableSectionElement)d.getElementById("section");
		HTMLCollection<HTMLTableRowElement> rows=section.getRows();
		assertEquals(0, rows.getLength());
		
		//insertRow() at end
		HTMLTableRowElement row=section.insertRow();
		rows=section.getRows();
		assertEquals(1, rows.getLength());
		assertEquals(row, rows.item(0));
		
		//insertRow(index) at end
		row=section.insertRow(rows.getLength());
		rows=section.getRows();
		assertEquals(2, rows.getLength());
		assertEquals(row, rows.item(1));
		
		//insertRow(0)
		row=section.insertRow(0);
		rows=section.getRows();
		assertEquals(3, rows.getLength());
		assertEquals(row, rows.item(0));

		//insertRow(-1) at end
		row=section.insertRow(-1);
		rows=section.getRows();
		assertEquals(4, rows.getLength());
		assertEquals(row, rows.item(3));

		//deleteRow
		section.deleteRow(2);
		rows=section.getRows();
		assertEquals(3, rows.getLength());
		assertEquals(row, rows.item(2));
	}
}
