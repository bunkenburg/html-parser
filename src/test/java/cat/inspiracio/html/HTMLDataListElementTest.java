/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import javax.script.ScriptException;

import org.junit.Test;
import org.w3c.dom.Node;

import cat.inspiracio.dom.HTMLCollection;

public class HTMLDataListElementTest extends AbstractTest{

	@Test public void tclone() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "e=document.createElement('datalist');"
				+ "e.id='x';"
				+ "document.body.appendChild(e);"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;
		HTMLElement e=d.getElementById("x");
		Node n=e.cloneNode(true);
		assertType(n, HTMLDataListElement.class);
	}
	
	/** getOptions() */
	@Test public void tGetOptions0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<datalist id='x'>"
				+ "			<option name='a'>alpha</option>"
				+ "			<option name='b'>beta</option>"
				+ "			<option name='c'>gamma</option>"
				+ "		</datalist>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLDataListElement list=(HTMLDataListElement)d.getElementById("x");
		HTMLCollection<HTMLOptionElement> options=list.getOptions();
		
		assertEquals(3, options.getLength());
		assertEquals("alpha", options.item(0).getText());
		assertEquals("beta",  options.item(1).getText());
		assertEquals("gamma", options.item(2).getText());
		assertNull(options.item(3));
	}

}
