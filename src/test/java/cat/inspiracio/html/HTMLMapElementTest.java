/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;

import javax.script.ScriptException;

import org.junit.Test;
import org.w3c.dom.Node;

import cat.inspiracio.dom.HTMLCollection;

public class HTMLMapElementTest extends AbstractTest{

	@Test public void tclone() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "a=document.createElement('link');"
				+ "a.id='x';"
				+ "document.body.appendChild(a);"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;
		HTMLElement e=d.getElementById("x");
		Node n=e.cloneNode(true);
		assertType(n, HTMLLinkElement.class);
	}
	
	@Test public void tgetAreas0()throws Exception{
		String html="<html><body></body></html>";
		HTMLDocument d=parse(html);
		HTMLBodyElement body=d.getBody();
		HTMLMapElement map=d.createElement(HTMLMapElement.class);
		body.appendChild(map);
		HTMLAreaElement area=d.createElement(HTMLAreaElement.class);
		map.appendChild(area);
		
		HTMLCollection<HTMLAreaElement> areas=map.getAreas();
		assertEquals(1, areas.getLength());
		assertEquals(area, areas.item(0));
	}

	@Test public void tgetImages0()throws Exception{
		String html="<html><body></body></html>";
		HTMLDocument d=parse(html);
		HTMLBodyElement body=d.getBody();
		HTMLMapElement map=d.createElement(HTMLMapElement.class);
		map.setId("map");
		map.setName("MAP");
		body.appendChild(map);
		HTMLImageElement image=d.createElement(HTMLImageElement.class);
		image.setUseMap("#map");
		body.appendChild(image);
		HTMLObjectElement object=d.createElement(HTMLObjectElement.class);
		object.setUseMap("#Map");
		body.appendChild(object);
		
		HTMLCollection<HTMLElement> images=map.getImages();
		assertEquals(2, images.getLength());
		assertEquals(image, images.item(0));
		assertEquals(object, images.item(1));
	}
}
