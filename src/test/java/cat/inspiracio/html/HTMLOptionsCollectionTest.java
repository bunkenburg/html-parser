/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.w3c.dom.NodeList;

import cat.inspiracio.dom.HTMLCollection;
import cat.inspiracio.html.host.Document;

public class HTMLOptionsCollectionTest extends AbstractTest{

	@Test public void tNamedItem0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<select id='x'>"
				+ "			<option name='a'>alpha</option>"
				+ "			<option name='b'>beta</option>"
				+ "			<option name='c'>gamma</option>"
				+ "		</select>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLSelectElement select=(HTMLSelectElement) d.getElementById("x");
		HTMLOptionsCollection options=select.getOptions();
		
		assertEquals("alpha", options.namedItem("a").getText());
		assertEquals("beta",  options.namedItem("b").getText());
		assertEquals("gamma", options.namedItem("c").getText());
		assertNull(options.namedItem("document"));
	}
	
	/** get(name) */
	@Test public void tGet0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<select id='x'>"
				+ "			<option name='a'>alpha</option>"
				+ "			<option name='b'>beta</option>"
				+ "			<option name='c'>gamma</option>"
				+ "		</select>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLSelectElement select=(HTMLSelectElement) d.getElementById("x");
		HTMLOptionsCollection options=select.getOptions();
		
		assertEquals("alpha", options.get("a").getText());
		assertEquals("beta",  options.get("b").getText());
		assertEquals("gamma", options.get("c").getText());
		assertNull(options.get("document"));
	}
	
	/** add(group) */
	@Test public void tdAddGroup0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<select id='x'>"
				+ "			<option name='a'>alpha</option>"
				+ "			<option name='b'>beta</option>"
				+ "			<option name='c'>gamma</option>"
				+ "		</select>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLSelectElement select=(HTMLSelectElement)d.getElementById("x");
		HTMLOptGroupElement group=d.createElement(HTMLOptGroupElement.class);
		group.setLabel("label");
		HTMLOptionElement option=d.createElement(HTMLOptionElement.class);
		option.setText("new");
		group.appendChild(option);
		
		select.add(group);
		
		HTMLCollection<HTMLElement> children=select.getChildElements();
		assertEquals(4, children.getLength());
		assertType(children.item(0), HTMLOptionElement.class);
		assertType(children.item(1), HTMLOptionElement.class);
		assertType(children.item(2), HTMLOptionElement.class);
		assertType(children.item(3), HTMLOptGroupElement.class);
		assertEquals(group, children.item(3));
	}

	/** add(group, option) */
	@Test public void tdAddGroup1() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<select id='x'>"
				+ "			<option name='a'>alpha</option>"
				+ "			<option name='b'>beta</option>"
				+ "			<option name='c'>gamma</option>"
				+ "		</select>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLSelectElement select=(HTMLSelectElement)d.getElementById("x");
		HTMLOptGroupElement group=d.createElement(HTMLOptGroupElement.class);
		group.setLabel("label");
		HTMLOptionElement option=d.createElement(HTMLOptionElement.class);
		option.setText("new");
		group.appendChild(option);
		HTMLOptionElement before=select.get(1);//beta
		
		select.add(group, before);
		
		HTMLCollection<HTMLElement> children=select.getChildElements();
		assertEquals(4, children.getLength());
		assertType(children.item(0), HTMLOptionElement.class);
		assertType(children.item(1), HTMLOptGroupElement.class);
		assertType(children.item(2), HTMLOptionElement.class);
		assertType(children.item(3), HTMLOptionElement.class);
		assertEquals(group, children.item(1));
	}
	
	/** add(group, index) */
	@Test public void tdAddGroup2() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<select id='x'>"
				+ "			<option name='a'>alpha</option>"
				+ "			<option name='b'>beta</option>"
				+ "			<option name='c'>gamma</option>"
				+ "		</select>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLSelectElement select=(HTMLSelectElement)d.getElementById("x");
		HTMLOptGroupElement group=d.createElement(HTMLOptGroupElement.class);
		group.setLabel("label");
		HTMLOptionElement option=d.createElement(HTMLOptionElement.class);
		option.setText("new");
		group.appendChild(option);
		
		select.add(group, 1);
		
		HTMLCollection<HTMLElement> children=select.getChildElements();
		assertEquals(4, children.getLength());
		assertType(children.item(0), HTMLOptionElement.class);
		assertType(children.item(1), HTMLOptGroupElement.class);
		assertType(children.item(2), HTMLOptionElement.class);
		assertType(children.item(3), HTMLOptionElement.class);
		assertEquals(group, children.item(1));
	}

	/** Can I insert a group into a select? */
	@Test public void tdAddGroup4() throws Exception{
		Document d=new Document();
		HTMLSelectElement select=d.createElement(HTMLSelectElement.class);
		HTMLOptGroupElement group=d.createElement(HTMLOptGroupElement.class);
		select.insertBefore(group, null);
		assertEquals(select, group.getParentNode());
		NodeList children=select.getChildNodes();
		int length=children.getLength();
		assertEquals(1, length);
	}
	
	/** add(option) */
	@Test public void tdAddOption0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<select id='x'>"
				+ "			<option name='a'>alpha</option>"
				+ "			<option name='b'>beta</option>"
				+ "			<option name='c'>gamma</option>"
				+ "		</select>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLSelectElement select=(HTMLSelectElement)d.getElementById("x");
		HTMLOptionElement option=d.createElement(HTMLOptionElement.class);
		
		select.add(option);
		
		HTMLCollection<HTMLElement> children=select.getChildElements();
		assertEquals(4, children.getLength());
		assertType(children.item(0), HTMLOptionElement.class);
		assertType(children.item(1), HTMLOptionElement.class);
		assertType(children.item(2), HTMLOptionElement.class);
		assertType(children.item(3), HTMLOptionElement.class);
		assertEquals(option, children.item(3));
	}

	/** add(option, option) */
	@Test public void tdAddOption1() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<select id='x'>"
				+ "			<option name='a'>alpha</option>"
				+ "			<option name='b'>beta</option>"
				+ "			<option name='c'>gamma</option>"
				+ "		</select>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLSelectElement select=(HTMLSelectElement)d.getElementById("x");
		HTMLOptionElement option=d.createElement(HTMLOptionElement.class);
		HTMLOptionElement before=select.get(1);
		assertEquals("beta", before.getText());
		
		select.add(option, before);
		
		HTMLCollection<HTMLElement> children=select.getChildElements();
		assertEquals(4, children.getLength());
		assertType(children.item(0), HTMLOptionElement.class);
		assertType(children.item(1), HTMLOptionElement.class);
		assertType(children.item(2), HTMLOptionElement.class);
		assertType(children.item(3), HTMLOptionElement.class);
		assertEquals(option, children.item(1));
	}
	
	/** add(option, index) */
	@Test public void tdAddOption2() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<select id='x'>"
				+ "			<option name='a'>alpha</option>"
				+ "			<option name='b'>beta</option>"
				+ "			<option name='c'>gamma</option>"
				+ "		</select>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLSelectElement select=(HTMLSelectElement)d.getElementById("x");
		HTMLOptionElement option=d.createElement(HTMLOptionElement.class);
		
		select.add(option, 1);
		
		HTMLCollection<HTMLElement> children=select.getChildElements();
		assertEquals(4, children.getLength());
		assertType(children.item(0), HTMLOptionElement.class);
		assertType(children.item(1), HTMLOptionElement.class);
		assertType(children.item(2), HTMLOptionElement.class);
		assertType(children.item(3), HTMLOptionElement.class);
		assertEquals(option, children.item(1));
	}
	
	
}
