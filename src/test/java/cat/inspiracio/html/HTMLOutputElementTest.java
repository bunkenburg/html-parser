/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;

import javax.script.ScriptException;

import org.junit.Test;
import org.w3c.dom.Node;

public class HTMLOutputElementTest extends AbstractTest{

	@Test public void tclone() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "e=document.createElement('output');"
				+ "e.id='e';"
				+ "document.body.appendChild(e);"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;

		HTMLElement e=d.getElementById("e");
		Node n=e.cloneNode(true);
		assertType(n, HTMLOutputElement.class);
	}
	
	@Test public void tGetForm0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<form id='form'>"
				+ "			<output id='a'>alpha</output>"
				+ "		</datalist>"
				+ "	</body>"
				+ "	<output id='b' form='form'>alpha</output>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLFormElement form=(HTMLFormElement) d.getElementById("form");
		ReassociateableElement a=(ReassociateableElement) d.getElementById("a");
		ReassociateableElement b=(ReassociateableElement) d.getElementById("b");
		assertEquals(form, a.getForm());
		assertEquals(form, b.getForm());
	}

}
