/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import javax.script.ScriptException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

public class HTMLAreaElementTest extends URLUtilsTest{

    @Override
	@Before public void setup(){
        super.setup();
        utils=document.createElement(HTMLAreaElement.class);
    }
    
    @Override
	@After public void teardown(){super.teardown();}

    @Test public void tclone() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "a=document.createElement('area');"
				+ "a.id='x';"
				+ "document.body.appendChild(a);"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;
		HTMLAreaElement a=(HTMLAreaElement)d.getElementById("x");
		Node n=a.cloneNode(true);
		assertType(n, HTMLAreaElement.class);
	}

    @Test public void tType(){
        assertType(utils, HTMLAreaElement.class);
    }

    @Override @Test public void tHref() {super.tHref();}
    @Override @Test public void tOrigin() {super.tOrigin();}
    @Override @Test public void tProtocol() {super.tProtocol();}
    @Override @Test public void tUsername() {super.tUsername();}
    @Override @Test public void tPassword() {super.tPassword();}
    @Override @Test public void tHost() {super.tHost();}
    @Override @Test public void tHostname() {super.tHostname();}
    @Override @Test public void tPort() {super.tPort();}
    @Override @Test public void tPathname() {super.tPathname();}
    @Override @Test public void tSearch() {super.tSearch();}
    @Override @Test public void tSearchParams(){super.tSearchParams();}
    @Override @Test public void tHash(){super.tHash();}
    @Override @Test public void threfInitial() {super.threfInitial();}
    @Override @Test public void threfSetGet() {super.threfSetGet();}

}
