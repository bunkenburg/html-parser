/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static java.lang.Double.isNaN;
import static java.util.Calendar.MILLISECOND;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.script.*;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class AbstractTest {

	// state --------------------------------------------------
	
	/** Imports all the host objects: Audio, Document, Image, Option.
	 * 
	 * How best import a couple of classes, so that it works in Rhino and in Nashorn?
	 * 
	 * Rhino:
	 * "importClass" is good for Rhino in Java 7, but not for Nashorn in Java 8.
	 * Nashorn:
     * load(\"nashorn:mozilla_compat.js\");importClass(...)"
     * In Mozilla's Rhino: "load not defined".
     * In Java 8 fine.
	 * Study http://www.oracle.com/technetwork/articles/java/jf14-nashorn-2126515.html
	 * load(path) in Rhino and in Nashorn loads a javascript file.
     * The file nashorn:mozilla_compat.js defines importClass and importPackage,
     * as they were in Rhino. But discouraged, better:
     * var CollectionsAndFiles = new JavaImporter(
     *     java.util,
     *     java.io,
     *     java.nio);
     * with (CollectionsAndFiles) {
     *     var files = new LinkedHashSet();
     *     files.add(new File("Plop"));
     *     files.add(new File("Foo"));
     *     files.add(new File("w00t.js"));
     * }
     * I don't want to do that, because of the coda. 
     * 
     * I'll just make a new variable in global space and assign the class, that 
     * works in Rhino and in Nashorn.
     * */
	private String prelude=""
			+ "Audio=Packages.cat.inspiracio.html.host.Audio;"
            + "Document=Packages.cat.inspiracio.html.host.Document;"
            + "Image=Packages.cat.inspiracio.html.host.Image;"
            + "Option=Packages.cat.inspiracio.html.host.Option;"
			+ "";

	// life cycle --------------------------------------------

	protected AbstractTest(){}
	@Before public void setUp(){}
	@After public void tearDown(){}
	
	// inherited methods -------------------------------------
	
	/** Asserts that the object is an instance of that class. */
	@SuppressWarnings("unchecked")
	protected <T> T assertType(Object o, Class<T> k){
		Assert.assertNotNull(k);
		Assert.assertNotNull(o);
		assertTrue(o + " : " + o.getClass(), k.isInstance(o));
		return (T)o;
	}

	/** Executes some javascript and returns the result.
	 * 
	 * @param script Need not import the host objects.
	 * 
	 * @throws ScriptException */
	protected Object eval(String script) throws ScriptException {
		ScriptEngineManager mgr = new ScriptEngineManager();

		//In Java 8, getting JavaScript engine emits a warning:
		//Warning: Nashorn engine is planned to be removed from a future JDK release
		//To switch it off:
		//  -Dnashorn.args="--no-deprecation-warning"
		System.setProperty("nashorn.args", "--no-deprecation-warning");
		ScriptEngine engine = mgr.getEngineByName("JavaScript");

		Bindings n=new SimpleBindings();
		script=prelude + ";\n" + script;
		return engine.eval(script, n);
	}

	protected InputStream getInputStream(String path){
		Class<?>c=getClass();
		ClassLoader loader=c.getClassLoader();
		return loader.getResourceAsStream(path);
	}
	
	/** Parses a document from a path.
	 * 
	 * @param path Path in src/test/resources/
	 * */
	protected HTMLDocumentImp path(String path) throws SAXException, IOException{
		try(InputStream in=getInputStream(path);){
			HTMLBuilder builder=new HTMLDocumentBuilder();
			HTMLDocumentImp document=(HTMLDocumentImp)builder.parse(in);
			document.setLocation(path);//It's what we've got.
			return document;
		}
	}
	
	/** Parses a document from a html.
	 * 
	 * @param html The html as String
	 * */
	protected HTMLDocument parse(String html) throws SAXException, IOException{
		HTMLDocumentBuilder builder=new HTMLDocumentBuilder();
		InputSource source=new InputSource(new StringReader(html));
		return builder.parse(source);
	}
	
	protected HTMLDocument newDocument(){
		HTMLDocumentBuilder builder=new HTMLDocumentBuilder();
		return builder.newDocument();
	}
	
	/** Formats a node.
	 * Returns the string and also writes it to System.out. */
	protected String format(Node n) throws IOException{
		try{
			Writer writer=new StringWriter();
			DocumentWriter sender=new DocumentWriter(writer);
			sender.node(n);
			return writer.toString();
		}
		catch(IOException e){
			throw e;
		}
		catch(Exception e){
			throw new RuntimeException();
		}
	}

	protected void say(Object o){System.out.println(o);}
	
	protected void assertEmpty(String s){
		assertTrue(s!=null);
		assertEquals(0, s.length());
	}
	
	protected void assertNaN(double d){
		assertTrue(isNaN(d));
	}
	
	protected HTMLElement getSelectById(Document d, String id){
		Element e=d.getElementById(id);
		assertType(e, HTMLElement.class);
		HTMLElement select=(HTMLElement)e;
		return select;
	}

	protected Date date(int year, int month, int day){
		Calendar calendar=Calendar.getInstance();
		TimeZone zone=TimeZone.getTimeZone("GMT");
		calendar.setTimeZone(zone);
		calendar.set(year, month-1, day, 0, 0, 0);
		calendar.set(MILLISECOND, 0);
		return calendar.getTime();
	}
	
	protected Date time(int hours, int minutes){
		return time(hours, minutes, 0, 0);
	}
	
	protected Date time(int hours, int minutes, int seconds){
		return time(hours, minutes, seconds, 0);
	}
	
	protected Date time(int hours, int minutes, int seconds, int millis){
		Calendar calendar=Calendar.getInstance();
		TimeZone zone=TimeZone.getTimeZone("GMT");
		calendar.setTimeZone(zone);
		calendar.set(1970, 0, 1, hours, minutes, seconds);
		calendar.set(MILLISECOND, millis);
		return calendar.getTime();
	}
}
