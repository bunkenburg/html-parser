/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.*;

import org.junit.Test;

public class RadioNodeListTest extends AbstractTest {

	@Test public void tValue0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<fieldset id='x'>"
				+ "		</fieldset>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLFieldSetElement set=(HTMLFieldSetElement)d.getElementById("x");
		HTMLInputElement a=radio(d, "radios", "a");
		HTMLInputElement b=radio(d, "radios", "b");
		HTMLInputElement c=radio(d, "radios", "c");
		set.appendChild(a);
		set.appendChild(b);
		set.appendChild(c);
		
		HTMLFormControlsCollection controls=set.getElements();
		Object o=controls.namedItem("radios");
		assertType(o, RadioNodeList.class);
		RadioNodeList list=(RadioNodeList)o;
		assertEquals(3, list.getLength());
		HTMLElement e=list.item(1);
		assertEquals(b, e);
		
		String v=list.getValue();
		assertNull(v);
		list.setValue("b");
		v=list.getValue();
		assertEquals("b", v);
	}
	
	HTMLInputElement radio(HTMLDocument d, String name, String value){
		HTMLInputElement r=d.createElement(HTMLInputElement.class);
		r.setType("radio");
		r.setName(name);
		r.setValue(value);
		return r;
	}
}
