/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LocationTest extends URLUtilsTest{
    
    private static final String INITIAL="http://www.google.com";
    
    @Override @Before public void setup(){
        super.setup();
        //Just some initial document.location,
        //otherwise it would be null.
        document.setLocation(INITIAL);
        utils=document.getLocation();
    }
    
    @Override @After public void teardown(){super.teardown();}

    @Override @Test public void tHref(){super.tHref();}
    @Override @Test public void tOrigin(){super.tOrigin();}
    @Override @Test public void tProtocol(){super.tProtocol();}
    @Override @Test public void tUsername(){super.tUsername();}
    @Override @Test public void tPassword(){super.tPassword();}
    @Override @Test public void tHost() {super.tHost();}
    @Override @Test public void tHostname(){super.tHostname();}
    @Override @Test public void tPort() {super.tPort();}
    @Override @Test public void tPathname(){super.tPathname();}
    @Override @Test public void tSearch(){super.tSearch();}
    @Override @Test public void tSearchParams(){super.tSearchParams();}
    @Override @Test public void tHash(){super.tHash();}
    
    /** document.location is null initially, 
     * here we test the value initialised in setup(). */
    @Override @Test public void threfInitial(){      
        String href=utils.getHref();
        assertEquals(INITIAL, href);
    }

    @Override @Test public void threfSetGet(){super.threfSetGet();}
}
