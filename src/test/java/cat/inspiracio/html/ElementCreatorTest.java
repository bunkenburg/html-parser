/*
Copyright 2019 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import org.junit.Test;

public class ElementCreatorTest extends AbstractTest{

    ElementCreator creator = new ElementCreator();
    HTMLDocumentBuilder builder = new HTMLDocumentBuilder();
    HTMLDocumentImp document = (HTMLDocumentImp) builder.newDocument();

    //HTMLElement create(HTMLDocumentImp c, String tag) ---------------------

    @Test public void article()throws Exception{
        HTMLElement e = creator.create(document, "article");
        assertType(e, HTMLElement.class);
    }

    @Test public void audio()throws Exception{
        HTMLElement e = creator.create(document, "audio");
        assertType(e, HTMLAudioElement.class);
    }

    @Test public void col()throws Exception{
        HTMLElement e = creator.create(document, "col");
        assertType(e, HTMLTableColElement.class);
    }

    @Test public void colgroup()throws Exception{
        HTMLElement e = creator.create(document, "colgroup");
        assertType(e, HTMLTableColGroupElement.class);
    }

    @Test public void del()throws Exception{
        HTMLElement e = creator.create(document, "del");
        assertType(e, HTMLModElement.class);
    }

    @Test public void ins()throws Exception{
        HTMLElement e = creator.create(document, "ins");
        assertType(e, HTMLModElement.class);
    }

    @Test public void video()throws Exception{
        HTMLElement e = creator.create(document, "video");
        assertType(e, HTMLVideoElement.class);
    }

    //<T extends HTMLElement> T create(HTMLDocumentImp c, Class<T> c) ---------------------

    @Test public void anchorClass(){
        creator.create(document, HTMLAnchorElement.class);
    }

    @Test public void audioClass(){
        creator.create(document, HTMLAudioElement.class);
    }

    @Test public void blockquoteClass(){
        creator.create(document, HTMLBlockquoteElement.class);
    }

    @Test public void colClass(){
        creator.create(document, HTMLTableColElement.class);
    }

    @Test public void colgroupClass(){
        creator.create(document, HTMLTableColGroupElement.class);
    }

    @Test public void videoClass(){
        creator.create(document, HTMLVideoElement.class);
    }

}
