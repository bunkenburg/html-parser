/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;

import javax.script.ScriptException;

import org.junit.Test;
import org.w3c.dom.Node;

public class HTMLTextAreaElementTest extends AbstractTest{

	@Test public void tclone() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "e=document.createElement('textarea');"
				+ "e.id='e';"
				+ "document.body.appendChild(e);"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;

		HTMLElement e=d.getElementById("e");
		Node n=e.cloneNode(true);
		assertType(n, HTMLTextAreaElement.class);
	}
	
	@Test public void tgetForm0()throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<form id='form'>"
				+ "			<textarea id='textarea0'></textarea>"
				+ "		</form>"
				+ "		<textarea id='textarea1' form='form'></textarea>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLFormElement form=(HTMLFormElement)d.getElementById("form");
		HTMLTextAreaElement textarea0=(HTMLTextAreaElement)d.getElementById("textarea0");
		HTMLTextAreaElement textarea1=(HTMLTextAreaElement)d.getElementById("textarea1");
		assertEquals(form, textarea0.getForm());
		assertEquals(form, textarea1.getForm());
	}

	@Test public void tvalue0()throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<textarea id='textarea'></textarea>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLTextAreaElement textarea=(HTMLTextAreaElement)d.getElementById("textarea");
		String value=textarea.getValue();
		assertEquals("", value);
		
		textarea.setValue("bla\ndi\nblub");
		value=textarea.getValue();
		assertEquals("bla\ndi\nblub", value);
	}

}
