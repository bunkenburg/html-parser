/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import javax.script.ScriptException;

import org.junit.Test;
import org.w3c.dom.Node;
import org.w3c.dom.stylesheets.StyleSheet;

import cat.inspiracio.html.host.Document;

public class HTMLStyleElementTest extends AbstractTest{
    
    static final String css="text/css";

	@Test public void tclone() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "e=document.createElement('style');"
				+ "e.id='e';"
				+ "document.body.appendChild(e);"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;

		HTMLElement e=d.getElementById("e");
		Node n=e.cloneNode(true);
		assertType(n, HTMLStyleElement.class);
	}

    /** not in a document: null */
    @Test public void tsheet0(){
        HTMLDocument document=new Document();
        HTMLStyleElement style=document.createElement(HTMLStyleElement.class);
        StyleSheet sheet=style.getSheet();
        assertNull(sheet);
    }
    
    @Test public void tsheet1(){
        HTMLDocument document=new Document();
        HTMLStyleElement style=document.createElement(HTMLStyleElement.class);
        HTMLHeadElement head=document.getHead();
        head.appendChild(style);
        
        StyleSheet sheet=style.getSheet();
        assertNotNull(sheet);
    }

    @Test public void tsheet5(){
        HTMLDocument document=new Document();
        HTMLStyleElement style=document.createElement(HTMLStyleElement.class);
        HTMLHeadElement head=document.getHead();
        head.appendChild(style);
        String title="Big";
        style.setTitle(title);
        
        StyleSheet sheet=style.getSheet();
        assertNotNull(sheet);
        
        assertEquals(css, sheet.getType());
        assertFalse(sheet.getDisabled());
        assertEquals(style, sheet.getOwnerNode());
        assertNull(sheet.getParentStyleSheet());
        assertNull(sheet.getHref());
        assertEquals(title, sheet.getTitle());
    }
    
    /** wrong type: null*/
    @Test public void tsheet6(){
        HTMLDocument document=new Document();
        HTMLStyleElement style=document.createElement(HTMLStyleElement.class);
        HTMLHeadElement head=document.getHead();
        head.appendChild(style);
        style.setType("text/plain");//wrong
        
        StyleSheet sheet=style.getSheet();
        assertNull(sheet);
    }
    
    /** all good: not null*/
    @Test public void tsheet7(){
        HTMLDocument document=new Document();
        HTMLStyleElement style=document.createElement(HTMLStyleElement.class);
        HTMLHeadElement head=document.getHead();
        head.appendChild(style);
        
        StyleSheet sheet=style.getSheet();
        assertNotNull(sheet);

        assertEquals(css, sheet.getType());
        assertFalse(sheet.getDisabled());
        assertEquals(style, sheet.getOwnerNode());
        assertNull(sheet.getParentStyleSheet());
        assertNull(sheet.getHref());
        assertEquals("", sheet.getTitle());
    }
    
    /** all good: with type */
    @Test public void tsheet8(){
        HTMLDocument document=new Document();
        HTMLStyleElement style=document.createElement(HTMLStyleElement.class);
        HTMLHeadElement head=document.getHead();
        head.appendChild(style);
        String type="text/css";
        style.setType(type);
        
        StyleSheet sheet=style.getSheet();
        assertNotNull(sheet);

        assertEquals(type, sheet.getType());
        assertFalse(sheet.getDisabled());
        assertEquals(style, sheet.getOwnerNode());
        assertNull(sheet.getParentStyleSheet());
        assertNull(sheet.getHref());
        assertEquals("", sheet.getTitle());
    }
    
}
