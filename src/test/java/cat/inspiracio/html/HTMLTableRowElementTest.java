/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;

import javax.script.ScriptException;

import org.junit.Test;
import org.w3c.dom.Node;

import cat.inspiracio.dom.HTMLCollection;

public class HTMLTableRowElementTest extends AbstractTest{

	@Test public void tclone() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "e=document.createElement('tr');"
				+ "e.id='e';"
				+ "document.body.appendChild(e);"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;

		HTMLElement e=d.getElementById("e");
		Node n=e.cloneNode(true);
		assertType(n, HTMLTableRowElement.class);
	}

	@Test public void tgetRowIndex0()throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<table id='table'>"
				+ "			<tr id='tr0'></tr>"
				+ "			<tr id='tr1'></tr>"
				+ "		</table>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		//HTMLTableElement table=(HTMLTableElement)document.getElementById("table");
		HTMLTableRowElement tr0=(HTMLTableRowElement)d.getElementById("tr0");
		HTMLTableRowElement tr1=(HTMLTableRowElement)d.getElementById("tr1");
		HTMLTableRowElement trn=d.createElement(HTMLTableRowElement.class);
		assertEquals(0, tr0.getRowIndex());
		assertEquals(1, tr1.getRowIndex());
		assertEquals(-1, trn.getRowIndex());
	}
	
	/** getSectionRowIndex in table: the parser inserts a tbody. */
	@Test public void tgetSectionRowIndex0()throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<table id='table'>"
				+ "			<tr id='tr0'></tr>"
				+ "			<tr id='tr1'></tr>"
				+ "		</table>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLTableRowElement tr0=(HTMLTableRowElement)d.getElementById("tr0");
		HTMLTableRowElement tr1=(HTMLTableRowElement)d.getElementById("tr1");
		HTMLTableRowElement trn=d.createElement(HTMLTableRowElement.class);
		assertEquals(0, tr0.getSectionRowIndex());
		assertEquals(1, tr1.getSectionRowIndex());
		assertEquals(-1, trn.getSectionRowIndex());
	}

	/** getSectionRowIndex in tbody. */
	@Test public void tgetSectionRowIndex1()throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<table id='table'>"
				+ "			<tbody>"
				+ "			<tr id='tr0'></tr>"
				+ "			<tr id='tr1'></tr>"
				+ "			</tbody>"
				+ "		</table>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLTableRowElement tr0=(HTMLTableRowElement)d.getElementById("tr0");
		HTMLTableRowElement tr1=(HTMLTableRowElement)d.getElementById("tr1");
		HTMLTableRowElement trn=d.createElement(HTMLTableRowElement.class);
		assertEquals(0, tr0.getSectionRowIndex());
		assertEquals(1, tr1.getSectionRowIndex());
		assertEquals(-1, trn.getSectionRowIndex());
	}

	/** getSectionRowIndex in thead. */
	@Test public void tgetSectionRowIndex2()throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<table id='table'>"
				+ "			<thead>"
				+ "				<tr id='tr0'></tr>"
				+ "				<tr id='tr1'></tr>"
				+ "			</thead>"
				+ "		</table>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLTableRowElement tr0=(HTMLTableRowElement)d.getElementById("tr0");
		HTMLTableRowElement tr1=(HTMLTableRowElement)d.getElementById("tr1");
		HTMLTableRowElement trn=d.createElement(HTMLTableRowElement.class);
		assertEquals(0, tr0.getSectionRowIndex());
		assertEquals(1, tr1.getSectionRowIndex());
		assertEquals(-1, trn.getSectionRowIndex());
	}

	/** getSectionRowIndex in tfoot. */
	@Test public void tgetSectionRowIndex3()throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<table id='table'>"
				+ "			<tfoot>"
				+ "				<tr id='tr0'></tr>"
				+ "				<tr id='tr1'></tr>"
				+ "			</tfoot>"
				+ "		</table>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLTableRowElement tr0=(HTMLTableRowElement)d.getElementById("tr0");
		HTMLTableRowElement tr1=(HTMLTableRowElement)d.getElementById("tr1");
		HTMLTableRowElement trn=d.createElement(HTMLTableRowElement.class);
		assertEquals(0, tr0.getSectionRowIndex());
		assertEquals(1, tr1.getSectionRowIndex());
		assertEquals(-1, trn.getSectionRowIndex());
	}

	@Test public void tgetCells0()throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<table id='table'>"
				+ "			<tr id='tr'></tr>"
				+ "		</table>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLTableRowElement tr=(HTMLTableRowElement)d.getElementById("tr");
		HTMLTableCellElement th=d.createElement(HTMLTableHeaderCellElement.class);
		HTMLTableCellElement td0=d.createElement(HTMLTableDataCellElement.class);
		HTMLTableCellElement td1=d.createElement(HTMLTableDataCellElement.class);
		tr.appendChild(th);
		tr.appendChild(td0);
		tr.appendChild(td1);
		HTMLCollection<HTMLTableCellElement>cells=tr.getCells();
		assertEquals(3, cells.getLength());
		assertEquals(th, cells.item(0));
		assertEquals(td0, cells.item(1));
		assertEquals(td1, cells.item(2));
	}

	//insertCell()
	//insertCell(index)
	//deleteCell(index)
	
	@Test public void tinsertCell0()throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<table id='table'><tr id='tr'/></table>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		//HTMLTableElement table=(HTMLTableElement)document.getElementById("table");
		HTMLTableRowElement row=(HTMLTableRowElement)d.getElementById("tr");
		HTMLCollection<HTMLTableCellElement> cells=row.getCells();
		assertEquals(0, cells.getLength());
		
		//insertCell() at end
		HTMLTableCellElement cell=row.insertCell();
		cells=row.getCells();
		assertEquals(1, cells.getLength());
		assertEquals(cell, cells.item(0));
		
		//insertCell(index) at end
		cell=row.insertCell(cells.getLength());
		cells=row.getCells();
		assertEquals(2, cells.getLength());
		assertEquals(cell, cells.item(1));
		
		//insertCell(0)
		cell=row.insertCell(0);
		cells=row.getCells();
		assertEquals(3, cells.getLength());
		assertEquals(cell, cells.item(0));

		//insertCell(-1) at end
		cell=row.insertCell(-1);
		cells=row.getCells();
		assertEquals(4, cells.getLength());
		assertEquals(cell, cells.item(3));

		//deleteRow
		row.deleteCell(2);
		cells=row.getCells();
		assertEquals(3, cells.getLength());
		assertEquals(cell, cells.item(2));
	}

}
