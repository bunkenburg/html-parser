/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import javax.script.ScriptException;

import org.junit.Test;
import org.w3c.dom.Node;

import cat.inspiracio.dom.HTMLCollection;

public class HTMLTableElementTest extends AbstractTest{

	@Test public void tclone() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "e=document.createElement('table');"
				+ "e.id='e';"
				+ "document.body.appendChild(e);"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;

		HTMLElement e=d.getElementById("e");
		Node n=e.cloneNode(true);
		assertType(n, HTMLTableElement.class);
	}
	
	@Test public void tcaption()throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<table id='table'>"
				+ "		</table>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLTableElement table=(HTMLTableElement)d.getElementById("table");
		assertNull(table.getCaption());
		
		HTMLTableCaptionElement caption=d.createElement(HTMLTableCaptionElement.class);
		table.setCaption(caption);
		assertEquals(caption, table.getCaption());
		
		table.deleteCaption();
		assertNull(table.getCaption());
		
		caption=table.createCaption();
		assertEquals(caption, table.getCaption());
	}

	@Test public void thead()throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<table id='table'>"
				+ "		</table>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLTableElement table=(HTMLTableElement)d.getElementById("table");
		assertNull(table.getTHead());
		
		HTMLTableSectionElement head=d.createElement(HTMLTableHeadElement.class);
		table.setTHead(head);
		assertEquals(head, table.getTHead());
		
		table.deleteTHead();
		assertNull(table.getTHead());
		
		head=table.createTHead();
		assertEquals(head, table.getTHead());
	}

	@Test public void tfoot()throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<table id='table'>"
				+ "		</table>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLTableElement table=(HTMLTableElement)d.getElementById("table");
		assertNull(table.getTFoot());
		
		HTMLTableSectionElement foot=d.createElement(HTMLTableFootElement.class);
		table.setTFoot(foot);
		assertEquals(foot, table.getTFoot());
		
		table.deleteTFoot();
		assertNull(table.getTFoot());
		
		foot=table.createTFoot();
		assertEquals(foot, table.getTFoot());
	}

	@Test public void tbodies()throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<table id='table'></table>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLTableElement table=(HTMLTableElement)d.getElementById("table");
		HTMLCollection<HTMLTableSectionElement> bodies=table.getTBodies();
		assertEquals(0, bodies.getLength());
		
		HTMLTableSectionElement body=d.createElement(HTMLTableBodyElement.class);
		table.appendChild(body);//not quite right place
		bodies=table.getTBodies();
		assertEquals(1, bodies.getLength());
		assertEquals(body, bodies.item(0));
		
		body=table.createTBody();
		bodies=table.getTBodies();
		assertEquals(2, bodies.getLength());
		assertEquals(body, bodies.item(1));
	}

	@Test public void tgetRows()throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<table id='table'></table>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLTableElement table=(HTMLTableElement)d.getElementById("table");
		HTMLCollection<HTMLTableRowElement> rows=table.getRows();
		assertEquals(0, rows.getLength());
		
		HTMLTableRowElement row=d.createElement(HTMLTableRowElement.class);
		table.appendChild(row);//not quite right place
		rows=table.getRows();
		assertEquals(1, rows.getLength());
		assertEquals(row, rows.item(0));
		
		row=d.createElement(HTMLTableRowElement.class);
		table.appendChild(row);//not quite right place
		rows=table.getRows();
		assertEquals(2, rows.getLength());
		assertEquals(row, rows.item(1));
	}

	@Test public void tinsertRow1()throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<table id='table'></table>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLTableElement table=(HTMLTableElement)d.getElementById("table");
		HTMLCollection<HTMLTableRowElement> rows=table.getRows();
		assertEquals(0, rows.getLength());
		
		//insertRow() at end
		HTMLTableRowElement row=table.insertRow();
		rows=table.getRows();
		assertEquals(1, rows.getLength());
		assertEquals(row, rows.item(0));
		
		//insertRow(index) at end
		row=table.insertRow(rows.getLength());
		rows=table.getRows();
		assertEquals(2, rows.getLength());
		assertEquals(row, rows.item(1));
		
		//insertRow(0)
		row=table.insertRow(0);
		rows=table.getRows();
		assertEquals(3, rows.getLength());
		assertEquals(row, rows.item(0));

		//insertRow(-1) at end
		row=table.insertRow(-1);
		rows=table.getRows();
		assertEquals(4, rows.getLength());
		assertEquals(row, rows.item(3));

		//deleteRow
		table.deleteRow(2);
		rows=table.getRows();
		assertEquals(3, rows.getLength());
		assertEquals(row, rows.item(2));
	}

}
