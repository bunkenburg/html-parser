/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.*;

import java.io.IOException;

import javax.script.ScriptException;

import org.junit.Test;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import cat.inspiracio.html.host.Document;

public class HTMLHtmlElementTest extends AbstractTest{

	@Test public void tparse() throws IOException, SAXException{
		HTMLDocument d=path("html/html/index.html");
		HTMLHtmlElement e=d.getDocumentElement();
		assertType(e, HTMLHtmlElement.class);
		assertType(e, HTMLHtmlElementImp.class);
	}

	@Test public void tclone() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "a=document.html;"
				+ "a.id='x';"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;
		HTMLElement e=d.getElementById("x");
		Node n=e.cloneNode(true);
		assertType(n, HTMLHtmlElement.class);
	}

	@Test public void tHeadBody(){
		HTMLDocument d=new Document();
		HTMLHtmlElement html=d.getHtml();
		HTMLHeadElement head=html.getHead();
		HTMLBodyElement body=html.getBody();
		assertNotNull(head);
		assertNotNull(body);
	}
}
