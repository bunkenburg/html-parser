/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;

import javax.script.ScriptException;

import org.junit.Test;
import org.w3c.dom.Node;

public class HTMLTableCellElementTest extends AbstractTest{

	@Test public void tclone() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "e=document.createElement('td');"
				+ "e.id='e';"
				+ "document.body.appendChild(e);"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;

		HTMLElement e=d.getElementById("e");
		Node n=e.cloneNode(true);
		assertType(n, HTMLTableCellElement.class);
	}

	@Test public void tclone1() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "e=document.createElement('th');"
				+ "e.id='e';"
				+ "document.body.appendChild(e);"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;

		HTMLElement e=d.getElementById("e");
		Node n=e.cloneNode(true);
		assertType(n, HTMLTableCellElement.class);
	}

	@Test public void tGetCellIndex0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<table>"
				+ "			<tr id='tr'>"
				+ "			</tr>"
				+ "		</table>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLTableRowElement tr=(HTMLTableRowElement) d.getElementById("tr");
		HTMLTableCellElement a=d.createElement(HTMLTableCellElement.class);
		HTMLTableCellElement b=d.createElement(HTMLTableCellElement.class);
		HTMLTableCellElement c=d.createElement(HTMLTableCellElement.class);
		HTMLTableCellElement n=d.createElement(HTMLTableCellElement.class);
		tr.appendChild(a);
		tr.appendChild(b);
		tr.appendChild(c);
		//Don't insert n
		assertEquals(0, a.getCellIndex());
		assertEquals(1, b.getCellIndex());
		assertEquals(2, c.getCellIndex());
		assertEquals(-1, n.getCellIndex());
	}
}
