/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;

import javax.script.ScriptException;

import org.junit.Test;
import org.w3c.dom.Node;

public class HTMLOptionElementTest extends AbstractTest{

	@Test public void tclone() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "e=document.createElement('option');"
				+ "e.id='e';"
				+ "document.body.appendChild(e);"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;

		HTMLElement e=d.getElementById("e");
		Node n=e.cloneNode(true);
		assertType(n, HTMLOptionElement.class);
	}
	
	@Test public void tSetLength0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<select id='x'>"
				+ "			<option value='a'>alpha</option>"
				+ "			<option value='b'>beta</option>"
				+ "			<option value='c'>gamma</option>"
				+ "		</select>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLSelectElement select=(HTMLSelectElement)d.getElementById("x");
		assertEquals(3, select.getLength());
		
		//same length
		select.setLength(3);
		assertEquals(3, select.getLength());
		
		//lengthen
		select.setLength(6);
		assertEquals(6, select.getLength());
		
		//shorten
		select.setLength(2);
		assertEquals(2, select.getLength());
		
		//shorten to 0
		select.setLength(0);
		assertEquals(0, select.getLength());
	}

}
