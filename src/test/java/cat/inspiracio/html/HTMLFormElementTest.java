/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;

import javax.script.ScriptException;

import org.junit.Test;
import org.w3c.dom.Node;

import cat.inspiracio.html.host.Document;

public class HTMLFormElementTest extends AbstractTest{

	@Test public void tclone() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "a=document.createElement('form');"
				+ "a.id='x';"
				+ "document.body.appendChild(a);"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;
		HTMLElement e=d.getElementById("x");
		Node n=e.cloneNode(true);
		assertType(n, HTMLFormElement.class);
	}
	
	@Test public void tGetElements0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<form id='form'>"
				+ "			<button/>"
				+ "			<fieldset/>"
				+ "			<input/>"
				+ "			<input type='image'/>"	//excluded
				+ "			<keygen/>"
				+ "			<object></object>"
				+ "			<output></output>"
				+ "			<select/>"
				+ "		</from>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLFormElement form=(HTMLFormElement)d.getElementById("form");
		HTMLTextAreaElement text=d.createElement(HTMLTextAreaElement.class);
		text.setName("text");
		form.appendChild(text);
		
		HTMLFormControlsCollection controls=form.getElements();
		assertEquals(8, controls.getLength());
		assertEquals(8, form.getLength());
		ListedElement e=controls.item(7);
		assertEquals(text, e);
		assertEquals(text, form.item(7));
		Object o=controls.namedItem("text");
		assertEquals(text, o);
	}

	@Test public void tGetNamedItem0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<form id='form'>"
				+ "			<input name='x' type='radio' value='a'/>"
				+ "			<input name='x' type='radio' value='b'/>"
				+ "			<input name='x' type='radio' value='c'/>"
				+ "			<input type='image' name='x'/>"	//excluded
				+ "			<keygen/>"
				+ "			<object></object>"
				+ "			<output></output>"
				+ "			<select/>"
				+ "		</from>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLFormElement form=(HTMLFormElement)d.getElementById("form");
		HTMLTextAreaElement text=d.createElement(HTMLTextAreaElement.class);
		text.setName("text");
		form.appendChild(text);
		
		Object o=form.namedItem("x");
		assertType(o, RadioNodeList.class);
		RadioNodeList radios=(RadioNodeList)o;
		assertEquals(3, radios.getLength());
	}

	/** img elements in the form */
	@Test public void tGetNamedItem1() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<form id='form'>"
				+ "			<img id='x'/>"
				+ "			<img name='x' value='a'/>"
				+ "			<img name='x' value='b'/>"
				+ "			<input type='image' name='x'/>"	//excluded
				+ "			<keygen/>"
				+ "			<object></object>"
				+ "			<output></output>"
				+ "			<select/>"
				+ "		</from>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLFormElement form=(HTMLFormElement)d.getElementById("form");
		HTMLTextAreaElement text=d.createElement(HTMLTextAreaElement.class);
		text.setName("text");
		form.appendChild(text);
		
		Object o=form.namedItem("x");
		assertType(o, RadioNodeList.class);
		RadioNodeList radios=(RadioNodeList)o;
		assertEquals(3, radios.getLength());
	}
	
	@Test public void tGetNamedItem2() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<form id='form'>"
				+ "			<button name='x'>alpha</button>"
				+ "			<input type='image' name='x'/>"	//excluded
				+ "		</from>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLFormElement form=(HTMLFormElement)d.getElementById("form");
		HTMLTextAreaElement text=d.createElement(HTMLTextAreaElement.class);
		text.setName("text");
		form.appendChild(text);
		
		Object o=form.namedItem("x");
		assertType(o, HTMLButtonElement.class);
		HTMLButtonElement button=(HTMLButtonElement)o;
		assertEquals("x", button.getName());
	}

	@Test public void tenctype(){
		HTMLDocument d=new Document();
		
		HTMLFormElement form=d.createElement(HTMLFormElement.class);
		assertEquals("", form.getEnctype());
		
		form.setEnctype("multipart/form-data");
		assertEquals("multipart/form-data", form.getEnctype());
		
		//invalid default
		form.setEnctype("invalid");
		assertEquals("application/x-www-form-urlencoded", form.getEnctype());

		//invalid default
		form.setAttribute("enctype", "invalid");//no check here
		assertEquals("application/x-www-form-urlencoded", form.getEnctype());
	}


}
