/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import javax.script.ScriptException;

import org.junit.Test;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Comment;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import cat.inspiracio.html.host.Document;

public class HTMLElementTest extends AbstractTest{

	@Test public void tclone() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "a=document.createElement('area');"
				+ "a.id='x';"
				+ "document.body.appendChild(a);"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;
		HTMLElement e=d.getElementById("x");
		Node n=e.cloneNode(true);
		assertType(n, HTMLElement.class);
	}

	/** Test for < inside text. */
	@Test public void tescapeElement() throws SAXException, IOException{
		String s=
				"<!DOCTYPE html>"
				+ "<html><head/><body>Show a title tag: &lt;title>.That's it.</body></html>";
		
		//Parsing must produce "<title>" in the text. 
		HTMLDocument d=parse(s);
		HTMLElement body=d.getBody();
		String text=body.getTextContent();
		assertEquals("Show a title tag: <title>.That's it.", text);
		
		//Rendering must output "&lt;title>" so that the text can not be confused with a tag.
		String rendered=format(d);
		assertEquals(s, rendered);
	}

	/** No escaping of < > & in a script element. */
	@Test public void tescapeScriptElement() throws SAXException, IOException{
		String s=
				"<!DOCTYPE html>"
				+ "<html>"
				+ "<head/>"
				+ "<body>"
				+ "<script>0<1 && 1>0</script>"
				+ "</body>"
				+ "</html>";
		
		HTMLDocument d=parse(s);
		HTMLElement e=d.getElementByTagName("script");
		String text=e.getTextContent();
		assertEquals(text, "0<1 && 1>0");
		
		//Formatting must give the same script element.
		String rendered=format(d);
		assertEquals(s, rendered);
	}

	/** Test ' and " inside attribute. 
	 * DocumentWriter puts all attribute inside " and only escapes " by &quot;. */
	@Test public void tescapeAttribute() throws SAXException, IOException{
		String s=
				"<!DOCTYPE html>"
				+ "<html apos=\"&#39;\" quot=\"&quot;\">"
				+ "<head/>"
				+ "<body/>"
				+ "</html>";
		String expected=
				"<!DOCTYPE html>"
				+ "<html apos=\"'\" quot=\"&quot;\">"
				+ "<head/>"
				+ "<body/>"
				+ "</html>";
		
		//Parsing must produce ' and " in the values. 
		HTMLDocument d=parse(s);
		HTMLElement e=d.getDocumentElement();
		String apos=e.getAttribute("apos");
		assertEquals(apos, "'");
		String quot=e.getAttribute("quot");
		assertEquals(quot, "\"");
		
		//Rendering must escape them again.
		String rendered=format(d);
		assertEquals(expected, rendered);
	}

	/** Test ]]> inside cdata. 
	 * I could find no spec, only wikipedia recommendation to insert another cdata section. */
	@Test public void tescapeCData() throws SAXException, IOException{
		String s=
				"<!DOCTYPE html>"
				+ "<html>"
				+ "<head/>"
				+ "<body>"
				+ "<![CDATA[bla]]>"
				+ "</body>"
				+ "</html>";
		String expected=
				"<!DOCTYPE html>"
				+ "<html>"
				+ "<head/>"
				+ "<body/>"
				+ "<![CDATA[]]]]><![CDATA[>]]>"
				+ "</html>";
		
		//Parsing loses the cdata, because html5 does not have cdata sections.
		HTMLDocument d=parse(s);
		String rendered=format(d);
		assertFalse(rendered.contains("CDATA"));
		
		//Add cdata section programmatically.
		String data="";
		CDATASection cdata=d.createCDATASection(data);
		HTMLElement e=d.getDocumentElement();
		e.appendChild(cdata);
		rendered=format(d);
		assertTrue(rendered.contains("CDATA"));
		
		//Check ]]> inside the cdata will be escaped by rendering.
		cdata.setData("]]>");
		rendered=format(d);
		assertEquals(expected, rendered);
	}

	/** Test --> inside comment to something else.
	 * I couldn't find any spec or recommendation what I should escape -- to,
	 * so this test is implementation-dependent. */
	@Test public void tescapeComment() throws SAXException, IOException{
		String s=
				"<!DOCTYPE html>"
				+ "<html>"
				+ "<head/>"
				+ "<body>"
				+ "<!--comment-->"
				+ "</body>"
				+ "</html>";
		String expected=
				"<!DOCTYPE html>"
				+ "<html>"
				+ "<head/>"
				+ "<body/>"
				+ "<!--[escaped double -]>-->"
				+ "</html>";
		
		//Parsing loses the comment.
		HTMLDocument d=parse(s);
		String rendered=format(d);
		assertFalse(rendered.contains("comment"));
		
		//Add comment programmatically.
		String data="comment";
		Comment comment=d.createComment(data);
		HTMLElement e=d.getDocumentElement();
		e.appendChild(comment);
		rendered=format(d);
		assertTrue(rendered.contains("comment"));
		
		//Check --> inside the comment will be escaped by rendering.
		comment.setData("-->");
		rendered=format(d);
		assertEquals(expected, rendered);
	}

	@Test public void taccessKey(){
	    Document d=new Document();
	    HTMLElement e=d.createElement("div");
	    
	    assertEquals("", e.getAccessKey());
	    
	    e.setAccessKey("a");
	    assertEquals("a", e.getAccessKey());
	    
	    assertEquals("", e.getAccessKeyLabel());//just ""
	}
}
