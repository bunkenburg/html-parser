/*
Copyright 2019 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import org.junit.Test;

import static org.junit.Assert.*;

public class HTMLDocumentBuilderTest {

    /** Call the document builder with my own implementation
     * for documents. This is similar to what yellow's Engine does. */
    @Test public void extendedImplementation(){
        HTMLDOMImplementation implementation = new HTMLDOMImplementation(){

            @Override public HTMLDocument createHTMLDocument(String title) {
                HTMLDocumentImp d = new HTMLDocumentImp(this);
                d.setTitle("alex");
                return d;
            }

        };
        HTMLDocumentBuilder builder = new HTMLDocumentBuilder(implementation);
        HTMLDocument document = builder.newDocument();

        String title = document.getTitle();
        assertEquals("alex", title);
    }

    /** What happens when I parse a fragment?
     * builder.parse() returns a full document with fake elements. */
    @Test public void fragment()throws Exception{
        HTMLDocumentBuilder builder = new HTMLDocumentBuilder();
        HTMLDocument d = builder.parse("<div id='id'>alex</div>");//with fake elements
        HTMLElement div = d.getElementById("id");
        String content = div.getTextContent();
        assertEquals("alex", content);
    }

}
