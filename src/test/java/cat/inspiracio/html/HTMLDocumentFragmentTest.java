/*
Copyright 2017 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import java.io.IOException;

import org.junit.Test;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class HTMLDocumentFragmentTest extends AbstractTest {

	@Test public void tClone() throws SAXException, IOException{
		HTMLDocument d=parse("<html/>");
		DocumentFragment f=d.createDocumentFragment();
		assertType(f, HTMLDocumentFragmentImp.class);
		Node c=f.cloneNode(true);
		assertType(c, HTMLDocumentFragmentImp.class);
	}
}
