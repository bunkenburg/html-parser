/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.*;

import java.io.IOException;

import javax.script.ScriptException;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import cat.inspiracio.dom.HTMLCollection;

public class HTMLTemplateElementTest extends AbstractTest{

	@Test public void tClass() throws IOException, SAXException{
		Document d=path("html/template/template.html");
		format(d);
		Element template=d.getElementById("0");
		assertType(template, HTMLTemplateElement.class);
	}

	@Test public void tclone() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "e=document.createElement('template');"
				+ "e.id='e';"
				+ "document.body.appendChild(e);"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;

		HTMLElement e=d.getElementById("e");
		Node n=e.cloneNode(true);
		assertType(n, HTMLTemplateElement.class);
	}
	
	@Test public void tgetContent0()throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<template id='template'><div>Bla!</div></template>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLBodyElement body=d.getBody();
		HTMLTemplateElement template=(HTMLTemplateElement)d.getElementById("template");
		DocumentFragment fragment=template.getContent();
		body.appendChild(fragment);
		body.removeChild(template);
		HTMLCollection<HTMLElement> children=body.getChildElements();
		assertEquals(1, children.getLength());
		HTMLElement child=children.item(0);
		assertType(child, HTMLDivElement.class);
	}

}
