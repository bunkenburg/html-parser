/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import javax.script.ScriptException;

import org.junit.Test;
import org.w3c.dom.Node;
import org.w3c.dom.stylesheets.StyleSheet;

import cat.inspiracio.html.host.Document;

public class HTMLLinkElementTest extends AbstractTest{

	@Test public void tclone() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "a=document.createElement('map');"
				+ "a.id='x';"
				+ "document.body.appendChild(a);"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;
		HTMLElement e=d.getElementById("x");
		Node n=e.cloneNode(true);
		assertType(n, HTMLMapElement.class);
	}
    
	/** not in a document: null */
    @Test public void tsheet0(){
        HTMLDocument document=new Document();
        HTMLLinkElement link=document.createElement(HTMLLinkElement.class);
        link.setRel("stylesheet");
        link.setHref("default.css");
        StyleSheet sheet=link.getSheet();
        assertNull(sheet);
    }
    
    /** No href: null */
    @Test public void tsheet1(){
        HTMLDocument document=new Document();
        HTMLLinkElement link=document.createElement(HTMLLinkElement.class);
        HTMLHeadElement head=document.getHead();
        head.appendChild(link);
        
        StyleSheet sheet=link.getSheet();
        assertNull(sheet);
    }

    /** No rel: null*/
    @Test public void tsheet2(){
        HTMLDocument document=new Document();
        HTMLLinkElement link=document.createElement(HTMLLinkElement.class);
        HTMLHeadElement head=document.getHead();
        head.appendChild(link);
        link.setHref("default.css");

        StyleSheet sheet=link.getSheet();
        assertNull(sheet);
    }
    
    /** rel without "stylesheet": null*/
    @Test public void tsheet3(){
        HTMLDocument document=new Document();
        HTMLLinkElement link=document.createElement(HTMLLinkElement.class);
        HTMLHeadElement head=document.getHead();
        head.appendChild(link);
        link.setHref("default.css");
        link.setRel("alternate");
        
        StyleSheet sheet=link.getSheet();
        assertNull(sheet);
    }
    
    /** "alternate stylesheet" without title: null*/
    @Test public void tsheet4(){
        HTMLDocument document=new Document();
        HTMLLinkElement link=document.createElement(HTMLLinkElement.class);
        HTMLHeadElement head=document.getHead();
        head.appendChild(link);
        link.setHref("default.css");
        link.setRel("alternate stylesheet");
        
        StyleSheet sheet=link.getSheet();
        assertNull(sheet);
    }
    
    /** "alternate stylesheet" with title: not null*/
    @Test public void tsheet5(){
        HTMLDocument document=new Document();
        HTMLLinkElement link=document.createElement(HTMLLinkElement.class);
        HTMLHeadElement head=document.getHead();
        head.appendChild(link);
        String href="default.css";
        link.setHref(href);
        link.setRel("alternate stylesheet");
        String title="Big";
        link.setTitle(title);
        
        StyleSheet sheet=link.getSheet();
        assertNotNull(sheet);
        
        assertNull(sheet.getType());
        assertFalse(sheet.getDisabled());
        assertEquals(link, sheet.getOwnerNode());
        assertNull(sheet.getParentStyleSheet());
        assertEquals(href, sheet.getHref());
        assertEquals(title, sheet.getTitle());
    }
    
    /** wrong type: null*/
    @Test public void tsheet6(){
        HTMLDocument document=new Document();
        HTMLLinkElement link=document.createElement(HTMLLinkElement.class);
        HTMLHeadElement head=document.getHead();
        head.appendChild(link);
        link.setHref("default.css");
        link.setRel("stylesheet");
        link.setType("text/plain");//wrong
        
        StyleSheet sheet=link.getSheet();
        assertNull(sheet);
    }
    
    /** all good: not null*/
    @Test public void tsheet7(){
        HTMLDocument document=new Document();
        HTMLLinkElement link=document.createElement(HTMLLinkElement.class);
        HTMLHeadElement head=document.getHead();
        head.appendChild(link);
        String href="default.css";
        link.setHref(href);
        link.setRel("stylesheet");
        
        StyleSheet sheet=link.getSheet();
        assertNotNull(sheet);

        assertNull(sheet.getType());
        assertFalse(sheet.getDisabled());
        assertEquals(link, sheet.getOwnerNode());
        assertNull(sheet.getParentStyleSheet());
        assertEquals(href, sheet.getHref());
        assertNull(sheet.getTitle());
    }
    
    /** all good: with type */
    @Test public void tsheet8(){
        HTMLDocument document=new Document();
        HTMLLinkElement link=document.createElement(HTMLLinkElement.class);
        HTMLHeadElement head=document.getHead();
        head.appendChild(link);
        String href="default.css";
        link.setHref(href);
        link.setRel("stylesheet");
        String type="text/css";
        link.setType(type);
        
        StyleSheet sheet=link.getSheet();
        assertNotNull(sheet);

        assertEquals(type, sheet.getType());
        assertFalse(sheet.getDisabled());
        assertEquals(link, sheet.getOwnerNode());
        assertNull(sheet.getParentStyleSheet());
        assertEquals(href, sheet.getHref());
        assertNull(sheet.getTitle());
    }
    
}
