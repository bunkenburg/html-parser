/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class VideoTrackListTest extends AbstractTest{

    final int N=3;
    VideoTrack[] a=new VideoTrack[N];
    VideoTrackListImp ts=new VideoTrackListImp();
    VideoTrackList tracks;

    @Before public void setup(){
        for(int i=0; i<N; i++){
            VideoTrackImp t=new VideoTrackImp();
            t.setId("id" + i);
            ts.add(t);
            a[i]=t;
        }
        tracks=ts;
    }
    
    @Test public void tLength(){assertEquals(N, tracks.getLength());}

    @Test public void tItem0(){
        for(int i=0; i<N; i++){
            VideoTrack t=tracks.item(i);
            assertEquals(a[i], t);
            assertEquals("id" + i, t.getId());
        }
    }

    @Test public void tgetTrackById0(){
        for(int i=0; i<N; i++){
            VideoTrack t=tracks.getTrackById("id" + i);
            assertEquals(a[i], t);
        }
    }

    @Test public void tselected0(){
        for(int i=0; i<N; i++){
            ts.setSelectedIndex(i);
            assertEquals(i, tracks.getSelectedIndex());
        }
    }
}
