/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;
import cat.inspiracio.url.URLSearchParams;
import cat.inspiracio.url.URLUtils;

public class URLUtilsTest extends AbstractTest{

    protected HTMLDocument document;
    protected URLUtils utils;

    public void setup(){
		HTMLDocumentBuilder builder=new HTMLDocumentBuilder();
        document=builder.newDocument();
    }

    public void teardown(){}

    public void tHref() {
        String url="http://www.google.com";
        utils.setHref(url);
        String href=utils.getHref();
        assertEquals(url, href);
    }

    public void tOrigin() {
        String url="http://www.google.com:8888";
        utils.setHref(url);
        String origin=utils.getOrigin();
        assertEquals("http://www.google.com:8888", origin);
    }

    public void tProtocol() {
        String url="http://www.google.com:8888";
        utils.setHref(url);
        String protocol=utils.getProtocol();
        assertEquals("http:", protocol);
        
        utils.setProtocol("https");
        protocol=utils.getProtocol();
        assertEquals("https:", protocol);
    }

    public void tUsername() {
        String url="http://alex@www.google.com:8888";
        utils.setHref(url);
        String username=utils.getUsername();
        assertEquals("alex", username);
        
        utils.setUsername("laia");
        username=utils.getUsername();
        assertEquals("laia", username);
    }

    public void tPassword() {
        String url="http://alex:pass@www.google.com:8888";
        utils.setHref(url);
        String password=utils.getPassword();
        assertEquals("pass", password);
        
        utils.setPassword("word");
        password=utils.getPassword();
        assertEquals("word", password);
    }

    public void tHost() {
        String url="http://alex@www.google.com:8888";
        utils.setHref(url);
        String host=utils.getHost();
        assertEquals("www.google.com:8888", host);
        
        utils.setHost("www.boogle.com:80");
        host=utils.getHost();
        assertEquals("www.boogle.com", host);
    }

    public void tHostname() {
        String url="http://alex@www.google.com:8888";
        utils.setHref(url);
        String host=utils.getHostname();
        assertEquals("www.google.com", host);
        
        utils.setHostname("www.boogle.com");
        host=utils.getHostname();
        assertEquals("www.boogle.com", host);
    }

    public void tPort() {
        String url="http://alex@www.google.com:8888";
        utils.setHref(url);
        int port=utils.port();
        assertEquals(8888, port);
        String p=utils.getPort();
        assertEquals("8888", p);
        
        utils.setPort("9999");
        port=utils.port();
        assertEquals(9999, port);
        
        utils.setPort(7777);
        port=utils.port();
        assertEquals(7777, port);
    }

    public void tPathname() {
        String url="https://www.google.es/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&q=cactus";
        utils.setHref(url);
        String pathname=utils.getPathname();
        assertEquals("/webhp", pathname);
        
        utils.setPathname("/bla/di/blub");
        pathname=utils.getPathname();
        assertEquals("/bla/di/blub", pathname);
    }

    public void tSearch() {
        String url="https://www.google.es/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&q=cactus";
        utils.setHref(url);
        String search=utils.getSearch();
        assertEquals("?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&q=cactus", search);
        
        utils.setSearch("sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&q=orchid");
        search=utils.getSearch();
        assertEquals("?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&q=orchid", search);
    }

    public void tSearchParams() {
        String url="https://www.google.es/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&q=cactus";
        utils.setHref(url);
        URLSearchParams params=utils.getSearchParams();
        assertEquals(new URLSearchParams("sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&q=cactus"), params);
        
        params.set("sourceid", "firefox");
        utils.setSearchParams(params);
        params=utils.getSearchParams();
        assertEquals(new URLSearchParams("sourceid=firefox&ion=1&espv=2&ie=UTF-8&q=cactus"), params);
    }

    public void tHash() {
        String url="https://www.google.es/webhp#cactus";
        utils.setHref(url);
        String hash=utils.getHash();
        assertEquals("#cactus", hash);
        
        utils.setHash("orchid");
        hash=utils.getHash();
        assertEquals("#orchid", hash);
    }

    public void threfInitial(){      
        String href=utils.getHref();
        assertEquals("", href);
    }

    public void threfSetGet() {
        String url="http://www.google.com";
        utils.setHref(url);
        assertEquals(url, utils.getHref());
    }

}
