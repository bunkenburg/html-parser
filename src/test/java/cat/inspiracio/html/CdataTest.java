/*
Copyright 2016 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class CdataTest extends AbstractTest{

    @Override @Before public void setUp(){super.setUp();}
    @Override @After public void tearDown(){super.tearDown();}
    
    @Ignore
    @Test public void tcdata() throws SAXException, IOException{
        Document d=path("html/cdata/index.html");
        Element p=d.getElementById("cdata");
        String text=p.getTextContent();
        assertEquals("Ignores CDATA", "hello", text);
    }

}
