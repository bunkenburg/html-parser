/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import javax.script.ScriptException;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import cat.inspiracio.dom.HTMLCollection;

public class HTMLSelectElementTest extends AbstractTest{

	@Test public void tAutofocus() throws IOException, SAXException{
		Document d=path("html/select/autofocus.html");
		format(d);

		HTMLSelectElement select;
		select=(HTMLSelectElement) getSelectById(d, "0");
		boolean autofocus=select.getAutofocus();
		assertTrue(autofocus);
		select.setAutofocus(false);
		autofocus=select.getAutofocus();
		assertTrue(!autofocus);
		select.setAutofocus(true);
		assertTrue(select.getAutofocus());
		
		select=(HTMLSelectElement) getSelectById(d, "1");
		assertTrue(!select.getAutofocus());
		select.setAutofocus(true);
		assertTrue(select.getAutofocus());
		select.setAutofocus(false);
		assertTrue(!select.getAutofocus());
	}

	@Test public void tValue() throws IOException, SAXException{
		Document d=path("html/select/value.html");
		format(d);

		HTMLSelectElement select=(HTMLSelectElement) getSelectById(d, "0");
		assertTrue("".equals(select.getValue()));
		select.setValue("a");
		assertTrue("a".equals(select.getValue()));
		
		select=(HTMLSelectElement) getSelectById(d, "1");
		assertTrue("b".equals(select.getValue()));
		select.setValue("c");
		assertTrue("c".equals(select.getValue()));
	}

	@Test public void tType() throws IOException, SAXException{
		Document d=path("html/select/type.html");
		
		HTMLSelectElement select=(HTMLSelectElement) getSelectById(d, "0");
		assertTrue("select-multiple".equals(select.getType()));
		
		select=(HTMLSelectElement) getSelectById(d, "1");
		assertTrue("select-one".equals(select.getType()));
	}

	@Test public void tOptions() throws IOException, SAXException{
		Document d=path("html/select/options.html");
		
		HTMLSelectElement select=(HTMLSelectElement) getSelectById(d, "0");
		HTMLOptionsCollection options=select.getOptions();
		
		assertTrue(3==options.getLength());
//		for(int i=0; i<options.getLength(); i++){
//			HTMLOptionElement option=options.item(i);
//			say(option);
//		}
	}

	@Test public void t0() throws IOException, SAXException{
		Document d=path("html/select/select.html");
		format(d);
		
		NodeList list=d.getElementsByTagName("select");
		assertTrue(list.getLength()==2);
		Node node=list.item(0);
		node=d.getElementById("0");
		assertType(node, HTMLSelectElement.class);
		
		HTMLSelectElement select=(HTMLSelectElement)node;
		
		String value=select.getValue();
		assertTrue("b".equals(value));
		
		select.setValue("a");
		value=select.getValue();
		assertTrue("a".equals(value));
		
		String type=select.getType();
		assertTrue("select-one".equals(type));
		
		select=(HTMLSelectElement)d.getElementById("1");
		type=select.getType();
		assertTrue("select-multiple".equals(type));
	}

	@Test public void tclone() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "e=document.createElement('select');"
				+ "e.id='e';"
				+ "document.body.appendChild(e);"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;

		HTMLElement e=d.getElementById("e");
		Node n=e.cloneNode(true);
		assertType(n, HTMLSelectElement.class);
	}

	/** set(index, null) should remove */
	@Test public void tSet0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<select id='x'>"
				+ "			<option value='a'>alpha</option>"
				+ "			<option value='b'>beta</option>"
				+ "			<option value='c'>gamma</option>"
				+ "		</select>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLSelectElement select=(HTMLSelectElement) d.getElementById("x");
		select.set(1, null);// delete beta
		HTMLOptionsCollection options=select.getOptions();
		assertEquals(2, options.getLength());
	}

	/** set(index, option) adding more options */
	@Test public void tSet1() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<select id='x'>"
				+ "			<option value='a'>alpha</option>"
				+ "		</select>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLSelectElement select=(HTMLSelectElement) d.getElementById("x");
		HTMLOptionElement option=option(d, "gamma");
		select.set(2, option);// adds two
		HTMLOptionsCollection options=select.getOptions();
		assertEquals(3, options.getLength());
		option=options.item(2);
		assertEquals("gamma", option.getValue());
	}

	/** set(index, option) replacing an option */
	@Test public void tSet2() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<select id='x'>"
				+ "			<option value='a'>alpha</option>"
				+ "			<option value='b'>beta</option>"
				+ "			<option value='c'>gamma</option>"
				+ "		</select>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLSelectElement select=(HTMLSelectElement) d.getElementById("x");
		HTMLOptionElement option=option(d, "second");
		select.set(1, option);// replaces 1
		HTMLOptionsCollection options=select.getOptions();
		assertEquals(3, options.getLength());
		option=options.item(1);
		assertEquals("second", option.getValue());
	}

	@Test public void tHas0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<select id='x'>"
				+ "			<option value='a'>alpha</option>"
				+ "			<option value='b'>beta</option>"
				+ "			<option value='c'>gamma</option>"
				+ "		</select>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLSelectElement select=(HTMLSelectElement) d.getElementById("x");
		assertTrue(select.has(0));
		assertTrue(select.has(1));
		assertTrue(select.has(2));
		assertFalse(select.has(3));
	}

	@Test public void tGet0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<select id='x'>"
				+ "			<option value='a'>alpha</option>"
				+ "			<option value='b'>beta</option>"
				+ "			<option value='c'>gamma</option>"
				+ "		</select>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLSelectElement select=(HTMLSelectElement) d.getElementById("x");
		assertEquals("a", select.get(0).getValue());
		assertEquals("b", select.get(1).getValue());
		assertEquals("c", select.get(2).getValue());
		assertNull(select.get(3));
	}

	@Test public void tDelete0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<select id='x'>"
				+ "			<option value='a'>alpha</option>"
				+ "			<option value='b'>beta</option>"
				+ "			<option value='c'>gamma</option>"
				+ "		</select>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLSelectElement select=(HTMLSelectElement) d.getElementById("x");
		assertEquals(3, select.getOptions().getLength());
		select.delete(0);
		assertEquals(2, select.getOptions().getLength());
		select.delete(1);
		assertEquals(1, select.getOptions().getLength());
		select.delete(0);
		assertEquals(0, select.getOptions().getLength());
		select.delete(0);
		assertEquals(0, select.getOptions().getLength());
	}
	
	@Test public void tRemove0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<select id='x'>"
				+ "			<option value='a'>alpha</option>"
				+ "			<option value='b'>beta</option>"
				+ "			<option value='c'>gamma</option>"
				+ "		</select>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLSelectElement select=(HTMLSelectElement) d.getElementById("x");

		select.remove();
		
		assertNull(select.getParentNode());
		select=(HTMLSelectElement) d.getElementById("x");
		assertNull(select);
	}
	
	@Test public void tRemoveIndex0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<select id='x'>"
				+ "			<option value='a'>alpha</option>"
				+ "			<option value='b'>beta</option>"
				+ "			<option value='c'>gamma</option>"
				+ "		</select>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLSelectElement select=(HTMLSelectElement) d.getElementById("x");

		select.remove(1);

		assertEquals(2, select.getLength());
		HTMLOptionElement o=select.item(1);
		assertEquals("c", o.getValue());
	}
	
	@Test public void tgetSelectedOptions0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<select id='x'>"
				+ "			<option value='a'>alpha</option>"
				+ "			<option value='b'>beta</option>"
				+ "			<option value='c'>gamma</option>"
				+ "		</select>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLSelectElement select=(HTMLSelectElement) d.getElementById("x");
		HTMLCollection<HTMLOptionElement> selected=select.getSelectedOptions();
		assertEquals(0, selected.getLength());
		
		HTMLOptionElement o=select.item(1);
		assertEquals("b", o.getValue());
		o.setSelected(true);
		
		selected=select.getSelectedOptions();
		assertEquals(1, selected.getLength());
		o=selected.item(0);
		assertEquals("b", o.getValue());
		
		o=select.item(2);
		assertEquals("c", o.getValue());
		o.setSelected(true);

		selected=select.getSelectedOptions();
		assertEquals(2, selected.getLength());
		o=selected.item(1);
		assertEquals("c", o.getValue());
	}
	
	@Test public void tgetSelectedIndex0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<select id='x'>"
				+ "			<option value='a'>alpha</option>"
				+ "			<option value='b'>beta</option>"
				+ "			<option value='c'>gamma</option>"
				+ "		</select>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLSelectElement select=(HTMLSelectElement) d.getElementById("x");
		
		int selected=select.getSelectedIndex();
		assertEquals(-1, selected);
		
		HTMLOptionElement o=select.item(1);
		assertEquals("b", o.getValue());
		o.setSelected(true);
		
		selected=select.getSelectedIndex();
		assertEquals(1, selected);
		o=select.item(1);
		assertEquals("b", o.getValue());
		
		//If we selected two, still only the first will be reported.
		o=select.item(2);
		assertEquals("c", o.getValue());
		o.setSelected(true);

		selected=select.getSelectedIndex();
		assertEquals(1, selected);
	}
	
	@Test public void tsetSelectedIndex0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<select id='x'>"
				+ "			<option value='a'>alpha</option>"
				+ "			<option value='b'>beta</option>"
				+ "			<option value='c'>gamma</option>"
				+ "		</select>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLSelectElement select=(HTMLSelectElement)d.getElementById("x");
		
		select.setSelectedIndex(0);
		int selected=select.getSelectedIndex();
		assertEquals(0, selected);
		
		select.setSelectedIndex(2);
		selected=select.getSelectedIndex();
		assertEquals(2, selected);
		
		select.setSelectedIndex(3);//out of range
		selected=select.getSelectedIndex();
		assertEquals(-1, selected);
	}
	
	@Test public void tgetLabels0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<select id='x'>"
				+ "			<option value='a'>alpha</option>"
				+ "			<option value='b'>beta</option>"
				+ "			<option value='c'>gamma</option>"
				+ "		</select>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLBodyElement body=d.getBody();
		HTMLSelectElement select=(HTMLSelectElement)d.getElementById("x");

		// no labels
		HTMLCollection<HTMLLabelElement>labels=select.getLabels();
		assertEquals(0, labels.getLength());
		
		// a label for for attribute
		HTMLLabelElement label=d.createElement(HTMLLabelElement.class);
		label.setHtmlFor("x");
		body.appendChild(label);
		labels=select.getLabels();
		assertEquals(1, labels.getLength());
		assertEquals(label, labels.item(0));
		
		// a surrounding label
		label=d.createElement(HTMLLabelElement.class);
		body.appendChild(label);
		select.remove();
		label.appendChild(select);
		labels=select.getLabels();
		assertEquals(2, labels.getLength());
		assertEquals(label, labels.item(1));
		
		// a surrounding label, but select is not first child
		HTMLDivElement div=d.createElement(HTMLDivElement.class);
		label.prepend(div);//div is first child
		div=d.createElement(HTMLDivElement.class);
		label.appendChild(div);
		div.appendChild(select);
		//select is child of second div
		labels=select.getLabels();
		assertEquals(2, labels.getLength());
		assertEquals(label, labels.item(1));
	}
	
	@Test public void tGetForm0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<form id='form'>"
				+ "			<select id='a'>alpha</select>"
				+ "		</datalist>"
				+ "	</body>"
				+ "	<select id='b' form='form'>alpha</select>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLFormElement form=(HTMLFormElement) d.getElementById("form");
		ReassociateableElement a=(ReassociateableElement) d.getElementById("a");
		ReassociateableElement b=(ReassociateableElement) d.getElementById("b");
		assertEquals(form, a.getForm());
		assertEquals(form, b.getForm());
	}

	// helpers -------------------------------------------------
	
	private HTMLOptionElement option(HTMLDocument d, String value){
		HTMLOptionElement option=(HTMLOptionElement) d.createElement("option");
		option.setValue(value);
		return option;
	}
}
