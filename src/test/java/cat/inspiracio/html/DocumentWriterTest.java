package cat.inspiracio.html;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.StringWriter;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;

public class DocumentWriterTest extends AbstractTest{

    private StringWriter writer;
    private DocumentWriter dw;
    
    @Before public void before(){
        writer=new StringWriter();
        dw=new DocumentWriter(writer);
    }
    @After public void after(){}

    void assertContains(String s){
        String result=writer.toString();
        assertTrue(s, result.contains(s));
    }
    
    /** No doctype: doesn't fail. */
    @Test public void tdoctype() throws Exception{
        Document d=parse("<html/>");
        DocumentType type=d.getDoctype();
        assertNull(type);
        dw.document(d);
    }
    
    /** doctype: always html5. */
    @Test public void tdoctype1() throws Exception{
        Document d=parse("<html/>");
        DocumentType type=d.getDoctype();
        assertNull(type);
        dw.document(d);
        assertContains("<!DOCTYPE html>");
    }
    
    /** script element: no escaping. */
    @Test public void tscript()throws Exception{
        Document d=parse("<script> 0<1 && 1>0 </script>");
        dw.document(d);
        assertContains("<script> 0<1 && 1>0 </script>");
    }

    /** element without children: closed tag. */
    @Test public void tclosed1()throws Exception{
        Document d=parse("<p id='bla'></p>");
        dw.document(d);
        assertContains("<p id=\"bla\"></p>");
    }


    /** element without children: a has separate close tag. */
    @Test public void tclosedA()throws Exception{
        Document d=parse("<a></a>");
        dw.document(d);
        assertContains("<a></a>");
    }

    @Test public void tclosedDiv()throws Exception{
        Document d=parse("<div></div>");
        dw.document(d);
        assertContains("<div></div>");
    }

    @Test public void tclosedIframe()throws Exception{
        Document d=parse("<iframe></iframe>");
        dw.document(d);
        assertContains("<iframe></iframe>");
    }

    @Test public void tclosedNav()throws Exception{
        Document d=parse("<nav></nav>");
        dw.document(d);
        assertContains("<nav></nav>");
    }

    @Test public void tclosedP()throws Exception{
        Document d=parse("<p></p>");
        dw.document(d);
        assertContains("<p></p>");
    }

    /** element without children: script has separate close tag. */
    @Test public void tclosedScript()throws Exception{
        Document d=parse("<script></script>");
        dw.document(d);
        assertContains("<script></script>");
    }

    /** element without children: textarea has separate close tag. */
    @Test public void tclosedTextarea()throws Exception{
        Document d=parse("<textarea></textarea>");
        dw.document(d);
        assertContains("<textarea></textarea>");
    }
    
    /** element without children: title has separate close tag. */
    @Test public void tclosedTitle()throws Exception{
        Document d=parse("<title></title>");
        dw.document(d);
        assertContains("<title></title>");
    }


    /** normal attribute in double quotes */
    @Test public void tattribute()throws Exception{
        Document d=parse("<p id=\"bla\"/>");
        dw.document(d);
        assertContains("<p id=\"bla\"></p>");
    }
    
    /** normal attribute in single quotes: write double quotes */
    @Test public void tattribute1()throws Exception{
        Document d=parse("<p id='bla'/>");
        dw.document(d);
        assertContains("<p id=\"bla\"></p>");
    }
    
    /** attribute without value */
    @Test public void tattribute3()throws Exception{
        Document d=parse("<input checked/>");
        dw.document(d);
        assertContains("<input checked/>");
    }
    
    /** attribute with empty value: just the key */
    @Test public void tattribute4()throws Exception{
        Document d=parse("<input checked=''/>");
        dw.document(d);
        assertContains("<input checked/>");
    }
    
    /** attribute with " in value: escape */
    @Test public void tattribute5()throws Exception{
        Document d=parse("<input name='\"'/>");
        dw.document(d);
        assertContains("<input name=\"&quot;\"/>");
    }
    
    /** attribute with & in value: survives */
    @Test public void tattribute6()throws Exception{
        Document d=parse("<img href='image.jpg?x=1&id=13'/>");
        dw.document(d);
        assertContains("<img href=\"image.jpg?x=1&id=13\"/>");
    }
    
    /** cdata: the parser does not ignore them */
    @Ignore
    @Test public void tcdata()throws Exception{
        Document d=parse("<p><![CDATA[hello]]></p>");
        dw.document(d);
        assertContains("<p><![CDATA[hello]]></p>");
    }
    
    /** cdata: escape ]]> by adding another cdata section */
    @Test public void tcdata1()throws Exception{
        Document d=parse("<p id='cdata'></p>");
        CDATASection cdata=d.createCDATASection("]]>");
        Element p=d.getElementById("cdata");
        p.appendChild(cdata);
        dw.document(d);
        assertContains("<p id=\"cdata\"><![CDATA[]]]]><![CDATA[>]]></p>");
    }
    
    /** comment: disappears */
    @Test public void tcomment()throws Exception{
        Document d=parse("<p><!--hello--></p>");
        dw.document(d);
        assertContains("<p></p>");
    }
    
    /** comment: disappears */
    @Test public void tcomment1()throws Exception{
        Document d=parse("<div><!--hello--></div>");
        dw.document(d);
        assertContains("<div></div>");
    }
    
    /** text: just normal text */
    @Test public void ttext()throws Exception{
        Document d=parse("<p>ü</p>");
        dw.document(d);
        assertContains("<p>ü</p>");
    }
    
    /** text: escape & */
    @Test public void ttext0()throws Exception{
        Document d=parse("<p>&</p>");
        dw.document(d);
        assertContains("<p>&amp;</p>");
    }
    
    /** text: escape < */
    @Test public void ttext1()throws Exception{
        Document d=parse("<p><</p>");
        dw.document(d);
        assertContains("<p>&lt;</p>");
    }
    
}
