/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static java.lang.Double.NEGATIVE_INFINITY;
import static java.lang.Double.POSITIVE_INFINITY;
import static java.lang.Math.PI;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Date;

import javax.script.ScriptException;

import org.junit.Test;
import org.w3c.dom.Node;

import cat.inspiracio.dom.InvalidStateError;
import cat.inspiracio.html.host.Document;
import cat.inspiracio.script.TypeError;

public class HTMLInputElementTest extends AbstractTest{

	@Test public void tclone() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "a=document.createElement('input');"
				+ "a.id='x';"
				+ "document.body.appendChild(a);"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;
		HTMLElement e=d.getElementById("x");
		Node n=e.cloneNode(true);
		assertType(n, HTMLInputElement.class);
	}
	
	@Test public void tGetForm0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<form id='form'>"
				+ "			<input id='a'>alpha</input>"
				+ "		</datalist>"
				+ "	</body>"
				+ "	<input id='b' form='form'>alpha</input>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLFormElement form=(HTMLFormElement) d.getElementById("form");
		ReassociateableElement a=(ReassociateableElement) d.getElementById("a");
		ReassociateableElement b=(ReassociateableElement) d.getElementById("b");
		assertEquals(form, a.getForm());
		assertEquals(form, b.getForm());
	}
	
	@Test public void tformenctype(){
		HTMLDocument d=new Document();
		
		HTMLInputElement input=d.createElement(HTMLInputElement.class);
		assertEquals("", input.getFormEncType());
		
		input.setFormEncType("multipart/form-data");
		assertEquals("multipart/form-data", input.getFormEncType());
		
		//invalid default
		input.setFormEncType("invalid");
		assertEquals("application/x-www-form-urlencoded", input.getFormEncType());
	}

    @Test public void tdate0(){
        HTMLInputElement input=input("text");//not correct for getvalueAsDate
        input.setValue("2015-10-30");
        assertNull(input.getValueAsDate());
    }

    @Test public void tdate1(){
        HTMLInputElement input=input("date");
        input.setValue("bad");
        Date date=input.getValueAsDate();
        assertNull(date);
    }

    @Test public void tdate2(){
        HTMLInputElement input=input("date");
        input.setValue("2015-06-15");
        String v=input.getValue();
        assertEquals("2015-06-15", v);
    }

    @Test public void tdate3(){
        HTMLInputElement input=input("date");
        input.setValue("2015-06-15");
        Date date=input.getValueAsDate();
        Date exp=date(2015, 6, 15);
        assertEquals(exp, date);
    }

    @Test(expected=InvalidStateError.class)
    public void tdate4(){
        HTMLInputElement input=input();
        Date date=date(2015, 6, 12);
        input.setValueAsDate(date);//InvalidStateError
        assertEquals("2015-06-12", input.getValue());
        assertEquals(date, input.getValueAsDate());
    }

    @Test public void tdate5(){
        HTMLInputElement input=input("date");
        Date date=date(2015, 6, 12);
        input.setValueAsDate(date);
        assertEquals("2015-06-12", input.getValue());
        assertEquals(date, input.getValueAsDate());
    }
    
    @Test public void tnumber0(){
        HTMLInputElement input=input();
        double v=input.getValueAsNumber();
        assertNaN(v);
    }

    @Test public void tnumber1(){
        HTMLInputElement input=input("date");
        Date date=date(2015, 10, 30);
        input.setValueAsDate(date);
        double time=date.getTime();
        double number=input.getValueAsNumber();
        assertEquals(time, number, 0.0000000000001);
    }

    @Test public void tnumber2(){
        HTMLInputElement input=input("time");
        double v=input.getValueAsNumber();
        assertTrue(Double.isNaN(v));
    }

    @Test public void tnumber3(){
        HTMLInputElement input=input("number");
        input.setValueAsNumber(PI);
        assertTrue(PI==input.getValueAsNumber());
    }

    @Test public void tnumber4(){
        HTMLInputElement input=input("number");
        input.setValue("3.14");
        assertTrue(3.14==input.getValueAsNumber());
    }

    @Test(expected=TypeError.class)
    public void tnumber5(){
        HTMLInputElement input=input();
        input.setValueAsNumber(POSITIVE_INFINITY);
    }

    @Test(expected=TypeError.class)
    public void tnumber6(){
        HTMLInputElement input=input();
        input.setValueAsNumber(NEGATIVE_INFINITY);
    }

    @Test(expected=InvalidStateError.class)
    public void tnumber7(){
        HTMLInputElement input=input("radio");
        input.setValueAsNumber(3.14);
    }

    @Test public void tnumber8(){
        HTMLInputElement input=input("number");
        input.setValueAsNumber(Double.NaN);
        String v=input.getValue();
        assertEquals("", v);
    }

    @Test public void tnumber9(){
        HTMLInputElement input=input("date");
        Date date=date(2015, 10, 30);
        long number=date.getTime();
        input.setValueAsNumber(number);
        String v=input.getValue();
        assertEquals("2015-10-30", v);
    }

    @Test public void tnumber10(){
        HTMLInputElement input=input("number");
        input.setValueAsNumber(3.14);
        String v=input.getValue();
        assertEquals("3.14", v);
    }

    @Test(expected=InvalidStateError.class)
    public void tstep0(){
        HTMLInputElement input=input("radio");
        input.stepDown();
    }

    @Test(expected=InvalidStateError.class)
    public void tstep1(){
        HTMLInputElement input=input("checkbox");
        input.stepDown(2);
    }

    @Test(expected=InvalidStateError.class)
    public void tstep2(){
        HTMLInputElement input=input("text");
        input.stepUp();
    }

    @Test(expected=InvalidStateError.class)
    public void tstep3(){
        HTMLInputElement input=input("password");
        input.stepUp(2);
    }

    @Test(expected=InvalidStateError.class)
    public void tstep4(){
        HTMLInputElement input=input("hidden");
        input.stepUp(2);
    }

    @Test(expected=InvalidStateError.class)
    public void tstep5(){
        HTMLInputElement input=input("search");
        input.stepUp(2);
    }

    @Test(expected=InvalidStateError.class)
    public void tstep6(){
        HTMLInputElement input=input("url");
        input.stepUp(2);
    }

    @Test(expected=InvalidStateError.class)
    public void tstep7(){
        HTMLInputElement input=input("tel");
        input.stepUp(2);
    }

    @Test(expected=InvalidStateError.class)
    public void tstep8(){
        HTMLInputElement input=input("email");
        input.stepUp(2);
    }

    @Test(expected=InvalidStateError.class)
    public void tstep9(){
        HTMLInputElement input=input("color");
        input.stepUp(2);
    }

    @Test(expected=InvalidStateError.class)
    public void tstep10(){
        HTMLInputElement input=input("file");
        input.stepUp(2);
    }

    @Test(expected=InvalidStateError.class)
    public void tstep11(){
        HTMLInputElement input=input("submit");
        input.stepUp(2);
    }

    @Test(expected=InvalidStateError.class)
    public void tstep12(){
        HTMLInputElement input=input("image");
        input.stepUp(2);
    }

    @Test(expected=InvalidStateError.class)
    public void tstep13(){
        HTMLInputElement input=input("reset");
        input.stepUp(2);
    }

    @Test(expected=InvalidStateError.class)
    public void tstep14(){
        HTMLInputElement input=input("button");
        input.stepUp(2);
    }

    // Testing stepUp() ---------------------
    
    // Tests based on http://mxr.mozilla.org/mozilla-central/source/dom/html/test/forms/test_stepup_stepdown.html?force=1
    
    private static final boolean up=false;
    private static final boolean down=true;
    
    //regular case
    @Test public void numberStep0(){n("1", null, null, null, null, "2", null, up);}
    //argument testing
    @Test public void numberStep1(){n("1", null, null, null, 1, "2", null, false);}
    @Test public void numberStep2(){n("9", null, null, null, 9, "18", null, false);}
    @Test public void numberStep3(){n("1", null, null, null, -1, "0", null, false);}
    @Test public void numberStep4(){n("1", null, null, null, 0, "1", null, false);}
    //float values for n are rounded to integer 1.1 -> 1
    //@Test public void numberStep5(){n("1", null, null, null, 1.1, "2", false, false);}
    //with step values
    @Test public void numberStep6(){n("1", "0.5", null, null, null, "1.5", null, false);}
    @Test public void numberStep7(){n("1", "0.25", null, null, 4, "2", null, up);}
    //@Test public void numberStep8(){n(null, "0.5", null, null, null, "0.5", false, false);}
    @Test public void numberStep9(){n("1", "0.25", null, null, 4, "2", null, false);}
    //step=0 isn't allowed. -> step=1
    @Test public void numberStep10(){n("1", "0", null, null, null, "2", null, false);}
    //step<0 isn't allowed. -> step=1
    @Test public void numberStep11(){n("1", "-1", null, null, null, "2", null, false);}
    //step=NaN isn't allowed. -> step=1
    @Test public void numberStep12(){n("1", "foo", null, null, null, "2", null, false);}
    //min values testing
    @Test public void numberStep13(){n("1", "1", "foo", null, null, "2", null, false);}
    @Test public void numberStep14(){n("1", null, "-10", null, null, "2", null, false);}
    @Test public void numberStep15(){n("1", null, "0", null, null, "2", null, false);}
    @Test public void numberStep16(){n("1", null, "10", null, null, "10", null, false);}
    @Test public void numberStep17(){n("1", null, "2", null, null, "2", null, false);}
    @Test public void numberStep18(){n("1", null, "1", null, null, "2", null, false);}
    @Test public void numberStep19(){n("0", null, "4", null, 5, "5", null, false);}
    @Test public void numberStep20(){n("0", "2", "5", null, 3, "5", null, false);}
    //max values testing
    @Test public void numberStep21(){n("1", "1", null, "foo", null, "2", null, false);}
    @Test public void numberStep22(){n("1", null, null, "10", null, "2", null, false);}
    @Test public void numberStep23(){n("1", null, null, "0", null, "1", null, false);}
    @Test public void numberStep24(){n("1", null, null, "-10", null, "1", null, false);}
    @Test public void numberStep25(){n("1", null, null, "1", null, "1", null, false);}
    
    //stepUp() value is in range but out of step. There is no higher value.
    @Test public void numberStep26(){n("-3", "5", "-10", "-3", null, "-3", null, up);}//not -5. FF -3. Error in FF/spec.
    
    //step mismatch
    @Test public void numberStep27(){n("1", "2", "0", null, null, "2", null, false);}
    
    //stepUp(2) value=1 step=2 min=0 Initially out of step. 
    @Test public void numberStep28(){n("1", "2", "0", null, 2, "4", null, up);}//not 2. FF 4.
    
    @Test public void numberStep29(){n("8", "2", null, "9", null, "8", null, false);}
    @Test public void numberStep30(){n("-3", "2", null, null, null, "-1", null, false);}
    @Test public void numberStep31(){n("9", "3", "-10", null, null, "11", null, false);}
    @Test public void numberStep32(){n("7", "3", "-10", null, null, "8", null, false);}
    @Test public void numberStep33(){n("7", "3", "5", null, null, "8", null, false);}
    @Test public void numberStep34(){n("9", "4", "3", null, null, "11", null, false);}
    @Test public void numberStep35(){n("-2", "3", "-6", null, null, "0", null, false);}
    @Test public void numberStep36(){n("7", "3", "6", null, null, "9", null, false);}
    //Clamping
    @Test public void numberStep37(){n("1", "2", "0", "3", null, "2", null, up);}
    
    //stepUp(10) value=0 step=5 min=1 max=8 Initially out of step. 
    @Test public void numberStep38(){n("0", "5", "1", "8", 10, "6", null, up);}//not 1. FF 6.
    
    //stepUp(5) value=-9 step=3 min=-8 max=-1 Initially below range and out of step.
    @Test public void numberStep39(){n("-9", "3", "-8", "-1", 5, "-2", null, up);}//not -8. FF -2.
    
    //stepUp(15) value=-9 step=3 min=8 max==15 Initially below range and out of step.
    @Test public void numberStep40(){n("-9", "3", "8", "15", 15, "14", null, up);}//not 8. FF 14.
    
    @Test public void numberStep41(){n("-1", "3", "-1", "4", 3, "2", null, false);}
    @Test public void numberStep42(){n("-3", "2", "-6", "-2", null, "-2", null, up);}
    @Test public void numberStep43(){n("-3", "2", "-6", "-1", null, "-2", null, up);}
    
    // value="" (NaN)
    @Test public void numberStep44(){n("", null, null, null, null, "1", null, up);}
    @Test public void numberStep441(){n("", null, null, null, null, "1", null, up);}
    @Test public void numberStep442(){n("", "2", null, null, null, "2", null, up);}
    @Test public void numberStep443(){n("", "2", "3", null, null, "3", null, up);}
    @Test public void numberStep444(){n("", null, "3", null, null, "3", null, up);}
    @Test public void numberStep445(){n("", "2", "3", "8", null, "3", null, up);}
    @Test public void numberStep446(){n("", null, "-10", "10", null, "1", null, up);}
    @Test public void numberStep447(){n("", "3", "-10", "10", null, "2", null, up);}
    // with step="any"
    @Test public void numberStep45(){n("0", "any", null, null, 1, null, InvalidStateError.class, false);}
    @Test public void numberStep46(){n("0", "ANY", null, null, 1, null, InvalidStateError.class, false);}
    @Test public void numberStep47(){n("0", "AnY", null, null, 1, null, InvalidStateError.class, false);}
    @Test public void numberStep48(){n("0", "aNy", null, null, 1, null, InvalidStateError.class, up);}
    // with @value = step base
    @Test public void numberStep481(){n("1", "2", null, null, null, "3", null, up);}

    // stepDown tests
    /* Initial value | step | min | max | n | final value | exception */
    // Regular case.
    @Test public void numberStep49(){n("1",   null,  null,  null,  null, "0",    null, down);}
    // Argument testing.
    @Test public void numberStep50(){n("1",   null,  null,  null,  1,    "0",    null , true);}
    @Test public void numberStep51(){n("9",   null,  null,  null,  9,    "0",    null , true);}
    @Test public void numberStep52(){n("1",   null,  null,  null,  -1,   "2",    null , true);}
    @Test public void numberStep53(){n("1",   null,  null,  null,  0,    "1",    null , true);}
    // Float values are rounded to integer (1.1 -> 1).
    //@Test public void numberStep54(){n("1",   null,  null,  null,  1.1,  "0",    null , true);}
    // With step values.
    @Test public void numberStep55(){n("1",  "0.5",  null,  null,  null, "0.5",  null , true);}
    @Test public void numberStep56(){n("1",  "0.25", null,  null,  4,    "0",    null , true);}
    // step = 0 isn't allowed (-> step = 1).
    @Test public void numberStep57(){n("1",  "0",    null,  null,  null, "0",    null , true);}
    // step < 0 isn't allowed (-> step = 1).
    @Test public void numberStep58(){n("1",  "-1",   null,  null,  null, "0",    null , true);}
    // step = NaN isn't allowed (-> step = 1).
    @Test public void numberStep59(){n("1",  "foo",  null,  null,  null, "0",    null , true);}
    // Min values testing.
    @Test public void numberStep60(){n("1",  "1",    "foo", null,  null, "0",    null, true);}
    @Test public void numberStep61(){n("1",  null,   "-10", null,  null, "0",    null, true);}
    @Test public void numberStep62(){n("1",  null,   "0",   null,  null, "0",    null, true);}
    @Test public void numberStep63(){n("1",  null,   "10",  null,  null, "1",    null, down);}//not 10. FF 1.
    @Test public void numberStep64(){n("1",  null,   "2",   null,  null, "1",    null, true);}//not 2. FF 1.
    @Test public void numberStep65(){n("1",  null,   "1",   null,  null, "1",    null, true);}
    // Max values testing.
    @Test public void numberStep66(){n("1",  "1",    null,  "foo", null, "0",    null, true);}
    @Test public void numberStep67(){n("1",  null,   null,  "10",  null, "0",    null, true);}
    @Test public void numberStep68(){n("1",  null,   null,  "0",   null, "0",    null, true);}
    @Test public void numberStep69(){n("1",  null,   null,  "-10", null, "-10",  null, true);}
    @Test public void numberStep70(){n("1",  null,   null,  "1",   null, "0",    null, true);}
    @Test public void numberStep71(){n("5",  null,   null,  "3",   3,  "2",    null, true);}
    @Test public void numberStep72(){n("5",  "2",    "-6",  "3",   2,  "2",    null, true);}
    @Test public void numberStep73(){n("-3", "5",    "-10", "-3",  null, "-5",   null, true);}
    
    // Step mismatch.
    @Test public void numberStep74(){n("1",  "2",    "-2",  null,  null, "0",    null, true);}
    @Test public void numberStep75(){n("3",  "2",    "-2",  null,  null, "2",    null, true);}
    
    //stepDown(2) value=3 step=2 min=-2 Initially out of step.
    @Test public void numberStep76(){n("3",  "2",    "-2",  null,  2,  "0",    null, down);}//not 2. FF 0.
    
    //stepDown(-2) value=3 step=2 min=-2 Initially out of step.
    @Test public void numberStep77(){n("3",  "2",    "-2",  null,  -2, "6",    null, down);}//not 2. FF 6.
    
    @Test public void numberStep78(){n("1",  "2",    "-6",  null,  null, "0",    null, true);}
    @Test public void numberStep79(){n("1",  "2",    "-2",  null,  null, "0",    null, true);}
    @Test public void numberStep80(){n("1",  "3",    "-6",  null,  null, "0",    null, true);}
    @Test public void numberStep81(){n("2",  "3",    "-6",  null,  null, "0",    null, true);}
    @Test public void numberStep82(){n("2",  "3",    "1",   null,  null, "1",    null , true);}
    @Test public void numberStep83(){n("5",  "3",    "1",   null,  null, "4",    null , true);}
    @Test public void numberStep84(){n("3",  "2",    "-6",  null,  null, "2",    null , true);}
    @Test public void numberStep85(){n("5",  "2",    "-6",  null,  null, "4",    null , true);}
    @Test public void numberStep86(){n("6",  "2",   "1",    null,  null, "5",    null , true);}
    @Test public void numberStep87(){n("8",  "3",   "1",    null,  null, "7",    null , true);}
    @Test public void numberStep88(){n("9",  "2",   "-10",  null,  null, "8",    null , true);}
    @Test public void numberStep89(){n("7",  "3",   "-10",  null,  null, "5",    null , true);}
    @Test public void numberStep90(){n("-2", "3",   "-10",  null,  null, "-4",   null , true);}
    // Clamping.
    @Test public void numberStep91(){n("0",  "2",    "-1",  null,  null, "-1",   null , true);}
    @Test public void numberStep92(){n("10", "2",    "0",   "4",   10, "0",    null , true);}
    @Test public void numberStep93(){n("10", "2",    "0",   "4",   5,  "0",    null , true);}
    // value = "" (NaN).
    @Test public void numberStep94(){n("",   null,   null,  null,  null, "-1",   null, true);}
    @Test public void numberStep95(){n("",    "2",   null,  null,  null, "-2",   null , true);}
    @Test public void numberStep96(){n("",    "2",    "3",  null,  null,  "3",   null , down);}//FF 3.
    @Test public void numberStep97(){n("",   null,    "3",  null,  null,  "3",   null , down);}//FF 3.
    @Test public void numberStep98(){n("",    "2",    "3",   "8",  null,  "3",   null , true);}//FF 3.
    @Test public void numberStep99(){n("",   null,  "-10",  "10",  null, "-1",   null , true);}
    @Test public void numberStep100(){n("",    "3",  "-10",  "10",  null, "-1",   null , true);}//not -4. FF -1.
    // With step = "any".
    @Test public void numberStep101(){n("0",  "any",  null,  null,  1,    null,   InvalidStateError.class , true);}
    @Test public void numberStep102(){n("0",  "ANY",  null,  null,  1,    null,   InvalidStateError.class, true);}
    @Test public void numberStep103(){n("0",  "AnY",  null,  null,  1,    null,   InvalidStateError.class, true);}
    @Test public void numberStep104(){n("0",  "aNy",  null,  null,  1,    null,   InvalidStateError.class, down);}
    // With @value = step base.
    @Test public void numberStep105(){n("1",  "2",    null,  null,  null, "-1",   null, down);}

    /** step test for type=number
     * @param initial value
     * @param step or null
     * @param min or null
     * @param max or null
     * @param n or null
     * @param expected final value
     * @param exception Should it throw an exception? 
     * @param down Downwards? (or up?) */
    private void n(String initial, String step, String min, String max, Integer n, String expected, Class<? extends Exception> exception, boolean down){
    	HTMLInputElement input=input("number");
    	if(initial!=null && 0<initial.length())input.setValue(initial);
    	if(step!=null)input.setStep(step);
    	if(min!=null)input.setMin(min);
    	if(max!=null)input.setMax(max);
    	try{
    		if(n==null){
    			if(down)
    				input.stepDown();
    			else
    				input.stepUp();
    		}else{
    			if(down)
    				input.stepDown(n);
    			else
    				input.stepUp(n);
    		}
    		String v=input.getValue();
    		if(exception==null)
    			assertEquals(expected, v);
    		else
    			fail("Expected " + exception);
    	}
    	catch(Exception e){
    		if(exception==null)
    			fail("Unexpected " + e);
    		else
    			assertType(e, exception);	
    	}
    }
    
    // stepDown range 
    
    // Regular case.
    @Test public void rangeStep000(){r("1",   null,  null,  null,  null, "0",    null, down);}
    // Argument testing.
    @Test public void rangeStep001(){r( "1",   null,  null,  null,  1,    "0",    null, down);}
    @Test public void rangeStep002(){r( "9",   null,  null,  null,  9,    "0",    null, down);}
    @Test public void rangeStep003(){r( "1",   null,  null,  null,  -1,   "2",    null, down);}
    @Test public void rangeStep004(){r( "1",   null,  null,  null,  0,    "1",    null, down);}
    // Float values are rounded to integer (1.1 -> 1).
    //@Test public void rangeStep005(){r( "1",   null,  null,  null,  1.1,  "0",    null, down);}
    // With step values.
    @Test public void rangeStep006(){r( "1",  "0.5",  null,  null,  null, "0.5",  null, down);}
    @Test public void rangeStep007(){r( "1",  "0.25", null,  null,  4,    "0",    null, down);}
    // step = 0 isn't allowed (-> step = 1).
    @Test public void rangeStep008(){r( "1",  "0",    null,  null,  null, "0",    null, down);}
    // step < 0 isn't allowed (-> step = 1).
    @Test public void rangeStep009(){r( "1",  "-1",   null,  null,  null, "0",    null, down);}
    // step = NaN isn't allowed (-> step = 1).
    @Test public void rangeStep010(){r( "1",  "foo",  null,  null,  null, "0",    null, down);}
    // Min values testing.
    @Test public void rangeStep011(){r( "1",  "1",    "foo", null,  null, "0",    null, down);}
    @Test public void rangeStep012(){r( "1",  null,   "-10", null,  null, "0",    null, down);}
    @Test public void rangeStep013(){r( "1",  null,   "0",   null,  null, "0",    null, down);}
    
    //stepDown() value=1 min=10 Already below min. FF raises to min. Discrepancy with FF.
    @Test public void rangeStep014(){r( "1",  null,   "10",  null,  null, "1",   null, down);}//not 1. FF 10.
    
    //stepDown() value=1 min=2 Already below min. FF raises to min. Discrepancy with FF.
    @Test public void rangeStep015(){r( "1",  null,   "2",   null,  null, "1",    null, down);}//not 1. FF 2.
    
    @Test public void rangeStep016(){r( "1",  null,   "1",   null,  null, "1",    null, down);}
    // Max values testing.
    @Test public void rangeStep017(){r( "1",  "1",    null,  "foo", null, "0",    null, down);}
    @Test public void rangeStep018(){r( "1",  null,   null,  "10",  null, "0",    null, down);}
    
    //stepDown() value=1 max=0 Starting above max. FF lowers one step and reaches max.
    @Test public void rangeStep019(){r( "1",  null,   null,  "0",   null, "0",    null, down);}
    
    //stepDown() value=1 max=-10 Starting above max. FF lowers one step, still above max. Discrepancy with FF.
    @Test public void rangeStep020(){r( "1",  null,   null,  "-10", null, "1",    null, down);}//not 1.
    
    @Test public void rangeStep021(){r( "1",  null,   null,  "1",   null, "0",    null, down);}
    
    //stepDown(3) value=5 max=3 Starting above max. Can't explain it. Discrepancy with FF.
    @Test public void rangeStep022(){r( "5",  null,   null,  "3",   3,  "2",    null, down);}//not 2. FF 0.
    
    //stepDown(2) value=5 step=2 min=-6 max=3 Starting above max, out of step. Can't explain it. Discrepancy with FF.
    @Test public void rangeStep023(){r( "5",  "2",    "-6",  "3",   2,  "2",   null, down);}//not 2. FF -2.
    
    //stepDown() value=-3 step=5 min=-10 max=-3 Starting at max, out of step. Can't explain it. Discrepancy with FF.
    @Test public void rangeStep024(){r( "-3", "5",    "-10", "-3",  null, "-5",  null, down);}//not -5.
    
    // Step mismatch.
    @Test public void rangeStep025(){r( "1",  "2",    "-2",  null,  null, "0",    null, down);}
    @Test public void rangeStep026(){r( "3",  "2",    "-2",  null,  null, "2",    null, down);}
    @Test public void rangeStep027(){r( "3",  "2",    "-2",  null,  2,  "0",    null, down);}
    
    //stepDown(-2) -> stepUp(2) value=3 step=2 min=-2 Out of step. Can't explain. Discrepancy with FF.
    @Test public void rangeStep028(){r( "3",  "2",    "-2",  null,  -2, "6",    null, down);}//not 6. FF 8.
    
    @Test public void rangeStep029(){r( "1",  "2",    "-6",  null,  null, "0",    null, down);}
    @Test public void rangeStep030(){r( "1",  "2",    "-2",  null,  null, "0",    null, down);}
    
    //stepDown() value=1 step=3 min=-6 Out of step. Discrepancy with FF.
    @Test public void rangeStep031(){r( "1",  "3",    "-6",  null,  null, "0",   null, down);}//not 0. FF -3.
    
    @Test public void rangeStep032(){r( "2",  "3",    "-6",  null,  null, "0",    null, down);}
    @Test public void rangeStep033(){r( "2",  "3",    "1",   null,  null, "1",    null, down);}
    
    //stepDown() value=5 step=3 min=1 Out of step. Discrepancy with FF.
    @Test public void rangeStep034(){r( "5",  "3",    "1",   null,  null, "4",    null, down);}//not 4. FF 1.
    
    @Test public void rangeStep035(){r( "3",  "2",    "-6",  null,  null, "2",    null, down);}
    @Test public void rangeStep036(){r( "5",  "2",    "-6",  null,  null, "4",    null, down);}
    @Test public void rangeStep037(){r( "6",  "2",   "1",    null,  null, "5",    null, down);}
    
    //stepDown() value=8 step=3 min=1 Out of step. Discrepancy with FF.
    @Test public void rangeStep038(){r( "8",  "3",   "1",    null,  null, "7",    null, down);}//not 7. FF 4.
    
    @Test public void rangeStep039(){r( "9",  "2",   "-10",  null,  null, "8",    null, down);}
    @Test public void rangeStep040(){r( "7",  "3",   "-10",  null,  null, "5",    null, down);}
    @Test public void rangeStep041(){r( "-2", "3",   "-10",  null,  null, "-4",   null, down);}
    // Clamping.
    @Test public void rangeStep042(){r( "0",  "2",    "-1",  null,  null, "-1",   null, down);}
    @Test public void rangeStep043(){r( "10", "2",    "0",   "4",   10, "0",    null, down);}
    @Test public void rangeStep044(){r( "10", "2",    "0",   "4",   5,  "0",    null, down);}
    
    // value = "" (default will be 50).
    //stepDown() value="" Default value for range is broken.
    @Test public void rangeStep045(){r( "",   null,   null,  null,  null, "49",   null, down);}
    
    // With step = "any".
    @Test public void rangeStep046(){r( "0",  "any",  null,  null,  1,    null,   InvalidStateError.class, down);}
    @Test public void rangeStep047(){r( "0",  "ANY",  null,  null,  1,    null,   InvalidStateError.class, down);}
    @Test public void rangeStep048(){r( "0",  "AnY",  null,  null,  1,    null,   InvalidStateError.class, down);}
    @Test public void rangeStep049(){r( "0",  "aNy",  null,  null,  1,    null,   InvalidStateError.class, down);}
    // With @value = step base.
    //stepDown() value=1 step=2 Can't explain it. Discrepancy with FF.
    @Test public void rangeStep050(){r( "1",  "2",    null,  null,  null, "0",    null, down);}//not 0. FF 1.

    //stepUp range
    
    // Regular case.
    @Test public void rangeStep051(){r( "1",   null,  null,  null,  null, "2",   null, up);}
    // Argument testing.
    @Test public void rangeStep052(){r( "1",   null,  null,  null,  1,    "2",   null, up);}
    @Test public void rangeStep053(){r( "9",   null,  null,  null,  9,    "18",  null, up);}
    @Test public void rangeStep054(){r( "1",   null,  null,  null,  -1,   "0",   null, up);}
    @Test public void rangeStep055(){r( "1",   null,  null,  null,  0,    "1",   null, up);}
    // Float values are rounded to integer (1.1 -> 1).
    //@Test public void rangeStep056(){r( "1",   null,  null,  null,  1.1,  "2",   null, up);}
    // With step values.
    @Test public void rangeStep057(){r( "1",  "0.5",  null,  null,  null, "1.5", null, up);}
    @Test public void rangeStep058(){r( "1",  "0.25", null,  null,  4,    "2",   null, up);}
    // step = 0 isn't allowed (-> step = 1).
    @Test public void rangeStep059(){r( "1",  "0",    null,  null,  null, "2",   null, up);}
    // step < 0 isn"t allowed (-> step = 1).
    @Test public void rangeStep060(){r( "1",  "-1",   null,  null,  null, "2",   null, up);}
    // step = NaN isn"t allowed (-> step = 1).
    @Test public void rangeStep061(){r( "1",  "foo",  null,  null,  null, "2",   null, up);}
    // Min values testing.
    @Test public void rangeStep062(){r( "1",  "1",    "foo", null,  null, "2",   null, up);}
    @Test public void rangeStep063(){r( "1",  null,   "-10", null,  null, "2",   null, up);}
    @Test public void rangeStep064(){r( "1",  null,   "0",   null,  null, "2",   null, up);}
    
    //stepUp() value=1 min=10 Below min. FF raises to min and does one step. Discrepancy with FF.
    @Test public void rangeStep065(){r( "1",  null,   "10",  null,  null, "10",  null, up);}//not 10. FF 11.
    
    //stepUp() value=1 min=2 Below min. FF raises to min and does one step. Discrepancy with FF.
    @Test public void rangeStep066(){r( "1",  null,   "2",   null,  null, "2",   null, up);}//not 2. FF 3.
    
    @Test public void rangeStep067(){r( "1",  null,   "1",   null,  null, "2",   null, up);}
    
    //stepUp(5) value=0 min=4 Below min. FF raises to min and then does the 5 steps. Discrepancy with FF.
    @Test public void rangeStep068(){r( "0",  null,   "4",   null,  5,  "5",   null, up);}//not 5. FF 9.
    
    //stepUp(3) value=0 step=2 min=5. Below min. FF raises to min and then does the 3 steps. Discrepancy with FF.
    @Test public void rangeStep069(){r( "0",  "2",    "5",   null,  3,  "5",  null, up);}//not 5
    
    // Max values testing.
    @Test public void rangeStep070(){r( "1",  "1",    null,  "foo", null, "2",   null, up);}
    @Test public void rangeStep071(){r( "1",  null,   null,  "10",  null, "2",   null, up);}
    
    //stepUp() value=1 max=0 Already above max.  Discrepancy with FF.
    @Test public void rangeStep072(){r( "1",  null,   null,  "0",   null, "1",   null, up);}//not 1. FF 0. 
    
    //stepUp() value=1 max=-10 Already above max. Discrepancy with FF.
    @Test public void rangeStep073(){r( "1",  null,   null,  "-10", null, "1",   null, up);}//FF 0.
    
    @Test public void rangeStep074(){r( "1",  null,   null,  "1",   null, "1",   null, up);}
    
    //stepUp() value=-3 step=5 min=-10 max=-3 Already at max, and out of step. Discrepancy with FF.
    @Test public void rangeStep075(){r( "-3", "5",    "-10", "-3",  null, "-3",  null, up);}//not -3. FF -5.
    
    // Step mismatch.
    //stepUp() value=1 step=2 min=0 Out of step. Discrepancy with FF.
    @Test public void rangeStep076(){r( "1",  "2",    "0",   null,  null, "2",   null, up);}//not 2. FF 4.
    
    //stepUp(2) value=1 step=2 min=0 Out of step. Discrepancy with FF.
    @Test public void rangeStep077(){r( "1",  "2",    "0",   null,  2,  "4",   null, up);}//not 4. FF 6.
    
    @Test public void rangeStep078(){r( "8",  "2",    null,  "9",   null, "8",   null, up);}
    
    //stepUp() value=-3 step=2 min=-6 Out of step. Discrepancy with FF.
    @Test public void rangeStep079(){r( "-3", "2",    "-6",  null,  null, "-2",   null, up);}//not -2. FF 0.
    
    @Test public void rangeStep080(){r( "9",  "3",    "-10", null,  null, "11",  null, up);}
    
    //stepUp() value=7 step=3 min=-10 Out of step. Discrepancy with FF.
    @Test public void rangeStep081(){r( "7",  "3",    "-10", null,  null, "8",  null, up);}//not 8. FF 11.
    
    //stepUp() value=7 step=3 min=5 Out of step. Discrepancy with FF.
    @Test public void rangeStep082(){r( "7",  "3",    "5",   null,  null, "8",  null, up);}//not 8. FF 11.
    
    //stepUp() value=9 step=4 min=3 Discrepancy with FF.
    @Test public void rangeStep083(){r( "9",  "4",    "3",   null,  null, "11",  null, up);}//not 11. FF 15.
    
    @Test public void rangeStep084(){r( "-2", "3",    "-6",  null,  null, "0",   null, up);}
    @Test public void rangeStep085(){r( "7",  "3",    "6",   null,  null, "9",   null, up);}
    // Clamping.
    @Test public void rangeStep086(){r( "1",  "2",    "0",  "3",   null,  "2",   null, up);}
    @Test public void rangeStep087(){r( "0",  "5",    "1",  "8",   10,  "6",   null, up);}
    @Test public void rangeStep088(){r( "-9", "3",    "-8", "-1",   5,   "-2",  null, up);}
    @Test public void rangeStep089(){r( "-9", "3",    "8",  "15",   15,  "14",  null, up);}
    @Test public void rangeStep090(){r( "-1", "3",    "-1", "4",    3,   "2",   null, up);}
    @Test public void rangeStep091(){r( "-3", "2",    "-6",  "-2",  null, "-2",  null, up);}
    @Test public void rangeStep092(){r( "-3", "2",    "-6",  "-1",  null, "-2",  null, up);}
    // value = "" (default will be 50).
    //stepUp() value=""
    @Test public void rangeStep093(){r( "",   null,   null,  null,  null, "51",  null, up);}//not 1
    // With step = "any".
    @Test public void rangeStep094(){r( "0",  "any",  null,  null,  1,    null,  InvalidStateError.class, up);}
    @Test public void rangeStep095(){r( "0",  "ANY",  null,  null,  1,    null,  InvalidStateError.class, up);}
    @Test public void rangeStep096(){r( "0",  "AnY",  null,  null,  1,    null,  InvalidStateError.class, up);}
    @Test public void rangeStep097(){r( "0",  "aNy",  null,  null,  1,    null,  InvalidStateError.class, up);}
    // With @value = step base.
    //stepUp() value=1 step=2 Out of step. Discrepancy with FF.
    @Test public void rangeStep098(){r( "1",  "2",    null,  null,  null, "2",   null, up);}//not 2. FF 3.
  
    /** step test for type=range
     * @param initial value
     * @param step or null
     * @param min or null
     * @param max or null
     * @param n or null
     * @param expected final value
     * @param exception Should it throw an exception? 
     * @param down Downwards? (or up?) */
    private void r(String initial, String step, String min, String max, Integer n, String expected, Class<? extends Exception> exception, boolean down){
    	HTMLInputElement input=input("range");
    	if(initial!=null && 0<initial.length())input.setValue(initial);
    	if(step!=null)input.setStep(step);
    	if(min!=null)input.setMin(min);
    	if(max!=null)input.setMax(max);
    	try{
    		if(n==null){
    			if(down)
    				input.stepDown();
    			else
    				input.stepUp();
    		}else{
    			if(down)
    				input.stepDown(n);
    			else
    				input.stepUp(n);
    		}
    		String v=input.getValue();
    		if(exception==null)
    			assertEquals(expected, v);
    		else
    			fail("Expected " + exception);
    	}
    	catch(Exception e){
    		if(exception==null)
    			fail("Unexpected " + e);
    		else
    			assertType(e, exception);	
    	}
    }

    // step down "date" ---------------------------------------------------------
    
    // Regular case.
    @Test public void dateStep000(){d( "2012-07-09",  null,  null,  null,  null, "2012-07-08",   null, down);}
    // Argument testing.
    @Test public void dateStep001(){d( "2012-07-09",  null,  null,  null,  1,    "2012-07-08",   null, down);}
    @Test public void dateStep002(){d( "2012-07-09",  null,  null,  null,  5,    "2012-07-04",   null, down);}
    @Test public void dateStep003(){d( "2012-07-09",  null,  null,  null,  -1,   "2012-07-10",   null, down);}
    @Test public void dateStep004(){d( "2012-07-09",  null,  null,  null,  0,    "2012-07-09",   null, down);}
    // Month/Year wrapping.
    @Test public void dateStep005(){d( "2012-08-01",  null,  null,  null,  1,    "2012-07-31",   null, down);}
    @Test public void dateStep006(){d( "1969-01-02",  null,  null,  null,  4,    "1968-12-29",   null, down);}
    @Test public void dateStep007(){d( "1969-01-01",  null,  null,  null,  -365, "1970-01-01",   null, down);}
    @Test public void dateStep008(){d( "2012-02-29",  null,  null,  null,  -1,   "2012-03-01",   null, down);}
    // Float values are rounded to integer (1.1 -> 1).
    //@Test public void dateStep009(){document( "2012-01-02",  null,  null,  null,  1.1,  "2012-01-01",   null, down);}
    //@Test public void dateStep010(){document( "2012-01-02",  null,  null,  null,  1.9,  "2012-01-01",   null, down);}
    // With step values.
    @Test public void dateStep011(){d( "2012-01-03",  "0.5", null,  null,  null, "2012-01-02",   null, down);}
    @Test public void dateStep012(){d( "2012-01-02",  "0.5", null,  null,  null, "2012-01-01",   null, down);}
    @Test public void dateStep013(){d( "2012-01-01",  "2",   null,  null,  null, "2011-12-30",   null, down);}
    @Test public void dateStep014(){d( "2012-01-02",  "0.25",null,  null,  4,    "2012-01-01",   null, down);}
    
    //value=2012-01-15 step=1.1 min=2012-01-01 stepDown(1) -> Discrepancy from Mozilla which has 2012-01-12. Imprecise?
    @Test public void dateStep015(){d( "2012-01-15",  "1.1",  "2012-01-01", null,  1,    "2012-01-14",   null, down);}
    
    @Test public void dateStep016(){d( "2012-01-12",  "1.1",  "2012-01-01", null,  2,    "2012-01-01",   null, down);}
    @Test public void dateStep017(){d( "2012-01-23",  "1.1",  "2012-01-01", null,  10,   "2012-01-12",   null, down);}
    @Test public void dateStep018(){d( "2012-01-23",  "1.1",  "2012-01-01", null,  11,   "2012-01-01",   null, down);}
    @Test public void dateStep019(){d( "1968-01-12",  "1.1",  "1968-01-01", null,  8,    "1968-01-01",   null, down);}
    // step = 0 isn't allowed (-> step = 1).
    @Test public void dateStep020(){d( "2012-01-02",  "0",   null,  null,  null, "2012-01-01",   null, down);}
    // step < 0 isn't allowed (-> step = 1).
    @Test public void dateStep021(){d( "2012-01-02",  "-1",  null,  null,  null, "2012-01-01",   null, down);}
    // step = NaN isn't allowed (-> step = 1).
    @Test public void dateStep022(){d( "2012-01-02",  "foo", null,  null,  null, "2012-01-01",   null, down);}
    // Min values testing.
    @Test public void dateStep023(){d( "2012-01-03",  "1",    "foo",        null,  2,     "2012-01-01",  null, down);}
    @Test public void dateStep024(){d( "2012-01-02",  "1",    "2012-01-01", null,  null,  "2012-01-01",  null, down);}
    @Test public void dateStep025(){d( "2012-01-01",  "1",    "2012-01-01", null,  null,  "2012-01-01",  null, down);}
    @Test public void dateStep026(){d( "2012-01-01",  "1",    "2012-01-10", null,  1,     "2012-01-01",  null, down);}
    
    //value=2012-01-05 step=3 min=2012-01-01 Out of step! stepDown() --> 2012-01-02
    @Test public void dateStep027(){d( "2012-01-05",  "3",    "2012-01-01", null,  null,  "2012-01-04",  null, down);}
    
    @Test public void dateStep028(){d( "1969-01-01",  "5",    "1969-01-01", "1969-01-02", null,  "1969-01-01",  null, down);}
    // Max values testing.
    @Test public void dateStep029(){d( "2012-01-02",  "1",    null,  "foo",         null,  "2012-01-01",  null, down);}
    @Test public void dateStep030(){d( "2012-01-02",  null,   null,  "2012-01-05",  null,  "2012-01-01",  null, down);}
    @Test public void dateStep031(){d( "2012-01-03",  null,   null,  "2012-01-03",  null,  "2012-01-02",  null, down);}
    @Test public void dateStep032(){d( "2012-01-07",  null,   null,  "2012-01-04",  4,     "2012-01-03",  null, down);}
    @Test public void dateStep033(){d( "2012-01-07",  "2",    null,  "2012-01-04",  3,     "2012-01-01",  null, down);}
    // Step mismatch.
    //value=2012-01-04 step=2 min=2012-01-01 stepDown() --> 2012-01-02
    @Test public void dateStep034(){d( "2012-01-04",  "2",    "2012-01-01",  null,         null,  "2012-01-03",  null, down);}
    
    //value=2012-01-06 step=2 min=2012-01-01 stepDown(2) --> 2012-01-02
    @Test public void dateStep035(){d( "2012-01-06",  "2",    "2012-01-01",  null,         2,     "2012-01-03",  null, down);}
    
    @Test public void dateStep036(){d( "2012-01-05",  "2",    "2012-01-04",  "2012-01-08", null,  "2012-01-04",  null, down);}
    @Test public void dateStep037(){d( "1970-01-04",  "2",    null,          null,         null,  "1970-01-02",  null, down);}
    @Test public void dateStep038(){d( "1970-01-09",  "3",    null,          null,         null,  "1970-01-06",  null, down);}
    // Clamping.
    //value=2012-05-01 max=2012-01-05 stepDown() Above max.
    @Test public void dateStep039(){d( "2012-05-01",  null,   null,          "2012-01-05", null,  "2012-01-05",  null, down);}
    
    //value=1979-01-05 step=2 min=1970-01-02 max=1970-01-05 Out of step. stepDown() --> 1970-01-03
    @Test public void dateStep040(){d( "1970-01-05",  "2",    "1970-01-02",  "1970-01-05", null,  "1970-01-04",  null, down);}
    
    @Test public void dateStep041(){d( "1970-01-01",  "5",    "1970-01-02",  "1970-01-09", 10,    "1970-01-01",  null, down);}
    
    //value=1970-01-07 step=5 min=1969-12-27 max=1970-01-06 Above max. stepDown(2) --> 1969-12-28 Out of step!
    @Test public void dateStep042(){d( "1970-01-07",  "5",    "1969-12-27",  "1970-01-06", 2,     "1970-01-01",  null, down);}
    
    @Test public void dateStep043(){d( "1970-03-08",  "3",    "1970-02-01",  "1970-02-07", 15,    "1970-02-01",  null, down);}
    @Test public void dateStep044(){d( "1970-01-10",  "3",    "1970-01-01",  "1970-01-06", 2,     "1970-01-04",  null, down);}
    // value = "" (NaN).
    @Test public void dateStep045(){d( "",   null,   null,  null,  null, "1969-12-31",    null, down);}
    // With step = "any".
    @Test public void dateStep046(){d( "2012-01-01",  "any",  null,  null,  1,  null,  InvalidStateError.class, down);}
    @Test public void dateStep047(){d( "2012-01-01",  "ANY",  null,  null,  1,  null,  InvalidStateError.class, down);}
    @Test public void dateStep048(){d( "2012-01-01",  "AnY",  null,  null,  1,  null,  InvalidStateError.class, down);}
    @Test public void dateStep049(){d( "2012-01-01",  "aNy",  null,  null,  1,  null,  InvalidStateError.class, down);}

    // stepUp() for type=date
    
    // Regular case.
    @Test public void dateStep050(){d( "2012-07-09",  null,  null,  null,  null, "2012-07-10",   null, up);}
    // Argument testing.
    @Test public void dateStep051(){d( "2012-07-09",  null,  null,  null,  1,    "2012-07-10",   null, up);}
    @Test public void dateStep052(){d( "2012-07-09",  null,  null,  null,  9,    "2012-07-18",   null, up);}
    @Test public void dateStep053(){d( "2012-07-09",  null,  null,  null,  -1,   "2012-07-08",   null, up);}
    @Test public void dateStep054(){d( "2012-07-09",  null,  null,  null,  0,    "2012-07-09",   null, up);}
    // Month/Year wrapping.
    @Test public void dateStep055(){d( "2012-07-31",  null,  null,  null,  1,    "2012-08-01",   null, up);}
    @Test public void dateStep056(){d( "1968-12-29",  null,  null,  null,  4,    "1969-01-02",   null, up);}
    @Test public void dateStep057(){d( "1970-01-01",  null,  null,  null,  -365, "1969-01-01",   null, up);}
    @Test public void dateStep058(){d( "2012-03-01",  null,  null,  null,  -1,   "2012-02-29",   null, up);}
    // Float values are rounded to integer (1.1 -> 1).
    //@Test public void dateStep059(){document( "2012-01-01",  null,  null,  null,  1.1,  "2012-01-02",   null, up);}
    //@Test public void dateStep060(){document( "2012-01-01",  null,  null,  null,  1.9,  "2012-01-02",   null, up);}
    // With step values.
    @Test public void dateStep061(){d( "2012-01-01",  "0.5",  null,         null,  null, "2012-01-02",   null, up);}
    @Test public void dateStep062(){d( "2012-01-01",  "0.5",  null,         null,  null, "2012-01-02",   null, up);}
    @Test public void dateStep063(){d( "2012-01-01",  "2",    null,         null,  null, "2012-01-03",   null, up);}
    @Test public void dateStep064(){d( "2012-01-01",  "0.25", null,         null,  4,    "2012-01-02",   null, up);}
    @Test public void dateStep065(){d( "2012-01-01",  "1.1",  "2012-01-01", null,  1,    "2012-01-12",   null, up);}
    @Test public void dateStep066(){d( "2012-01-01",  "1.1",  "2012-01-01", null,  2,    "2012-01-12",   null, up);}
    @Test public void dateStep067(){d( "2012-01-01",  "1.1",  "2012-01-01", null,  10,   "2012-01-12",   null, up);}
    @Test public void dateStep068(){d( "2012-01-01",  "1.1",  "2012-01-01", null,  11,   "2012-01-23",   null, up);}
    @Test public void dateStep069(){d( "1968-01-01",  "1.1",  "1968-01-01", null,  8,    "1968-01-12",   null, up);}
    // step = 0 isn't allowed (-> step = 1).
    @Test public void dateStep070(){d( "2012-01-01",  "0",   null,  null,  null, "2012-01-02",   null, up);}
    // step < 0 isn't allowed (-> step = 1).
    @Test public void dateStep071(){d( "2012-01-01",  "-1",  null,  null,  null, "2012-01-02",   null, up);}
    // step = NaN isn't allowed (-> step = 1).
    @Test public void dateStep072(){d( "2012-01-01",  "foo", null,  null,  null, "2012-01-02",   null, up);}
    // Min values testing.
    @Test public void dateStep073(){d( "2012-01-01",  "1",   "foo",         null,  null,  "2012-01-02",  null, up);}
    @Test public void dateStep074(){d( "2012-01-01",  null,  "2011-12-01",  null,  null,  "2012-01-02",  null, up);}
    @Test public void dateStep075(){d( "2012-01-01",  null,  "2012-01-02",  null,  null,  "2012-01-02",  null, up);}
    @Test public void dateStep076(){d( "2012-01-01",  null,  "2012-01-01",  null,  null,  "2012-01-02",  null, up);}
    @Test public void dateStep077(){d( "2012-01-01",  null,  "2012-01-04",  null,  4,     "2012-01-05",  null, up);}
    @Test public void dateStep078(){d( "2012-01-01",  "2",   "2012-01-04",  null,  3,     "2012-01-06",  null, up);}
    // Max values testing.
    @Test public void dateStep079(){d( "2012-01-01",  "1",    null,  "foo",        2,     "2012-01-03",  null, up);}
    @Test public void dateStep080(){d( "2012-01-01",  "1",    null,  "2012-01-10", 1,     "2012-01-02",  null, up);}
    @Test public void dateStep081(){d( "2012-01-02",  null,   null,  "2012-01-01", null,  "2012-01-02",  null, up);}
    @Test public void dateStep082(){d( "2012-01-02",  null,   null,  "2012-01-02", null,  "2012-01-02",  null, up);}
    @Test public void dateStep083(){d( "2012-01-02",  null,   null,  "2012-01-02", null,  "2012-01-02",  null, up);}
    @Test public void dateStep084(){d( "1969-01-02",  "5",    "1969-01-01",  "1969-01-02", null,  "1969-01-02",  null, up);}
    // Step mismatch.
    @Test public void dateStep085(){d( "2012-01-02",  "2",    "2012-01-01",  null,         null,  "2012-01-03",  null, up);}
    @Test public void dateStep086(){d( "2012-01-02",  "2",    "2012-01-01",  null,         2,     "2012-01-05",  null, up);}
    @Test public void dateStep087(){d( "2012-01-05",  "2",    "2012-01-01",  "2012-01-06", null,  "2012-01-05",  null, up);}
    @Test public void dateStep088(){d( "1970-01-02",  "2",    null,          null,         null,  "1970-01-04",  null, up);}
    @Test public void dateStep089(){d( "1970-01-05",  "3",    null,          null,         null,  "1970-01-08",  null, up);}
    @Test public void dateStep090(){d( "1970-01-03",  "3",    null,          null,         null,  "1970-01-06",  null, up);}
    @Test public void dateStep091(){d( "1970-01-03",  "3",    "1970-01-02",  null,         null,  "1970-01-05",  null, up);}
    // Clamping.
    @Test public void dateStep092(){d( "2012-01-01",  null,   "2012-01-31",  null,         null,  "2012-01-31",  null, up);}
    @Test public void dateStep093(){d( "1970-01-02",  "2",    "1970-01-01",  "1970-01-04", null,  "1970-01-03",  null, up);}
    @Test public void dateStep094(){d( "1970-01-01",  "5",    "1970-01-02",  "1970-01-09", 10,    "1970-01-07",  null, up);}
    @Test public void dateStep095(){d( "1969-12-28",  "5",    "1969-12-29",  "1970-01-06", 3,     "1970-01-03",  null, up);}
    @Test public void dateStep096(){d( "1970-01-01",  "3",    "1970-02-01",  "1970-02-07", 15,    "1970-02-07",  null, up);}
    @Test public void dateStep097(){d( "1970-01-01",  "3",    "1970-01-01",  "1970-01-06", 2,     "1970-01-04",  null, up);}
    // value = "" (NaN).
    @Test public void dateStep098(){d( "",   null,   null,  null,  null, "1970-01-02",    null, up);}
    // With step = "any".
    @Test public void dateStep099(){d( "2012-01-01",  "any",  null,  null,  1,  null,  InvalidStateError.class, up);}
    @Test public void dateStep100(){d( "2012-01-01",  "ANY",  null,  null,  1,  null,  InvalidStateError.class, up);}
    @Test public void dateStep101(){d( "2012-01-01",  "AnY",  null,  null,  1,  null,  InvalidStateError.class, up);}
    @Test public void dateStep102(){d( "2012-01-01",  "aNy",  null,  null,  1,  null,  InvalidStateError.class, up);}
    
    /** step test for type=date
     * @param initial value
     * @param step or null
     * @param min or null
     * @param max or null
     * @param n or null
     * @param expected final value
     * @param exception Should it throw an exception? 
     * @param down Downwards? (or up?) */
    private void d(String initial, String step, String min, String max, Integer n, String expected, Class<? extends Exception> exception, boolean down){
    	HTMLInputElement input=input("date");
    	if(initial!=null && 0<initial.length())input.setValue(initial);
    	if(step!=null)input.setStep(step);
    	if(min!=null)input.setMin(min);
    	if(max!=null)input.setMax(max);
    	try{
    		if(n==null){
    			if(down)
    				input.stepDown();
    			else
    				input.stepUp();
    		}else{
    			if(down)
    				input.stepDown(n);
    			else
    				input.stepUp(n);
    		}
    		String v=input.getValue();
    		if(exception==null)
    			assertEquals(expected, v);
    		else
    			fail("Expected " + exception);
    	}
    	catch(Exception e){
    		if(exception==null)
    			fail("Unexpected " + e);
    		else
    			assertType(e, exception);	
    	}
    }

    // stepDown() input=time -----------------

    // Regular case.
    @Test public void timeStep000(){t( "16:39",  null,  null,  null,  null, "16:38",   null, down); }
    // Argument testing.
    @Test public void timeStep001(){t( "16:40",  null,  null,  null,  1,    "16:39",   null, down); }
    @Test public void timeStep002(){t( "16:40",  null,  null,  null,  5,    "16:35",   null, down); }
    @Test public void timeStep003(){t( "16:40",  null,  null,  null,  -1,    "16:41",   null, down); }
    @Test public void timeStep0030(){t( "16:40",  null,  null,  null,  0,    "16:40",   null, down); }
    // hour/minutes/seconds wrapping.
    @Test public void timeStep004(){t( "05:00",  null,  null,  null, null,   "04:59",   null, down); }
    @Test public void timeStep005(){t( "05:00:00", "1",   null,  null, null,   "04:59:59",   null, down); }
    @Test public void timeStep006(){t( "05:00:00", "0.1", null,  null, null,   "04:59:59.900",   null, down); }
    @Test public void timeStep007(){t( "05:00:00", "0.01", null, null, null,   "04:59:59.990",   null, down); }
    @Test public void timeStep008(){t( "05:00:00", "0.001", null, null, null,  "04:59:59.999",   null, down); }
    // stepDown() on "00:00" gives "23:59".
    @Test public void timeStep009(){t( "00:00",  null,  null,  null,  1,    "23:59",   null, down); }
    @Test public void timeStep010(){t( "00:00",  null,  null,  null,  3,    "23:57",   null, down); }
    // Some random step values..
    @Test public void timeStep011(){t( "16:56",  "0.5", null,  null,  null, "16:55:59.500",   null, down); }
    @Test public void timeStep012(){t( "16:56",  "2",   null,  null,  null, "16:55:58",   null, down); }
    @Test public void timeStep013(){t( "16:56",  "0.25",null,  null,  4,    "16:55:59",   null, down); }
    @Test public void timeStep014(){t( "16:57",  "1.1",  "16:00", null,  1,    "16:56:59.900",   null, down); }
    @Test public void timeStep015(){t( "16:57",  "1.1",  "16:00", null,  2,    "16:56:58.800",   null, down); }
    @Test public void timeStep016(){t( "16:57",  "1.1",  "16:00", null,  10,   "16:56:50",   null, down); }
    @Test public void timeStep017(){t( "16:57",  "1.1",  "16:00", null,  11,   "16:56:48.900",   null, down); }
    @Test public void timeStep018(){t( "16:57",  "1.1",  "16:00", null,  8,    "16:56:52.200",   null, down); }
    // Invalid @step, means that we use the default value.
    @Test public void timeStep019(){t( "17:01",  "0",   null,  null,  null, "17:00",   null, down); }
    @Test public void timeStep020(){t( "17:01",  "-1",  null,  null,  null, "17:00",   null, down); }
    @Test public void timeStep021(){t( "17:01",  "foo", null,  null,  null, "17:00",   null, down); }
    // Min values testing.
    @Test public void timeStep022(){t( "17:02",  "60",    "foo",        null,  2,     "17:00",  null, down); }
    @Test public void timeStep023(){t( "17:10",  "60",    "17:09", null,  null,  "17:09",  null, down); }
    @Test public void timeStep024(){t( "17:10",  "60",    "17:10", null,  null,  "17:10",  null, down); }
    @Test public void timeStep025(){t( "17:10",  "60",    "17:30", null,  1,     "17:10",  null, down); }
    @Test public void timeStep026(){t( "17:10",  "180",    "17:05", null,  null,  "17:08",  null, down); }
    @Test public void timeStep027(){t( "17:10",  "300",    "17:10", "17:11", null,  "17:10",  null, down); }
    // Max values testing.
    @Test public void timeStep028(){t( "17:15",  "60",   null,  "foo",    null,  "17:14",  null, down); }
    @Test public void timeStep029(){t( "17:15",  null,   null,  "17:20",  null,  "17:14",  null, down); }
    @Test public void timeStep030(){t( "17:15",  null,   null,  "17:15",  null,  "17:14",  null, down); }
    @Test public void timeStep031(){t( "17:15",  null,   null,  "17:13",  4,     "17:11",  null, down); }
    @Test public void timeStep032(){t( "17:15",  "120",  null,  "17:13",  3,     "17:09",  null, down); }
    // Step mismatch.
    @Test public void timeStep033(){t( "17:19",  "120",  "17:10",  null,    null,  "17:18",  null, down); }
    @Test public void timeStep034(){t( "17:19",  "120",  "17:10",  null,    2,     "17:16",  null, down); }
    @Test public void timeStep035(){t( "17:19",  "120",  "17:18",  "17:25", null,  "17:18",  null, down); }
    @Test public void timeStep036(){t( "17:19",  "120",  null,     null,    null,  "17:17",  null, down); }
    @Test public void timeStep037(){t( "17:19",  "180",  null,     null,    null,  "17:16",  null, down); }
    // Clamping.
    @Test public void timeStep038(){t( "17:22",  null,   null,     "17:11", null,  "17:11",  null, down); }
    @Test public void timeStep039(){t( "17:22",  "120",  "17:20",  "17:22", null,  "17:20",  null, down); }
    @Test public void timeStep040(){t( "17:22",  "300",  "17:12",  "17:20", 10,    "17:12",  null, down); }
    //value=17:22 step=300s=5m min=17:18 max=17:20 Above max. The only valid value is min=base. stepDown(2)
    @Test public void timeStep041(){t( "17:22",  "300",  "17:18",  "17:20", 2,     "17:18",  null, down); }
    @Test public void timeStep042(){t( "17:22",  "180",  "17:00",  "17:20", 15,    "17:00",  null, down); }
    @Test public void timeStep043(){t( "17:22",  "180",  "17:10",  "17:20", 2,     "17:16",  null, down); }
    // value = "" (NaN).
    @Test public void timeStep044(){t( "",   null,   null,  null,  null, "23:59",    null, down); }
    // With step = "any".
    @Test public void timeStep045(){t( "17:26",  "any",  null,  null,  1,  null,  InvalidStateError.class, down);}
    @Test public void timeStep046(){t( "17:26",  "ANY",  null,  null,  1,  null,  InvalidStateError.class, down);}
    @Test public void timeStep047(){t( "17:26",  "AnY",  null,  null,  1,  null,  InvalidStateError.class, down);}
    @Test public void timeStep048(){t( "17:26",  "aNy",  null,  null,  1,  null,  InvalidStateError.class, down);}
    
    // stepUp() input=time -------------------
    
    // Regular case.
    @Test public void timeStep050(){t( "16:39",  null,  null,  null,  null, "16:40",   null, up); }
    // Argument testing.
    @Test public void timeStep051(){t( "16:40",  null,  null,  null,  1,    "16:41",   null, up); }
    @Test public void timeStep052(){t( "16:40",  null,  null,  null,  5,    "16:45",   null, up); }
    @Test public void timeStep053(){t( "16:40",  null,  null,  null,  -1,   "16:39",   null, up); }
    @Test public void timeStep054(){t( "16:40",  null,  null,  null,  0,    "16:40",   null, up); }
    // hour/minutes/seconds wrapping.
    @Test public void timeStep055(){t( "04:59",  null,  null,  null, null,   "05:00",  null, up); }
    @Test public void timeStep056(){t( "04:59:59", "1",   null,  null, null,   "05:00",  null, up); }
    @Test public void timeStep057(){t( "04:59:59.900", "0.1",  null,  null, null, "05:00",  null, up); }
    @Test public void timeStep058(){t( "04:59:59.990", "0.01", null,  null, null, "05:00",  null, up); }
    @Test public void timeStep059(){t( "04:59:59.999", "0.001", null, null, null, "05:00",  null, up); }
    // stepUp() on "23:59" gives "00:00".
    @Test public void timeStep060(){t( "23:59",  null,  null,  null,  1,    "00:00",   null, up); }
    @Test public void timeStep061(){t( "23:59",  null,  null,  null,  3,    "00:02",   null, up); }
    // Some random step values..
    //value=16:56 step=0.5=500ms stepUp()
    @Test public void timeStep062(){t( "16:56",  "0.5", null,  null,  null, "16:56:00.500",   null, up); }
    @Test public void timeStep063(){t( "16:56",  "2",   null,  null,  null, "16:56:02",   null, up); }
    @Test public void timeStep064(){t( "16:56",  "0.25",null,  null,  4,    "16:56:01",   null, up); }
    @Test public void timeStep065(){t( "16:57",  "1.1",  "16:00", null,  1,    "16:57:01",   null, up); }
    @Test public void timeStep066(){t( "16:57",  "1.1",  "16:00", null,  2,    "16:57:02.100",   null, up); }
    @Test public void timeStep067(){t( "16:57",  "1.1",  "16:00", null,  10,   "16:57:10.900",   null, up); }
    @Test public void timeStep068(){t( "16:57",  "1.1",  "16:00", null,  11,   "16:57:12",   null, up); }
    @Test public void timeStep069(){t( "16:57",  "1.1",  "16:00", null,  8,    "16:57:08.700",   null, up); }
    // Invalid @step, means that we use the default value.
    @Test public void timeStep070(){t( "17:01",  "0",   null,  null,  null, "17:02",   null, up); }
    @Test public void timeStep071(){t( "17:01",  "-1",  null,  null,  null, "17:02",   null, up); }
    @Test public void timeStep072(){t( "17:01",  "foo", null,  null,  null, "17:02",   null, up); }
    // Min values testing.
    @Test public void timeStep073(){t( "17:02",  "60",    "foo",        null,  2, "17:04",  null, up); }
    @Test public void timeStep074(){t( "17:10",  "60",    "17:09", null,  null,   "17:11",  null, up); }
    @Test public void timeStep0740(){t( "17:10",  "60",    "17:10", null,  null,   "17:11",  null, up); }
    @Test public void timeStep075(){t( "17:10",  "60",    "17:30", null,  1,      "17:30",  null, up); }
    @Test public void timeStep076(){t( "17:10",  "180",    "17:05", null,  null,  "17:11",  null, up); }
    @Test public void timeStep077(){t( "17:10",  "300",    "17:10", "17:11", null,"17:10",  null, up); }
    // Max values testing.
    @Test public void timeStep078(){t( "17:15",  "60",   null,  "foo",    null,  "17:16",  null, up); }
    @Test public void timeStep079(){t( "17:15",  null,   null,  "17:20",  null,  "17:16",  null, up); }
    @Test public void timeStep080(){t( "17:15",  null,   null,  "17:15",  null,  "17:15",  null, up); }
    @Test public void timeStep081(){t( "17:15",  null,   null,  "17:13",  4,     "17:15",  null, up); }
    @Test public void timeStep082(){t( "17:15",  "120",  null,  "17:13",  3,     "17:15",  null, up); }
    // Step mismatch.
    @Test public void timeStep083(){t( "17:19",  "120",  "17:10",  null,    null,  "17:20",  null, up); }
    @Test public void timeStep084(){t( "17:19",  "120",  "17:10",  null,    2,     "17:22",  null, up); }
    @Test public void timeStep085(){t( "17:19",  "120",  "17:18",  "17:25", null,  "17:20",  null, up); }
    @Test public void timeStep086(){t( "17:19",  "120",  null,     null,    null,  "17:21",  null, up); }
    @Test public void timeStep087(){t( "17:19",  "180",  null,     null,    null,  "17:22",  null, up); }
    // Clamping.
    @Test public void timeStep088(){t( "17:22",  null,   null,     "17:11", null,  "17:22",  null, up); }
    @Test public void timeStep089(){t( "17:22",  "120",  "17:20",  "17:22", null,  "17:22",  null, up); }
    @Test public void timeStep090(){t( "17:22",  "300",  "17:12",  "17:20", 10,    "17:22",  null, up); }
    @Test public void timeStep091(){t( "17:22",  "300",  "17:18",  "17:20", 2,     "17:22",  null, up); }
    @Test public void timeStep092(){t( "17:22",  "180",  "17:00",  "17:20", 15,    "17:22",  null, up); }
    @Test public void timeStep093(){t( "17:22",  "180",  "17:10",  "17:20", 2,     "17:22",  null, up); }
    // value = "" (NaN).
    @Test public void timeStep094(){t( "",   null,   null,  null,  null, "00:01",    null, up); }
    // With step = "any".
    @Test public void timeStep095(){t( "17:26",  "any",  null,  null,  1,  null,  InvalidStateError.class, up);}
    @Test public void timeStep096(){t( "17:26",  "ANY",  null,  null,  1,  null,  InvalidStateError.class, up);}
    @Test public void timeStep097(){t( "17:26",  "AnY",  null,  null,  1,  null,  InvalidStateError.class, up);}
    @Test public void timeStep098(){t( "17:26",  "aNy",  null,  null,  1,  null,  InvalidStateError.class, up);}
    
    /** step test for type=time
     * @param initial value
     * @param step or null
     * @param min or null
     * @param max or null
     * @param n or null
     * @param expected final value
     * @param exception Should it throw an exception? 
     * @param down Downwards? (or up?) */
    private void t(String initial, String step, String min, String max, Integer n, String expected, Class<? extends Exception> exception, boolean down){
    	HTMLInputElement input=input("time");
    	if(initial!=null && 0<initial.length())input.setValue(initial);
    	if(step!=null)input.setStep(step);
    	if(min!=null)input.setMin(min);
    	if(max!=null)input.setMax(max);
    	try{
    		if(n==null){
    			if(down)
    				input.stepDown();
    			else
    				input.stepUp();
    		}else{
    			if(down)
    				input.stepDown(n);
    			else
    				input.stepUp(n);
    		}
    		String v=input.getValue();
    		if(exception==null)
    			assertEquals(expected, v);
    		else
    			fail("Expected " + exception);
    	}
    	catch(Exception e){
    		if(exception==null)
    			fail("Unexpected " + e);
    		else
    			assertType(e, exception);	
    	}
    }

    @Test public void tparseTime0(){
    	HTMLInputElementImp input=(HTMLInputElementImp) input();
    	Date d=time(14, 15);
    	Date o=input.parseTime("14:15");
    	assertEquals(d, o);
    }
    
    @Test public void tparseTime1(){
    	HTMLInputElementImp input=(HTMLInputElementImp) input();
    	Date d=time(14, 15, 16);
    	Date o=input.parseTime("14:15:16");
    	assertEquals(d, o);
    }
    
    @Test public void tparseTime2(){
    	HTMLInputElementImp input=(HTMLInputElementImp) input();
    	Date d=time(14, 15, 16, 7);
    	Date o=input.parseTime("14:15:16.7");
    	assertEquals(d, o);
    }
    
    @Test public void tparseTime3(){
    	HTMLInputElementImp input=(HTMLInputElementImp) input();
    	Date d=time(14, 15, 16, 17);
    	Date o=input.parseTime("14:15:16.17");
    	assertEquals(d, o);
    }
    
    @Test public void tparseTime4(){
    	HTMLInputElementImp input=(HTMLInputElementImp) input();
    	Date d=time(14, 15, 16, 171);
    	Date o=input.parseTime("14:15:16.171");
    	assertEquals(d, o);
    }
    
    @Test public void tparseTime5(){
    	Date d=time(0, 0);
    	long time=d.getTime();
    	assertEquals(0L, time);
    }
    
    // helpers -------------------------------
    
    private HTMLInputElement input(){
        HTMLDocument d=new Document();
        return d.createElement(HTMLInputElement.class);
    }

    private HTMLInputElement input(String type){
        HTMLInputElement input=input();
        input.setType(type);
        return input;
    }
}
