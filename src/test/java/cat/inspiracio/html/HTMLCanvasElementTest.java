/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import javax.script.ScriptException;

import org.junit.Test;
import org.w3c.dom.Node;

public class HTMLCanvasElementTest extends AbstractTest{

	@Test public void tclone() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "e=document.createElement('canvas');"
				+ "e.id='x';"
				+ "document.body.appendChild(e);"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;
		HTMLElement e=d.getElementById("x");
		Node n=e.cloneNode(true);
		assertType(n, HTMLCanvasElement.class);
	}
}
