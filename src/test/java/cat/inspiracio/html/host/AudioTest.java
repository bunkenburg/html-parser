/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html.host;

import static org.junit.Assert.assertEquals;

import javax.script.ScriptException;

import org.junit.Test;
import org.w3c.dom.Node;

import cat.inspiracio.html.AbstractTest;
import cat.inspiracio.html.HTMLAudioElement;
import cat.inspiracio.html.HTMLDocument;
import cat.inspiracio.html.host.Audio;

/** Tests for constructor new Audio(src). */
public class AudioTest extends AbstractTest{

	@Test public void t0() throws ScriptException{
		String program=""
				+ "a=new Audio();"
				+ "a";
		Object o=eval(program);
		assertType(o, HTMLAudioElement.class);
		HTMLAudioElement a=(HTMLAudioElement)o;
		assertEquals("auto", a.getPreload());
	}
	
	@Test public void t1() throws ScriptException{
		String program=""
				+ "src='source';"
				+ "a=new Audio(src);"
				+ "a";
		Object o=eval(program);
		assertType(o, HTMLAudioElement.class);
		HTMLAudioElement a=(HTMLAudioElement)o;
		assertEquals("auto", a.getPreload());
		assertEquals("source", a.getSrc());
	}
	
	@Test public void t3() throws ScriptException{
		String program=""
				+ "d=new Document();"
				+ "a=new Audio();"
				+ "a.id='audio';"
				+ "d.body.appendChild(a);"
				+ "d";
		Object o=eval(program);
		assertType(o, HTMLDocument.class);
		HTMLDocument d=(HTMLDocument)o;
		HTMLAudioElement a=(HTMLAudioElement)d.getElementById("audio");
		assertEquals("auto", a.getPreload());
	}
	
	@Test public void t4() throws ScriptException{
		String program=""
				+ "d=new Document();"
				+ "a=new Audio();"
				+ "a.id='audio';"
				+ "d.body.insertBefore(a, null);"
				+ "d";
		Object o=eval(program);
		assertType(o, HTMLDocument.class);
		HTMLDocument d=(HTMLDocument)o;
		HTMLAudioElement a=(HTMLAudioElement)d.getElementById("audio");
		assertEquals("auto", a.getPreload());
	}
	
	/** Tests element.appendChild(new Audio()). */
	@Test public void t5() throws ScriptException{
		String program=""
				+ "d=new Document();"
				+ "a=new Audio();"
				+ "a.id='audio';"
				+ "d.body.appendChild(a);"
				+ "d";
		Object o=eval(program);
		assertType(o, HTMLDocument.class);
		HTMLDocument d=(HTMLDocument)o;
		HTMLAudioElement a=(HTMLAudioElement)d.getElementById("audio");
		assertEquals("auto", a.getPreload());
	}
	
	/** Tests element.insertBefore(new Audio()). */
	@Test public void t6() throws ScriptException{
		String program=""
				+ "d=new Document();"
				+ "a=new Audio();"
				+ "a.id='audio';"
				+ "d.body.insertBefore(a, null);"
				+ "d";
		Object o=eval(program);
		assertType(o, HTMLDocument.class);
		HTMLDocument d=(HTMLDocument)o;
		HTMLAudioElement a=(HTMLAudioElement)d.getElementById("audio");
		assertEquals("auto", a.getPreload());
	}
	
	/** Tests Audio.cloneNode(deep). */
	@Test public void tclone() throws ScriptException{
		String program=""
				+ "d=new Document();"
				+ "a=new Audio();"
				+ "a.id='audio';"
				+ "d.body.insertBefore(a, null);"
				+ "d";
		Object o=eval(program);
		assertType(o, HTMLDocument.class);
		HTMLDocument d=(HTMLDocument)o;
		HTMLAudioElement a=(HTMLAudioElement)d.getElementById("audio");
		Node n=a.cloneNode(true);
		assertType(n, Audio.class);
	}
	
}
