/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html.host;

import static org.junit.Assert.*;

import javax.script.ScriptException;

import org.junit.Test;

import cat.inspiracio.html.AbstractTest;
import cat.inspiracio.html.HTMLBodyElement;
import cat.inspiracio.html.HTMLHeadElement;
import cat.inspiracio.html.HTMLHtmlElement;
import cat.inspiracio.html.host.Document;

/** Tests for constructor new Document(). */
public class DocumentTest extends AbstractTest{

	@Test public void t0() throws ScriptException{
		String program="d=new Document();d";
		Object o=eval(program);
		assertType(o, Document.class);
	}
	
	@Test public void tclone() throws Exception{
		String program="d=new Document();d";
		Object o=eval(program);
		assertType(o, Document.class);
		Document d=(Document)o;
		Document c=d.cloneNode(true);
		assertType(c, Document.class);
		o=d.clone();
		assertType(o, Document.class);
	}

	@Test public void telements()throws Exception{
		Document d=new Document();
		HTMLHtmlElement html=d.getDocumentElement();
		HTMLHeadElement head=d.getHead();
		HTMLBodyElement body=d.getBody();
		assertNotNull(html);
		assertNotNull(head);
		assertNotNull(body);
	}
}
