/*
Copyright 2015 Alexander Bunkenburg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html.host;

import static org.junit.Assert.assertEquals;

import javax.script.ScriptException;

import org.junit.Test;
import org.w3c.dom.Node;

import cat.inspiracio.html.AbstractTest;
import cat.inspiracio.html.HTMLDocument;
import cat.inspiracio.html.HTMLOptionElement;
import cat.inspiracio.html.host.Option;

/** Tests for constructor new Option().
 * 
 * */
public class OptionTest extends AbstractTest{

	@Test public void t0() throws ScriptException{
		String program=""
				+ "i=new Option();"
				+ "i";
		Object o=eval(program);
		assertType(o, HTMLOptionElement.class);
	}
	
	@Test public void t1() throws ScriptException{
		String program=""
				+ "i=new Option('text');"
				+ "i";
		Object o=eval(program);
		assertType(o, HTMLOptionElement.class);
		HTMLOptionElement i=(HTMLOptionElement)o;
		assertEquals("text", i.getText());
	}
	
	@Test public void t2() throws ScriptException{
		String program=""
				+ "i=new Option('text', 'value');"
				+ "i";
		Object o=eval(program);
		assertType(o, HTMLOptionElement.class);
		HTMLOptionElement i=(HTMLOptionElement)o;
		assertEquals("text", i.getText());
		assertEquals("value", i.getValue());
		assertEquals(false, i.getSelected());
	}
	
	@Test public void t3() throws ScriptException{
		String program=""
				+ "d=new Document();"
				+ "i=new Option('text', 'value', true);"
				+ "i.id='option';"
				+ "d.body.appendChild(i);"
				+ "d";
		Object o=eval(program);
		assertType(o, HTMLDocument.class);
		HTMLDocument d=(HTMLDocument)o;
		HTMLOptionElement i=(HTMLOptionElement)d.getElementById("option");
		assertEquals("text", i.getText());
		assertEquals("value", i.getValue());
		assertEquals(true, i.getSelected());
	}
	
	@Test public void t4() throws ScriptException{
		String program=""
				+ "d=new Document();"
				+ "i=new Option('text', 'value', true, true);"
				+ "i.id='option';"
				+ "d.body.appendChild(i);"
				+ "d";
		Object o=eval(program);
		assertType(o, HTMLDocument.class);
		HTMLDocument d=(HTMLDocument)o;
		HTMLOptionElement i=(HTMLOptionElement)d.getElementById("option");
		assertEquals("text", i.getText());
		assertEquals("value", i.getValue());
		assertEquals(true, i.getSelected());
	}
	
	@Test public void t5() throws ScriptException{
		String program=""
				+ "d=new Document();"
				+ "i=new Option('text', 'value', true, false);"
				+ "i.id='option';"
				+ "d.body.appendChild(i);"
				+ "d";
		Object o=eval(program);
		assertType(o, HTMLDocument.class);
		HTMLDocument d=(HTMLDocument)o;
		HTMLOptionElement i=(HTMLOptionElement)d.getElementById("option");
		assertEquals("text", i.getText());
		assertEquals("value", i.getValue());
		assertEquals(true, i.getSelected());
	}
	
	@Test public void tclone() throws ScriptException{
		String program=""
				+ "d=new Document();"
				+ "i=new Option('text', 'value', true, false);"
				+ "i.id='option';"
				+ "d.body.appendChild(i);"
				+ "d";
		Object o=eval(program);
		assertType(o, HTMLDocument.class);
		HTMLDocument d=(HTMLDocument)o;
		Option i=(Option)d.getElementById("option");
		Node c=i.cloneNode(true);
		assertType(c, Option.class);
	}
	

}
