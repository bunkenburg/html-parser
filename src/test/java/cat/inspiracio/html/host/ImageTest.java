/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html.host;

import static org.junit.Assert.assertEquals;

import javax.script.ScriptException;

import org.junit.Test;

import cat.inspiracio.html.AbstractTest;
import cat.inspiracio.html.HTMLDocument;
import cat.inspiracio.html.HTMLImageElement;
import cat.inspiracio.html.host.Image;

/** Tests for constructor new Image(width, height).
 * 
 * */
public class ImageTest extends AbstractTest{

	@Test public void t0() throws ScriptException{
		String program=""
				+ "i=new Image();"
				+ "i";
		Object o=eval(program);
		assertType(o, HTMLImageElement.class);
	}
	
	@Test public void t1() throws ScriptException{
		String program=""
				+ "i=new Image(14);"
				+ "i";
		Object o=eval(program);
		assertType(o, HTMLImageElement.class);
		HTMLImageElement i=(HTMLImageElement)o;
		assertEquals(14, i.getWidth());
	}
	
	@Test public void t2() throws ScriptException{
		String program=""
				+ "i=new Image(14, 15);"
				+ "i";
		Object o=eval(program);
		assertType(o, HTMLImageElement.class);
		HTMLImageElement i=(HTMLImageElement)o;
		assertEquals(14, i.getWidth());
		assertEquals(15, i.getHeight());
	}
	
	@Test public void t3() throws ScriptException{
		String program=""
				+ "d=new Document();"
				+ "i=new Image(14, 15);"
				+ "i.id='img';"
				+ "d.body.appendChild(i);"
				+ "d";
		Object o=eval(program);
		assertType(o, HTMLDocument.class);
		HTMLDocument d=(HTMLDocument)o;
		HTMLImageElement i=(HTMLImageElement)d.getElementById("img");
		assertEquals(14, i.getWidth());
		assertEquals(15, i.getHeight());
	}
	
	@Test public void t4() throws ScriptException{
		String program=""
				+ "d=new Document();"
				+ "i=new Image(14, 15);"
				+ "i.id='img';"
				+ "d.body.insertBefore(i, null);"
				+ "d";
		Object o=eval(program);
		assertType(o, HTMLDocument.class);
		HTMLDocument d=(HTMLDocument)o;
		HTMLImageElement i=(HTMLImageElement)d.getElementById("img");
		assertEquals(14, i.getWidth());
		assertEquals(15, i.getHeight());
	}
	
	/** Tests element.appendChild(new Image()). */
	@Test public void t5() throws ScriptException{
		String program=""
				+ "d=new Document();"
				+ "i=new Image(14, 15);"
				+ "i.id='img';"
				+ "d.body.appendChild(i);"
				+ "d";
		Object o=eval(program);
		assertType(o, HTMLDocument.class);
		HTMLDocument d=(HTMLDocument)o;
		HTMLImageElement i=(HTMLImageElement)d.getElementById("img");
		assertEquals(14, i.getWidth());
		assertEquals(15, i.getHeight());
	}
	
	/** Tests element.insertBefore(new Image()). */
	@Test public void t6() throws ScriptException{
		String program=""
				+ "d=new Document();"
				+ "i=new Image(14, 15);"
				+ "i.id='img';"
				+ "d.body.insertBefore(i, null);"
				+ "d";
		Object o=eval(program);
		assertType(o, HTMLDocument.class);
		HTMLDocument d=(HTMLDocument)o;
		HTMLImageElement i=(HTMLImageElement)d.getElementById("img");
		assertEquals(14, i.getWidth());
		assertEquals(15, i.getHeight());
	}
	
	/** Tests Image.cloneNode(deep). */
	@Test public void tclone() throws ScriptException{
		String program=""
				+ "d=new Document();"
				+ "i=new Image(14, 15);"
				+ "i.id='img';"
				+ "d.body.insertBefore(i, null);"
				+ "d";
		Object o=eval(program);
		assertType(o, HTMLDocument.class);
		HTMLDocument d=(HTMLDocument)o;
		Image i=(Image)d.getElementById("img");
		Image c=i.cloneNode(true);
		assertType(c, Image.class);
	}
	

}
