/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.*;

import org.junit.Test;

import cat.inspiracio.dom.DOMStringMap;

public class DataSetTest extends AbstractTest{

	@Test public void tHas0()throws Exception{
		String script=""
				+ "document=new Document();"
				+ "e=document.createElement('div');"
				+ "e.set('data-bar-foo', 9);"
				+ "e.dataset";
		Object o=eval(script);
		DOMStringMap m=(DOMStringMap)o;
		boolean b=m.has("fooBar");
		assertFalse(b);
	}
	
	@Test public void tHas1()throws Exception{
		String script=""
				+ "document=new Document();"
				+ "e=document.createElement('div');"
				+ "e.set('data-foo-bar', 9);"
				+ "e.dataset";
		Object o=eval(script);
		DOMStringMap m=(DOMStringMap)o;
		boolean b=m.has("fooBar");
		assertTrue(b);
	}
	
	@Test public void tGet0()throws Exception{
		String script=""
				+ "document=new Document();"
				+ "e=document.createElement('div');"
				+ "e.set('data-foo-bar', 9);"
				+ "e.dataset";
		Object o=eval(script);
		DOMStringMap m=(DOMStringMap)o;
		String v=m.get("fooBar");
		assertEquals("9", v);
	}
	
	@Test public void tSet0()throws Exception{
		String script=""
				+ "document=new Document();"
				+ "e=document.createElement('div');"
				+ "e.set('data-foo-bar', 9);"
				+ "e.dataset.set('fooBar', '10');"
				+ "e";
		Object o=eval(script);
		HTMLElement e=(HTMLElement)o;
		String v=e.getAttribute("data-foo-bar");
		assertEquals("10", v);
	}
	
	@Test public void tDelete0()throws Exception{
		String script=""
				+ "document=new Document();"
				+ "e=document.createElement('div');"
				+ "e.set('data-foo-bar', 9);"       //should be "e['data-for-bar'] = 9"
                + "e.dataset.deleter('fooBar');"    //should be "delete e.dataset.fooBar;"
				+ "e";
		Object o=eval(script);
		HTMLElement e=(HTMLElement)o;
		boolean b=e.hasAttribute("data-foo-bar");
		assertFalse(b);
	}
	
}
