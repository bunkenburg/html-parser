/*
Copyright 2017 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;

import org.junit.Test;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 * A full HTML document (with doctype) will be returned as HTMLDocument.
 * A single HTML element will be returned as HTMLElement.
 * A text will be returned as Text.
 * Multiple nodes (for example, text, element, text) will be returned as DocumentFragment.
 * */
public class HTMLBuilderTest extends AbstractTest{

	private HTMLBuilder builder=new HTMLBuilder();
	
	// Document --------------------------
	
	@Test public void tdocument() throws SAXException, IOException{
		String s="<!DOCTYPE html><html><head/><body/></html>";
		Node n=builder.parse(s);
		assertType(n, HTMLDocument.class);
	}
	
	/** If the source is a document without doctype, returns HTMLDocument, not HTMLHtmlElement. */
	@Test public void tdocumentWithoutDoctype() throws SAXException, IOException{
		String s="<html><head/><body/></html>";
		Node n=builder.parse(s);
		assertType(n, HTMLDocument.class);
	}
	
	@Test public void tdocumentEmptyHtml() throws SAXException, IOException{
		String s="<!DOCTYPE html><html/>";
		Node n=builder.parse(s);
		HTMLDocument d=assertType(n, HTMLDocument.class);
		assertNull(d.getHead());
		assertNull(d.getBody());
	}

	@Test public void tdocumentNoHead() throws SAXException, IOException{
		String s="<!DOCTYPE html><html><body/></html>";
		Node n=builder.parse(s);
		HTMLDocument d=assertType(n, HTMLDocument.class);
		assertNull(d.getHead());
		assertNotNull(d.getBody());
	}

	@Test public void tdocumentNoBody() throws SAXException, IOException{
		String s="<!DOCTYPE html><html><head/></html>";
		Node n=builder.parse(s);
		HTMLDocument d=assertType(n, HTMLDocument.class);
		assertNotNull(d.getHead());

		HTMLBodyElement body = d.getBody();
		assertNull(body);
	}

	@Test public void fakeBodyNotEmpty() throws SAXException, IOException{
		String s="<!DOCTYPE html><html><head/><div id='id'/></html>";
		Node n=builder.parse(s);
		HTMLDocument d=assertType(n, HTMLDocument.class);
		assertNotNull(d.getHead());

		//Parsing fakes body
		HTMLBodyElement body = d.getBody();
		assertNotNull(body);

		HTMLElement div = d.getElementById("id");
		assertNotNull(div);
	}

	/** There is a <script> before <head>.
	 * The html5 parser inserts a fake <head> tag, inserts the <script>,
	 * and ignores the later real <head> tag. */
	@Test public void tdocumentScriptBeforeHead() throws SAXException, IOException{
		String s=
				"<!DOCTYPE html>"
				+ "<html>"
				+ "	<script type='server/java'>String title='hola';</script>"
				+ "	<head><title>${genus}</title></head>"
				+ "	<body><h1>${title}</h1></body>"
				+ "</html>";
		Node n=builder.parse(s);
		HTMLDocument d=assertType(n, HTMLDocument.class);
		assertNotNull(d.getHead());
		HTMLElement script=d.getElementByTagName("script");
		assertNotNull(script);
	}
	
	// Element ---------------------------
	
	@Test public void thead() throws SAXException, IOException{
		String s="<head a=3></head>";
		Node n=builder.parse(s);
		assertType(n, HTMLHeadElement.class);
	}
	
	@Test public void tbody() throws SAXException, IOException{
		String s="<body a=3></body>";
		Node n=builder.parse(s);
		assertType(n, HTMLBodyElement.class);
	}
	
	@Test public void tdiv() throws SAXException, IOException{
		String s="<div a=3></div>";
		Node n=builder.parse(s);
		assertType(n, HTMLDivElement.class);
	}

	@Test public void ttable() throws SAXException, IOException{
		String s="<table a=3></table>";
		Node n=builder.parse(s);
		assertType(n, HTMLTableElement.class);
	}

	@Test public void elementFoo() throws SAXException, IOException{
		String s = "<foo a=3></foo>";
		Node n = builder.parse(s);
		assertType(n, HTMLUnknownElement.class);
	}

	// Text ---------------------------------
	
	@Test public void ttext() throws SAXException, IOException{
		String s="hello";
		Node n=builder.parse(s);
		Text t=assertType(n, Text.class);
		assertEquals(s, t.getTextContent());
	}
	
	// fragment -----------------------------
	
	@Test public void tfragment() throws SAXException, IOException{
		String s="hola <b>guapo</b>";
		Node n=builder.parse(s);
		DocumentFragment f=assertType(n, DocumentFragment.class);
		NodeList cs=f.getChildNodes();
		assertEquals(2, cs.getLength());
		n=cs.item(0);
		Text t=assertType(n, Text.class);
		assertEquals("hola ", t.getTextContent());
		n=cs.item(1);
		assertType(n, HTMLElement.class);
	}

	/** This one is not useful. */
	@Test public void tHeadBody() throws SAXException, IOException{
		String s="<head/><body/>";
		Node n=builder.parse(s);
		DocumentFragment f=assertType(n, DocumentFragment.class);
		NodeList cs=f.getChildNodes();
		assertEquals(2, cs.getLength());
		n=cs.item(0);
		assertType(n, HTMLHeadElement.class);
		n=cs.item(1);
		assertType(n, HTMLBodyElement.class);
	}
	
}
