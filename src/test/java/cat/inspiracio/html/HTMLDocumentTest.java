/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.script.ScriptException;

import org.junit.Test;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import cat.inspiracio.dom.HTMLCollection;
import cat.inspiracio.html.host.Document;
import nu.validator.htmlparser.dom.HtmlDocumentBuilder;

public class HTMLDocumentTest extends AbstractTest{

	@Test public void tNewDocument() throws IOException{
		HTMLDocument d=newDocument();
		assertType(d, HTMLDocument.class);
		format(d);
	}

	@Test public void tLastModified(){
		HTMLDocument d=this.newDocument();
		String last=d.getLastModified();
		assertEquals(last, "");
	}
	
	@Test public void tLastModified1() throws IOException, SAXException{
		HTMLDocumentImp d=path("index.html");
		Date now=new Date();
		d.setLastModified(now);
		String last=d.getLastModified();
		
		DateFormat df=new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		String expected=df.format(now);
		assertEquals(last, expected);
	}
	
	@Test public void tclone() throws ScriptException{
		String program=""
				+ "document=new Document();"
				+ "html=document.html;"
				+ "html.id='x';"
				+ "document";
		Object o=eval(program);
		HTMLDocument d=(HTMLDocument)o;
		Node n=d.cloneNode(true);
		assertType(n, HTMLDocument.class);
	}

	@Test public void tLocation() throws SAXException, IOException{
		String url="http://www.inspiracio.cat/index.html";
		String path="index.html";
		HTMLDocument d=path(path);
		Location location=d.getLocation();
		String s=location.toString();
		assertEquals(path, s);
		d.setLocation(url);
		location=d.getLocation();
		s=location.toString();
		assertEquals(url, s);
	}

	/** get(name) has(name) delete(name) */
	@Test public void tGet0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<datalist name='x' id='x'>"
				+ "			<option name='a'>alpha</option>"
				+ "			<option name='b'>beta</option>"
				+ "			<option name='c'>gamma</option>"
				+ "		</datalist>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLDataListElement list=(HTMLDataListElement)d.getElementById("x");
		
		assertTrue(d.has("x"));
		HTMLElement e=d.get("x");
		assertEquals(list, e);
		d.deleter("x");
		assertFalse(d.has("x"));
		assertNull(d.get("x"));
	}

	@Test public void tGetImages0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<datalist name='x' id='x'>"
				+ "			<option name='a'>alpha</option>"
				+ "			<option name='b'>beta</option>"
				+ "			<option name='c'>gamma</option>"
				+ "		</datalist>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLBodyElement body=d.getBody();
		
		HTMLCollection<HTMLImageElement>images=d.getImages();
		assertEquals(0, images.getLength());
		
		HTMLImageElement image=d.createElement(HTMLImageElement.class);
		body.appendChild(image);
		images=d.getImages();
		assertEquals(1, images.getLength());
		assertEquals(image, images.item(0));
		
		image=d.createElement(HTMLImageElement.class);
		body.appendChild(image);
		images=d.getImages();
		assertEquals(2, images.getLength());
		assertEquals(image, images.item(1));
	}

	@Test public void tGetEmbeds0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<datalist name='x' id='x'>"
				+ "			<option name='a'>alpha</option>"
				+ "			<option name='b'>beta</option>"
				+ "			<option name='c'>gamma</option>"
				+ "		</datalist>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLBodyElement body=d.getBody();
		
		HTMLCollection<HTMLEmbedElement>embeds=d.getEmbeds();
		assertEquals(0, embeds.getLength());
		
		HTMLEmbedElement embed=d.createElement(HTMLEmbedElement.class);
		body.appendChild(embed);
		embeds=d.getEmbeds();
		assertEquals(1, embeds.getLength());
		assertEquals(embed, embeds.item(0));
		
		embed=d.createElement(HTMLEmbedElement.class);
		body.appendChild(embed);
		embeds=d.getEmbeds();
		assertEquals(2, embeds.getLength());
		assertEquals(embed, embeds.item(1));
	}

	/** Links: <a> or <area> with href. */
	@Test public void tGetLinks0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLBodyElement body=d.getBody();
		
		HTMLCollection<HTMLElement>links=d.getLinks();
		assertEquals(0, links.getLength());
		
		HTMLAnchorElement a=d.createElement(HTMLAnchorElement.class);
		a.setHref("bla");
		body.appendChild(a);
		links=d.getLinks();
		assertEquals(1, links.getLength());
		assertEquals(a, links.item(0));
		
		HTMLAreaElement area=d.createElement(HTMLAreaElement.class);
		area.setHref("di");
		body.appendChild(area);
		links=d.getLinks();
		assertEquals(2, links.getLength());
		assertEquals(area, links.item(1));

		// <area> without href: doesn't count
		area=d.createElement(HTMLAreaElement.class);
		body.appendChild(area);
		links=d.getLinks();
		assertEquals(2, links.getLength());//still only two
	}

	@Test public void tGetForms0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "		<datalist name='x' id='x'>"
				+ "			<option name='a'>alpha</option>"
				+ "			<option name='b'>beta</option>"
				+ "			<option name='c'>gamma</option>"
				+ "		</datalist>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLBodyElement body=d.getBody();
		
		HTMLCollection<HTMLFormElement>forms=d.getForms();
		assertEquals(0, forms.getLength());
		
		HTMLFormElement form=d.createElement(HTMLFormElement.class);
		body.appendChild(form);
		forms=d.getForms();
		assertEquals(1, forms.getLength());
		assertEquals(form, forms.item(0));
		
		form=d.createElement(HTMLFormElement.class);
		body.appendChild(form);
		forms=d.getForms();
		assertEquals(2, forms.getLength());
		assertEquals(form, forms.item(1));
	}

	@Test public void tGetScripts0() throws Exception{
		String html=""
				+ "<html>"
				+ "	<body>"
				+ "	</body>"
				+ "</html>";
		HTMLDocument d=parse(html);
		HTMLBodyElement body=d.getBody();
		
		HTMLCollection<HTMLScriptElement>scripts=d.getScripts();
		assertEquals(0, scripts.getLength());
		
		HTMLScriptElement script=d.createElement(HTMLScriptElement.class);
		body.appendChild(script);
		scripts=d.getScripts();
		assertEquals(1, scripts.getLength());
		assertEquals(script, scripts.item(0));
		
		script=d.createElement(HTMLScriptElement.class);
		body.appendChild(script);
		scripts=d.getScripts();
		assertEquals(2, scripts.getLength());
		assertEquals(script, scripts.item(1));
	}

	/** referrer: just a setter and getter */
	@Test public void tReferrer() throws SAXException, IOException{
		String url="http://www.yellow.cat/index.html";
		String path="index.html";
		HTMLDocumentImp d=path(path);
		String referrer=d.getReferrer();
		assertEquals("", referrer);
		d.setReferrer(url);
		referrer=d.getReferrer();
		assertEquals(url, referrer);
	}

	/** cookie: just a setter and getter */
	@Test public void tCookie() throws SAXException, IOException{
		String c="username=John Doe ";
		String path="index.html";
		HTMLDocumentImp d=path(path);
		String cookie=d.getCookie();
		assertEquals("", cookie);
		d.setCookie(c);
		cookie=d.getCookie();
		assertEquals(c, cookie);
	}

	/** domain: just a setter and getter */
	@Test public void tDomain0() throws SAXException, IOException{
		String c="inspiracio.cat";
		String path="index.html";
		HTMLDocumentImp d=path(path);
		String domain=d.getDomain();
		assertEquals("", domain);
		d.setDomain(c);
		domain=d.getDomain();
		assertEquals(c, domain);
	}

	/** dir: just a setter and getter */
	@Test public void tDir0() throws SAXException, IOException{
		String c="ltr";
		String path="index.html";
		HTMLDocumentImp d=path(path);
		String dir=d.getDir();
		assertEquals("", dir);
		d.setDir(c);
		dir=d.getDir();
		assertEquals(c, dir);
	}

	@Test public void tcreate0(){
		t(HTMLAnchorElement.class);
		t(HTMLAreaElement.class);
		t(HTMLAudioElement.class);
		t(HTMLBaseElement.class);
		t(HTMLBodyElement.class);
		t(HTMLBRElement.class);
		t(HTMLButtonElement.class);
		t(HTMLDataElement.class);
		t(HTMLDataListElement.class);
		t(HTMLDivElement.class);
		t(HTMLEmbedElement.class);
		t(HTMLFieldSetElement.class);
		t(HTMLFormElement.class);
		t(HTMLHeadElement.class);
		t(HTMLHRElement.class);
		t(HTMLHtmlElement.class);
		t(HTMLIFrameElement.class);
		t(HTMLInputElement.class);
		t(HTMLKeygenElement.class);		
		t(HTMLLabelElement.class);
		t(HTMLLegendElement.class);
		t(HTMLLIElement.class);
		t(HTMLLinkElement.class);
		t(HTMLMapElement.class);
		t(HTMLMetaElement.class);
		t(HTMLMeterElement.class);
		t(HTMLObjectElement.class);
		t(HTMLOListElement.class);
		t(HTMLOptGroupElement.class);
		t(HTMLOptionElement.class);
		t(HTMLOutputElement.class);
		t(HTMLParamElement.class);
		t(HTMLPictureElement.class);
		t(HTMLPreElement.class);
		t(HTMLProgressElement.class);
		t(HTMLQuoteElement.class);
		t(HTMLScriptElement.class);
		t(HTMLSelectElement.class);
		t(HTMLSpanElement.class);
		t(HTMLSourceElement.class);
		t(HTMLStyleElement.class);
		t(HTMLTextAreaElement.class);
		t(HTMLTableElement.class);
		t(HTMLTableCaptionElement.class);
		t(HTMLTableCellElement.class);
		t(HTMLTableColElement.class);
		//t(HTMLTableBodyElement.class);Does not have its own class.
		t(HTMLTableDataCellElement.class);
		//t(HTMLTableHeadElement.class);Does not have its own class.
		t(HTMLTableHeaderCellElement.class);
		t(HTMLTableRowElement.class);
		t(HTMLTemplateElement.class);
		t(HTMLTextAreaElement.class);
		t(HTMLTimeElement.class);
		t(HTMLTitleElement.class);
		//t(HTMLTableFootElement.class);Does not have its own class.
		t(HTMLTableRowElement.class);
		t(HTMLTrackElement.class);
		t(HTMLUListElement.class);
		t(HTMLVideoElement.class);
	}
	
	void t(Class<? extends HTMLElement>c){
		Document d=new Document();
		Object o=d.createElement(c);
		assertType(o, c);
	}

	// testing whether HTMLDocumentImp correctly marks fake elements -----------------------
	
	private HtmlDocumentBuilder htb=new HtmlDocumentBuilder(new HTMLDOMImplementation());

	@Test public void tHtmlReal() throws SAXException, IOException{
		String s="<!DOCTYPE html><html></html>";
		HTMLDocument d=parse_(s);
		assertReal(d.getHtml());
	}

	@Test public void tHtmlFake() throws SAXException, IOException{
		String s="<!DOCTYPE html><head/>";
		HTMLDocument d=parse_(s);
		assertFake(d.getHtml());
	}
	
	@Test public void tHeadReal() throws SAXException, IOException{
		String s="<!DOCTYPE html><html><head/></html>";
		HTMLDocument d=parse_(s);
		assertReal(d.getHead());
	}

	@Test public void tHeadFake() throws SAXException, IOException{
		String s="<!DOCTYPE html><html/>";
		HTMLDocument d=parse_(s);
		assertFake(d.getHead());
	}
	
	@Test public void tHeadFake2() throws SAXException, IOException{
		String s="<!DOCTYPE html><html><body/></html>";
		HTMLDocument d=parse_(s);
		assertFake(d.getHead());
	}
	
	@Test public void tBodyReal() throws SAXException, IOException{
		String s="<!DOCTYPE html><html><body/></html>";
		HTMLDocument d=parse_(s);
		assertReal(d.getBody());
	}

	@Test public void tBodyFake() throws SAXException, IOException{
		String s="<!DOCTYPE html><html/>";
		HTMLDocument d=parse_(s);
		assertFake(d.getBody());
	}
	
	/** Cannot detect this case of fake body */
	@org.junit.Ignore @Test public void tBodyFake2() throws SAXException, IOException{
		String s="<!DOCTYPE html><html><head/></html>";
		HTMLDocument d=parse_(s);
		assertFake(d.getBody());
	}

	private void assertFake(HTMLElement e){assertTrue(isFake(e));}
	private void assertReal(HTMLElement e){assertFalse(isFake(e));}
	private HTMLDocument parse_(String s) throws SAXException, IOException{return (HTMLDocument)htb.parse(new InputSource(new StringReader(s)));}
	private boolean isFake(HTMLElement e){return e.hasAttribute("fake");}
}
