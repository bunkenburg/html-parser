/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

public class HTMLBaseElementTest extends AbstractTest{

	@Test public void tHref() throws IOException, SAXException{
		HTMLDocument d=path("html/base/index.html");
		format(d);

		HTMLBaseElement base=(HTMLBaseElement)d.getElementByTagName("base");
		
		String href=base.getHref();
		assertEmpty(href);
		
		base.setHref("http://www.inspiracio.cat");
		href=base.getHref();
		assertEquals("http://www.inspiracio.cat", href);
	}

	@Test public void tTarget() throws IOException, SAXException{
		HTMLDocument d=path("html/base/index.html");
		format(d);

		HTMLBaseElement base=(HTMLBaseElement)d.getElementByTagName("base");
		
		String target=base.getTarget();
		assertEmpty(target);
		
		base.setTarget("target");
		target=base.getTarget();
		assertEquals("target", target);
	}

}
