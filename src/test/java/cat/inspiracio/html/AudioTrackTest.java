/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AudioTrackTest extends AbstractTest{

    @Test public void tId(){
        AudioTrackImp track=new AudioTrackImp();
        String id="783473";
        track.setId(id);
        assertEquals(id, track.getId());
    }
    
    @Test public void tKind(){
        AudioTrackImp track=new AudioTrackImp();
        String kind="alternative";
        track.setKind(kind);
        assertEquals(kind, track.getKind());
    }
    
    @Test public void tLabel(){
        AudioTrackImp track=new AudioTrackImp();
        String label="Label of the track";
        track.setLabel(label);
        assertEquals(label, track.getLabel());
    }
    
    @Test public void tLanguage(){
        AudioTrackImp track=new AudioTrackImp();
        String language="de-CH";
        track.setLanguage(language);
        assertEquals(language, track.getLanguage());
    }
    
    @Test public void tEnabled(){
        AudioTrack track=new AudioTrackImp();
        boolean enabled=true;
        track.setEnabled(enabled);
        assertEquals(enabled, track.getEnabled());
    }
    
}
