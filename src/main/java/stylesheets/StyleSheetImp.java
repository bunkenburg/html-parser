/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package stylesheets;

import org.w3c.dom.Node;
import org.w3c.dom.stylesheets.MediaList;
import org.w3c.dom.stylesheets.StyleSheet;

/** Java-Bean implementation of StyleSheet.
 *
 *  The <code>StyleSheet</code> interface is the abstract base interface for 
 * any type of style sheet. It represents a single style sheet associated 
 * with a structured document. In HTML, the StyleSheet interface represents 
 * either an external style sheet, included via the HTML  LINK element, or 
 * an inline  STYLE element. In XML, this interface represents an external 
 * style sheet, included via a style sheet processing instruction. 
 * <p>See also the <a href='http://www.w3.org/TR/2000/REC-DOM-Level-2-Style-20001113'>Document Object Model (DOM) Level 2 Style Specification</a>.
 * @since DOM Level 2
 */
public class StyleSheetImp implements StyleSheet {

    private String type;
    private boolean disabled=false;
    private Node node;
    private StyleSheet parent;
    private String href;
    private String title;
    private MediaList media=new MediaListImp();
    
    public StyleSheetImp(){}
    
    /**
     *  This specifies the style sheet language for this style sheet. The 
     * style sheet language is specified as a content type (e.g. 
     * "text/css"). The content type is often specified in the 
     * <code>ownerNode</code>. Also see the type attribute definition for 
     * the <code>LINK</code> element in HTML 4.0, and the type 
     * pseudo-attribute for the XML style sheet processing instruction. 
     */
    @Override public String getType(){return type;}
    public void setType(String t){type=t;}
   
    /**
     *  <code>false</code> if the style sheet is applied to the document. 
     * <code>true</code> if it is not. Modifying this attribute may cause a 
     * new resolution of style for the document. A stylesheet only applies 
     * if both an appropriate medium definition is present and the disabled 
     * attribute is false. So, if the media doesn't apply to the current 
     * user agent, the <code>disabled</code> attribute is ignored. 
     */
    @Override public boolean getDisabled(){return disabled;}

    /**
     *  <code>false</code> if the style sheet is applied to the document. 
     * <code>true</code> if it is not. Modifying this attribute may cause a 
     * new resolution of style for the document. A stylesheet only applies 
     * if both an appropriate medium definition is present and the disabled 
     * attribute is false. So, if the media doesn't apply to the current 
     * user agent, the <code>disabled</code> attribute is ignored. 
     */
    @Override public void setDisabled(boolean d){disabled=d;}

    /**
     *  The node that associates this style sheet with the document. For HTML, 
     * this may be the corresponding <code>LINK</code> or <code>STYLE</code> 
     * element. For XML, it may be the linking processing instruction. For 
     * style sheets that are included by other style sheets, the value of 
     * this attribute is <code>null</code>. 
     */
    @Override public Node getOwnerNode(){return node;}
    public void setOwnerNode(Node n){node=n;}

    /**
     *  For style sheet languages that support the concept of style sheet 
     * inclusion, this attribute represents the including style sheet, if 
     * one exists. If the style sheet is a top-level style sheet, or the 
     * style sheet language does not support inclusion, the value of this 
     * attribute is <code>null</code>. 
     */
    @Override public StyleSheet getParentStyleSheet(){return parent;}
    public void setParentStyleSheet(StyleSheet s){parent=s;}
    
    /**
     *  If the style sheet is a linked style sheet, the value of its attribute 
     * is its location. For inline style sheets, the value of this attribute 
     * is <code>null</code>. See the href attribute definition for the 
     * <code>LINK</code> element in HTML 4.0, and the href pseudo-attribute 
     * for the XML style sheet processing instruction. 
     */
    @Override public String getHref(){return href;}
    public void setHref(String h){href=h;}
    
    /**
     *  The advisory title. The title is often specified in the 
     * <code>ownerNode</code>. See the title attribute definition for the 
     * <code>LINK</code> element in HTML 4.0, and the title pseudo-attribute 
     * for the XML style sheet processing instruction. 
     */
    @Override public String getTitle(){return title;}
    public void setTitle(String t){title=t;}
    
    /**
     *  The intended destination media for style information. The media is 
     * often specified in the <code>ownerNode</code>. If no media has been 
     * specified, the <code>MediaList</code> will be empty. See the media 
     * attribute definition for the <code>LINK</code> element in HTML 4.0, 
     * and the media pseudo-attribute for the XML style sheet processing 
     * instruction . Modifying the media list may cause a change to the 
     * attribute <code>disabled</code>. 
     */
    @Override public MediaList getMedia(){return media;}
    public void setMedia(String m){
        if(media==null)
            media=new MediaListImp();
        media.setMediaText(m);
    }
    
}
