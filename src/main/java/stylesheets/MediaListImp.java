/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package stylesheets;

import org.w3c.dom.DOMException;
import org.w3c.dom.stylesheets.MediaList;

/**
 *  The <code>MediaList</code> interface provides the abstraction of an 
 * ordered collection of media, without defining or constraining how this 
 * collection is implemented. An empty list is the same as a list that 
 * contains the medium <code>"all"</code>. 
 * <p> The items in the <code>MediaList</code> are accessible via an integral 
 * index, starting from 0. 
 * <p>See also the <a href='http://www.w3.org/TR/2000/REC-DOM-Level-2-Style-20001113'>Document Object Model (DOM) Level 2 Style Specification</a>.
 * @since DOM Level 2
 */
public class MediaListImp implements MediaList {

    private String text;
    
    MediaListImp(){}
    MediaListImp(String m){text=m;}
    
    @Override public String getMediaText(){return text;}
    @Override public void setMediaText(String mediaText) throws DOMException {text=mediaText;}

    @Override public int getLength(){throw new UnsupportedOperationException();}
    @Override public String item(int index){throw new UnsupportedOperationException();}
    @Override public void deleteMedium(String oldMedium) throws DOMException {throw new UnsupportedOperationException();}
    @Override public void appendMedium(String newMedium) throws DOMException {throw new UnsupportedOperationException();}

}
