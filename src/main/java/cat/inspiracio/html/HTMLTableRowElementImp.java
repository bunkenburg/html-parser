/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import org.w3c.dom.Node;

import cat.inspiracio.dom.HTMLCollection;
import cat.inspiracio.dom.HTMLCollectionImp;
import cat.inspiracio.dom.IndexSizeError;

class HTMLTableRowElementImp extends HTMLElementImp implements HTMLTableRowElement {
	private static final long serialVersionUID = 1204236995788001549L;

	HTMLTableRowElementImp(HTMLDocumentImp owner){super(owner, "tr");}

	@Override public HTMLTableRowElementImp cloneNode(boolean deep){
		return (HTMLTableRowElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	/** Returns the position of the row in the table's rows list.
	 * Returns −1 if the element isn't in a table. */
	@Override public int getRowIndex(){
		HTMLTableElement table=getTable();
		if(table==null)return -1;
		
		HTMLCollection<HTMLTableRowElement> rows=table.getRows();
		for(int i=0; i<rows.getLength(); i++)
			if(this==rows.item(i))
				return i;
		return -1;
	}

	/** The sectionRowIndex attribute must, if the element has a parent 
	 * table, tbody, thead, or tfoot element, return the index of the 
	 * tr element in the parent element's rows collection 
	 * (for tables, that's the HTMLTableElement.rows collection; 
	 * for table sections, that's the HTMLTableRowElement.rows collection). 
	 * If there is no such parent element, then the attribute must return −1.
	 * */
	@Override public int getSectionRowIndex(){
		Node parent=getParentNode();
		if(parent==null)return -1;
		
		HTMLCollection<HTMLTableRowElement> rows=null;
		
		if(parent instanceof HTMLTableElement){
			HTMLTableElement table=(HTMLTableElement)parent;
			rows=table.getRows();
		}
		
		else if(parent instanceof HTMLTableSectionElement){
			HTMLTableSectionElement section=(HTMLTableSectionElement)parent;
			rows=section.getRows();
		}
		
		if(rows==null)return -1;
		for(int i=0; i<rows.getLength(); i++){
			if(this==rows.item(i))return i;
		}
		return -1;
	}

	/** Returns an HTMLCollection of the td and th elements of the row. 
	 * 
	 * The cells attribute must return an HTMLCollection rooted at the 
	 * tr element, whose filter matches only td and th elements that 
	 * are children of the tr element.*/
	@Override public HTMLCollection<HTMLTableCellElement> getCells(){
		HTMLCollectionImp<HTMLTableCellElement>cells=new HTMLCollectionImp<>();
		HTMLCollection<HTMLElement> children=getChildElements();
		for(HTMLElement e : children)
			if(e instanceof HTMLTableCellElement)
				cells.add((HTMLTableCellElement)e);
		return cells;
	}

	@Override public HTMLTableCellElement insertCell(){return insertCell(-1);}
	
	@Override public HTMLTableCellElement insertCell(int index){
		HTMLCollection<HTMLTableCellElement> cells=getCells();
		
		if(index<-1 || cells.getLength()<index)
			throw new IndexSizeError();
		
		HTMLTableDataCellElement td=createElement(HTMLTableDataCellElement.class);
		HTMLTableCellElement before= index==-1 ? null : cells.item(index);
		insertBefore(td, before);
		
		return td;
	}

	/** The deleteCell(index) method must remove the index'th element in the cells 
	 * collection from its parent. If index is less than zero or greater than or 
	 * equal to the number of elements in the cells collection, the method must 
	 * instead throw an IndexSizeError exception.*/
	@Override public void deleteCell(int index){
		HTMLCollection<HTMLTableCellElement>cells=getCells();
		if(index<0 || cells.getLength()<=index)
			throw new IndexSizeError();
		HTMLTableCellElement cell=cells.item(index);
		Node parent=cell.getParentNode();
		if(parent!=null)parent.removeChild(cell);
	}

	// helpers ----------------------------------------------
	
	/** Gets the table that contains this row, or null. */
	private HTMLTableElement getTable(){
		Node parent=getParentNode();
		while(parent!=null){
			if(parent instanceof HTMLTableElement)
				return (HTMLTableElement)parent;
			parent=parent.getParentNode();
		}
		return null;
	}
}
