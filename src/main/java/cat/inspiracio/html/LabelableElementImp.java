/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import org.w3c.dom.NodeList;

import cat.inspiracio.dom.HTMLCollection;
import cat.inspiracio.dom.HTMLCollectionImp;

abstract class LabelableElementImp extends HTMLElementImp implements LabelableElement {
	private static final long serialVersionUID = 5124125029959755616L;

	// construction -----------------------------------------------
	
	LabelableElementImp(HTMLDocumentImp owner, String tag){super(owner, tag);}
	
	// interface Labelable element --------------------------------
	
	@Override public HTMLCollection<HTMLLabelElement> getLabels(){
		HTMLCollectionImp<HTMLLabelElement>labels=new HTMLCollectionImp<>();
		HTMLDocument d=getOwnerDocument();
		if(d!=null){
			NodeList ls=d.getElementsByTagName("label");//in tree order
			for(int i=0; i<ls.getLength(); i++){
				HTMLLabelElement l=(HTMLLabelElement)ls.item(i);
				HTMLElement control=l.getControl();
				if(control==this)
					labels.add(l);
			}
		}
		return labels;
	}
	
}
