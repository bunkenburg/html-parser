/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

class HTMLProgressElementImp extends LabelableElementImp implements HTMLProgressElement {
	private static final long serialVersionUID = 4200569763563058476L;

	HTMLProgressElementImp(HTMLDocumentImp owner){super(owner, "progress");}

	@Override public HTMLProgressElementImp cloneNode(boolean deep){
		return (HTMLProgressElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public double getValue(){return getAttribute("value", 0.0);}
	@Override public void setValue(double d){setAttribute("value", d);}

	@Override public double getMax(){return getAttribute("max", 1.0);}
	@Override public void setMax(double d){setAttribute("max", d);}

	@Override public double getPosition(){
		if(isDeterminate()){
			double value=getValue();
			double max=getMax();
			return value/max;
		}
		return -1.0;
	}
	
	private boolean isDeterminate(){return hasAttribute("value");}

}
