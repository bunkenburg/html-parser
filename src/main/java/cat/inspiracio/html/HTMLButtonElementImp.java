/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

class HTMLButtonElementImp extends LabelableElementImp implements HTMLButtonElement {
	private static final long serialVersionUID = -7043269595420725890L;

	HTMLButtonElementImp(HTMLDocumentImp owner){super(owner, "button");}

	@Override public HTMLButtonElementImp cloneNode(boolean deep){
		return (HTMLButtonElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

    /** First checks explicit form-attribute for a form-id, then looks for an enclosing form. */
	@Override public HTMLFormElement getForm(){
		if(hasAttribute("form")){
			String id=getAttribute("form");
			return (HTMLFormElement) getElementById(id);
		}
		return super.getForm();
	}

	@Override public boolean getAutofocus(){return getAttributeBoolean("autofocus");}
	@Override public void setAutofocus(boolean auto){setAttribute("autofocus", auto);}

	@Override public boolean getDisabled(){return getAttributeBoolean("disabled");}
	@Override public void setDisabled(boolean b){setAttribute("disabled", b);}

	@Override public String getFormAction(){return getAttribute("formaction");}
	@Override public void setFormAction(String action){setAttribute("formaction", action);}

	@Override public String getFormEncType(){return getAttribute("formenctype");}
	@Override public void setFormEncType(String enc){setAttribute("formenctype", enc);}

	@Override public void setFormMethod(String method){setAttribute("formmethod", method);}
	@Override public String getFormMethod(){return getAttribute("formmethod");}

	@Override public boolean getFormNoValidate(){return getAttributeBoolean("formnovalidate");}
	@Override public void setFormNoValidate(boolean no){setAttribute("formnovalidate", no);}

	@Override public String getFormTarget(){return getAttribute("formtarget");}
	@Override public void setFormTarget(String target){setAttribute("formtarget", target);}

	@Override public String getType(){return getAttribute("type");}
	@Override public void setType(String type){setAttribute("type", type);}

	@Override public String getValue(){return getAttribute("value");}
	@Override public void setValue(String value){setAttribute("value", value);}

	// validation methods -----------------------------------------------
	
	@Override public boolean getWillValidate(){throw new UnsupportedOperationException();}
	@Override public ValidityState getValidity(){throw new UnsupportedOperationException();}
	@Override public String getValidationMessage(){throw new UnsupportedOperationException();}
	@Override public boolean checkValidity(){throw new UnsupportedOperationException();}
	@Override public void setCustomValidity(String error){throw new UnsupportedOperationException();}

}
