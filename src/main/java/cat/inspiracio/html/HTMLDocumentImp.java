/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static java.util.Locale.ENGLISH;
import static org.w3c.dom.UserDataHandler.NODE_CLONED;

import java.io.Serializable;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.xerces.dom.ChildNode;
import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.ElementImpl;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import cat.inspiracio.dom.HTMLCollection;
import cat.inspiracio.dom.HTMLCollectionImp;

/** The class is public only so that we can extend it. */
public class HTMLDocumentImp extends DocumentImpl implements HTMLDocument, Serializable{
	private static final long serialVersionUID = 4414078004022787530L;
	
	// state -----------------------------------
    
	private HTMLDOMImplementation implementation;
	
    private String cookie;
    private String domain;
	private Date lastModified;
	private Location location;
	private String referrer;

	// construction --------------------------
	
	/** Protected: outside this package, only subclasses can instantiate. 
	 * Starts off with no elements at all. 
	 * @deprecated Use new HTMLDocumentImp(HTMLDOMImplementation) */
	protected HTMLDocumentImp(){
		throw new RuntimeException("deprecated");
	}
	
	/** Protected: outside this package, only subclasses can instantiate. 
	 * Starts off with no elements at all.
	 * @param i ... */
	protected HTMLDocumentImp(HTMLDOMImplementation i){
		super();
		implementation=i;
	}
	
	void setImplementation(HTMLDOMImplementation i){
		implementation=i;
	}
	
	/** First adopts the new child, then inserts it.
	 * 
	 * I do this so that we can insert elements that were created with a 
	 * different owner document or no owner document, such as from
	 * "new Image()" in javascript.
	 * 
	 * @param newChild the new child
	 * @param refChild If null, inserts at end.
	 * 
	 * @see org.apache.xerces.dom.NodeImpl#appendChild(org.w3c.dom.Node)
	 */
	@Override public Node insertBefore(Node newChild, Node refChild) throws DOMException{
		adoptNode(newChild);
		return super.insertBefore(newChild, refChild);
	}
	
	/** First adopts the new child, then appends it.
	 * 
	 * I do this so that we can insert elements that were created with a 
	 * different owner document or no owner document, such as from
	 * "new Image()" in javascript.
	 * 
	 * @param newChild the new child
	 * @see org.apache.xerces.dom.NodeImpl#appendChild(org.w3c.dom.Node)
	 */
	@Override public Node appendChild(Node newChild) throws DOMException {
		//We don't need adoptChild(newChild) because super.appendChild() calls insertBefore(n,r).
		return super.appendChild(newChild);
	}

	// ---------------------------------------
	
	//Overwrite these methods because in super-class they instantiate elements,
	//and we want to instantiate our own implementations of the elements.
	
	/** Returns null if there is no html element yet. 
	 * Does not create elements. */
    @Override public HTMLHtmlElement getDocumentElement(){
    	//Maybe this casting is too strong.
    	//It makes toString() fail for documents
    	//that have a wrong root element.
    	return(HTMLHtmlElement)docElement;//from CoreDocumentImpl
    }
    
	/** Returns null if there is no html element yet. Does not create elements. */
    @Override public HTMLHtmlElement getHtml(){return(HTMLHtmlElement)docElement;}
    
    private void setDocumentElement(HTMLHtmlElement html){
    	if(docElement!=null)
    		throw new RuntimeException();// Implement replacing the document element!
    	docElement=(ElementImpl) html;
    	firstChild=(ChildNode)html;
		((HTMLHtmlElementImp)html).setOwned(true);
    }

    /** Returns null if there is no head element yet.
     * Does not create elements. */
	@Override public HTMLHeadElement getHead(){
		return (HTMLHeadElement)getElementByTagName("head");
    }

	/** Gets the text within the first title element within head element, or null. 
	 * Does not create elements. */
    @Override public String getTitle(){
        HTMLHeadElement head=getHead();
        if(head==null)
        	return null;
        NodeList list=head.getElementsByTagName("title");
        if(0<list.getLength()){
            Node title=list.item(0);
            return title.getTextContent();
        }
        return "";
    }
    
	/** Looks for html/head/title, and sets its text body.
	 * If there is no html element, adds one.
	 * If there is no html/head element, adds one.
	 * If there is no title element in html/head, adds one. */
    @Override public synchronized void setTitle(String newTitle){
    	HTMLTitleElement title;

    	HTMLHtmlElement html=getDocumentElement();
    	if(html==null){
    		html=createElement(HTMLHtmlElement.class);
    		setDocumentElement(html);
    	}

    	HTMLHeadElement head=getHead();
    	if(head==null){
    		head=createElement(HTMLHeadElement.class);
    		html.prepend(head);
    	}

    	NodeList titles=head.getElementsByTagName("title");
    	if(titles.getLength()==0){
    		title=createElement(HTMLTitleElement.class);
    		head.appendChild(title);
    	}else{
    		title=(HTMLTitleElement)titles.item(0);
    	}

    	title.setText(newTitle);
    }

    /** Returns null if there is no body. */
    @Override public HTMLBodyElement getBody(){
    	return (HTMLBodyElement)getElementByTagName("body");
    }

    @Override public synchronized void setBody(HTMLBodyElement newBody){
        synchronized ( newBody ){
        	
            // Call getDocumentElement() to get the HTML element that is also the
            // top-level element in the document. Get the first element in the
            // document that is called BODY. Work with that.
            Node html = getDocumentElement();
            Node head = getHead();
            synchronized ( html ){
            	
                NodeList list = getElementsByTagName( "body" );
                if ( list.getLength() > 0 ) {
                    // BODY exists but might not follow HEAD in HTML. If not,
                    // make it so and replace it. Start with the HEAD and make
                    // sure the BODY is the first element after the HEAD.
                    Node body = list.item( 0 );
                    synchronized ( body ){
                        Node child = head;
                        while ( child != null ){
                            if ( child instanceof Element){
                                if ( child != body )
                                    html.insertBefore( newBody, child );
                                else
                                    html.replaceChild( newBody, body );
                                return;
                            }
                            child = child.getNextSibling();
                        }
                        html.appendChild( newBody );
                    }
                    return;
                }
                // BODY does not exist, place it in the HTML element
                // right after the HEAD.
                html.appendChild( newBody );
            }
        }
    }

	@Override public HTMLCollection<HTMLImageElement> getImages(){
		NodeList nodes=getElementsByTagName("img");
		HTMLCollectionImp<HTMLImageElement>images=new HTMLCollectionImp<>();
		for(int i=0; i<nodes.getLength(); i++){
			HTMLImageElement image=(HTMLImageElement)nodes.item(i);
			images.add(image);
		}
		return images;
	}
	
	/**	a-elements with href and area-elements with href, in tree order */
	@Override public HTMLCollection<HTMLElement> getLinks(){
		HTMLCollectionImp<HTMLElement>collection=new HTMLCollectionImp<>();
		HTMLCollection<HTMLElement> all=getAll();		
		for(int i=0; i<all.getLength(); i++){
			HTMLElement e=all.item(i);
			String tag=e.getTagName();
			if(("a".equals(tag) || "area".equals(tag)) && e.hasAttribute("href"))
				collection.add(e);
		}
		return collection;
	}
	
	@Override public HTMLCollection<HTMLFormElement> getForms(){
		NodeList nodes=getElementsByTagName("form");
		HTMLCollectionImp<HTMLFormElement>collection=new HTMLCollectionImp<>();
		for(int i=0; i<nodes.getLength(); i++){
			HTMLFormElement e=(HTMLFormElement)nodes.item(i);
			collection.add(e);
		}
		return collection;
	}
	
	// factory methods -----------------------
    
    @Override public Attr createAttribute(String name)throws DOMException{
        return super.createAttribute(name);
    }

    @Override public HTMLElement createElementNS(String namespaceURI, String qualifiedName, String localpart) throws DOMException {
		throw new UnsupportedOperationException(namespaceURI + qualifiedName + localpart);
	}
	
	/** The HTML5 parser calls here, and this method delegates to createElement(tag)
	 * so that we really get an HTMLElement.
	 * @param namespaceURI The HTML parser calls with "http://www.w3.org/1999/xhtml" 
	 * 	but that's not what we want, so we ignore this parameter.
	 * @param qualifiedName Receive _unqualified_ tag name, like "html", "head", "body". */
	@Override public HTMLElement createElementNS(String namespaceURI, String qualifiedName) {
		String tag=qualifiedName;
		return createElement(tag);
	}

	/** Overrides org.apache.html.dom.HTMLDocumentImpl#createElement(java.lang.String) */
	@Override public HTMLElement createElement(String tag) throws DOMException {
		HTMLElement element=implementation.createElement(this, tag);

// Only correct for:
//		<dependency>
//			<groupId>nu.validator.htmlparser</groupId>
//			<artifactId>htmlparser</artifactId>
//			<version>1.4</version>
//		</dependency>
// (In order to update to
// <dependency>
//			<groupId>nu.validator</groupId>
//			<artifactId>htmlparser</artifactId>
//			<version>1.4.16</version>
//		</dependency>,
// you have to find the new correct ine numbers, or find a different trick.)
// The elements <html>, <head>, <body> may be fake in order to make a complete document.
// Detect whether they are faked or really in the source.
// Fake element creation is in class nu.validator.htmlparser.impl.TreeBuilder:
//
//	<html>
//	In 4789, method appendHtmlElementToDocumentAndPush(), called from
//		1093 
//		1372 
//		2730 
//		3786 <--maybe fake
//
//	<head>
//	In 4797, appendToCurrentNodeAndPushHeadElement(HtmlAttributes), called from
//		1113 
//		1381 
//		2773 
//		3803  <--maybe fake
//
//	<body>
//	In 4817, appendToCurrentNodeAndPushBodyElement(), called from
//		1174 <-- fake
//		1405 
//		2886 <-- fake
//		3846 <-- I think fake
		boolean fake=false;
		if("html".equals(tag)){
			StackTraceElement target=new StackTraceElement(
					"nu.validator.htmlparser.impl.TreeBuilder", 
					"appendHtmlElementToDocumentAndPush", null, 4789);
			StackTraceElement caller=caller(target);
			fake=line(caller, 1093, 2730, 1372);//3786 may be fake
		}
		else if("head".equals(tag)){
			StackTraceElement target=new StackTraceElement(
					"nu.validator.htmlparser.impl.TreeBuilder", 
					"appendToCurrentNodeAndPushHeadElement", null, 4797);
			StackTraceElement caller=caller(target);
			fake=line(caller, 1113, 1381, 2773);//3803 may be fake
		}
		else if("body".equals(tag)){
			StackTraceElement target=new StackTraceElement(
					"nu.validator.htmlparser.impl.TreeBuilder", 
					"appendToCurrentNodeAndPushBodyElement", null, 4817);
			StackTraceElement caller=caller(target);
			fake=line(caller, 1174, 1405, 2886, 3846);
		}
		if(fake)
			element.setAttribute("fake", "true");
		
		return element;
	}

	/** If the stack trace element is not null and its line numbers is one of the 
	 * arguments, returns true. Otherwise false. */
	private boolean line(StackTraceElement e, int... lines){
		if(e==null)
			return false;
		int line=e.getLineNumber();
		for(int i : lines)
			if(i==line)
				return true;
		return false;
	}
	
	/** Returns who called the identified method, or null. 
	 * @param t use method name and line number */
	private StackTraceElement caller(StackTraceElement t){
		StackTraceElement[]stack=new Exception().fillInStackTrace().getStackTrace();
		String method=t.getMethodName();
		int line=t.getLineNumber();
		for(int i=0; i<stack.length; i++){
			StackTraceElement e=stack[i];
			String m=e.getMethodName();
			if(line==e.getLineNumber() && method.equals(m))
				return stack[i+1];//out of bounds if searching for main(String[])
		}
		return null;
	}
	
	/** Overrides org.apache.html.dom.HTMLDocumentImpl#createElement(java.lang.String) */
	@Override public DocumentFragment createDocumentFragment() throws DOMException {
		HTMLDocumentFragmentImp f=new HTMLDocumentFragmentImp(this);
		return f;
	}
	
    @Override public HTMLDOMImplementation getImplementation(){return implementation;}

    /** Create an element of this class.
	 * 
	 * Convenience during java programming: the return type is exact.
	 * 
	 * Not implemented for all element classes.
	 * 
	 * Some element classes are used to several tags,
	 * for those this method does not work. 
	 * 
	 * @param <T> A subinterface of HTMLElement that represents one html element
	 * @param c The class object
	 * @return A fresh instance of the element
	 * @throws UnsupportedOperationException Not (yet) implemented for this interface.
	 * */
	@Override public <T extends HTMLElement> T createElement(Class<T> c){
		return implementation.createElement(this, c);
	}
	
	// Object --------------------------------
	
	/** Formats the whole element. Not efficient. */
	@Override public String toString(){
		try{
			StringWriter writer=new StringWriter();
			DocumentWriter w=new DocumentWriter(writer);
			w.document(this);
			return writer.toString();
		}
		catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	// accessors -----------------------------

    @Override public synchronized HTMLElement getElementById(String id){
        Element e=super.getElementById(id);
        if(e!=null)return (HTMLElement)e;
        return getElementById(id, this);
    }

    /**
     * Recursive method retrieves an element by its <code>id</code> attribute.
     * Called by {@link #getElementById(String)}.
     *
     * @param elementId The <code>id</code> value to look for
     * @return The node in which to look for
     */
    private HTMLElement getElementById( String elementId, Node node ){
        Node child = node.getFirstChild();
        while ( child != null ){
            if ( child instanceof Element ){
                if ( elementId.equals( ( (Element) child ).getAttribute( "id" ) ) )
                    return (HTMLElement) child;
                HTMLElement result = getElementById( elementId, child );
                if ( result != null )
                    return result;
            }
            child = child.getNextSibling();
        }
        return null;
    }

	/** Returns null if there is no such element. */
	HTMLElement getElementByTagName(String tag, int index){
		NodeList nodes = getElementsByTagName(tag);
		Node node = nodes.item(index);
		return (HTMLElement)node;
	}
	
	/** Convenience: gets the first element with that tag,
	 * or null if there are none. */
	@Override public HTMLElement getElementByTagName(String tag){return getElementByTagName(tag, 0);}
	
	/** @param tag Will be treated as lower-case. */
    @Override public final NodeList getElementsByTagName( String tag ){
    	tag=tag.toLowerCase(ENGLISH);
        return super.getElementsByTagName( tag);
    }

    /** @param key will be converted to lower-case */
    @Override public final NodeList getElementsByTagNameNS( String namespace, String key){
    	key=key.toLowerCase(ENGLISH);
    	if ( namespace != null && 0<namespace.length() )
    		return super.getElementsByTagNameNS( namespace, key);
    	return super.getElementsByTagName( key);
    }

	// org.w3c.dom.Document methods --------------------
	
	@Override public HTMLDocumentImp cloneNode(boolean deep){
		HTMLDocumentImp clone=new HTMLDocumentImp();
		clone.copy(this, deep);
		return clone;
	}

	/** Copy fields from a document to this document. 
	 * 
	 * Protected visibility for subclasses. 
	 * @param from the source document 
	 * @param deep whether to copy descendants too */
    protected void copy(HTMLDocumentImp from, boolean deep){
        from.callUserDataHandlers(from, this, NODE_CLONED);
        from.cloneNode(this, deep);
        
        // my state
        cookie=from.cookie;
        domain=from.domain;
        lastModified=from.lastModified;
        location= from.location==null ? null : from.location.clone();
        referrer=from.referrer;
    }

    // HTML5 methods ------------------------------------
    
	/** Just returns the last location that was set. */
    @Override public Location getLocation(){return location;}

    /** Just sets the location, does not navigate. */
    @Override public void setLocation(String l){location=new LocationImp(l);}
    
	@Override public String getLastModified(){
		if(lastModified==null)return "";
		DateFormat df=new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		return df.format(lastModified);
	}
	public void setLastModified(Date d){lastModified=d;}
	
	@Override public String getDir(){
		HTMLElement html=getDocumentElement();
		if(html==null)return "";
		return html.getDir();
	}
	@Override public void setDir(String dir){
		HTMLElement html=getDocumentElement();
		if(html==null)return;
		html.setDir(dir);
	}

	@Override public HTMLCollection<HTMLEmbedElement> getEmbeds(){
		NodeList nodes=getElementsByTagName("embed");
		HTMLCollectionImp<HTMLEmbedElement>collection=new HTMLCollectionImp<>();
		for(int i=0; i<nodes.getLength(); i++){
			HTMLEmbedElement e=(HTMLEmbedElement)nodes.item(i);
			collection.add(e);
		}
		return collection;
	}
	
	@Override public HTMLCollection<HTMLEmbedElement> getPlugins(){return getEmbeds();}
	
	@Override public HTMLCollection<HTMLScriptElement> getScripts(){
		NodeList nodes=getElementsByTagName("script");
		HTMLCollectionImp<HTMLScriptElement>collection=new HTMLCollectionImp<>();
		for(int i=0; i<nodes.getLength(); i++){
			HTMLScriptElement e=(HTMLScriptElement)nodes.item(i);
			collection.add(e);
		}
		return collection;
	}

	@Override public void setDomain(String d){domain=d;}
	@Override public String getDomain(){return domain==null ? "" : domain;}

	/** Not implemented. Makes no sense on server. */
	@Override public DocumentReadyState getReadyState(){throw new UnsupportedOperationException();}

    @Override public String getReferrer(){return referrer==null ? "" : referrer;}
	public void setReferrer(String url){referrer=url;}
    
    @Override public String getCookie(){return cookie==null ? "" : cookie;}
    @Override public void setCookie(String c){cookie=c;}

    // interface ScriptMap<HTMLElement> -----------------------
    
    /** Gets an element by name. */
	@Override public HTMLElement get(String name){
		NodeList list=getElementsByName(name);
		if(list.getLength()==0)return null;
		return (HTMLElement)list.item(0);
	}
	
	/** Has the document got an element with this name? */
	@Override public boolean has(String name){
		NodeList list=getElementsByName(name);
		return 0<list.getLength();
	}
	
	/** Not in spec. Not implemented. */
    @Override public void set(String name, HTMLElement element){throw new UnsupportedOperationException();}

    /** Not in spec. Not implemented. */
    @Override public void set(String name, int value){throw new UnsupportedOperationException();}
	
	/** Deletes a named element. Not in spec. */
	@Override public void deleter(String name){
		HTMLElement e=get(name);
		if(e!=null){
			Node parent=e.getParentNode();
			parent.removeChild(e);
		}
	}

	// dynamic markup insertion ------------------------
	
	@Override public HTMLDocument open(String type){return open(type, "");}
	@Override public HTMLDocument open(String type, String replace){throw new UnsupportedOperationException();}
	@Override public WindowProxy open(String url, String name, String features){return open(url, name, features, false);}
	@Override public WindowProxy open(String url, String name, String features, boolean replace){throw new UnsupportedOperationException();}
	@Override public void write(String... text){throw new UnsupportedOperationException();}
	@Override public void writeln(String... text){throw new UnsupportedOperationException();}
	@Override public void close(){throw new UnsupportedOperationException();}

	@Override public WindowProxy getDefaultView(){throw new UnsupportedOperationException();}

	@Override public Element getActiveElement(){throw new UnsupportedOperationException();}

	@Override public boolean hasFocus(){throw new UnsupportedOperationException();}

	// Document editing API --------------------------------------------------------
	
	@Override public String getDesignMode(){throw new UnsupportedOperationException();}

	@Override public void setDesignMode(String mode){throw new UnsupportedOperationException();}

	@Override public boolean execCommand(String commandId){return execCommand(commandId, false);}
	@Override public boolean execCommand(String commandId, boolean showId){return execCommand(commandId, showId, "");}
	@Override public boolean execCommand(String commandId, boolean showId, String value){throw new UnsupportedOperationException();}

	@Override public boolean queryCommandEnabled(String commandId){throw new UnsupportedOperationException();}
	@Override public boolean queryCommandIndeterm(String commandId){throw new UnsupportedOperationException();}
	@Override public boolean queryCommandState(String commandId){throw new UnsupportedOperationException();}
	@Override public boolean queryCommandSupported(String commandId){throw new UnsupportedOperationException();}
	@Override public String queryCommandValue(String commandId){throw new UnsupportedOperationException();}

	// org.apache.html.dom.HTMLDocument ------------------------------------------------------
	
	// Maybe they can have some useful implementation.
	
	/** Not implemented. */
	@Override public String getURL(){throw new UnsupportedOperationException();}
	
	/** Not implemented. */
	@Override public void setBody(org.w3c.dom.html.HTMLElement body){throw new UnsupportedOperationException();}
	
	/** Not implemented. */
	@Override public org.w3c.dom.html.HTMLCollection getApplets(){throw new UnsupportedOperationException();}
	
	/** Not implemented. */
	@Override public org.w3c.dom.html.HTMLCollection getAnchors(){throw new UnsupportedOperationException();}
	
	/** Not implemented. */
	@Override public void open(){throw new UnsupportedOperationException();}
	
	/** Not implemented. */
	@Override public void write(String text){throw new UnsupportedOperationException();}
	
	/** Not implemented. */
	@Override public void writeln(String text){throw new UnsupportedOperationException();}
	
	@Override public HTMLCollection<HTMLElement> getElementsByName(String name){
		
		HTMLCollectionImp<HTMLElement>elements=new HTMLCollectionImp<>();
		if(name==null || 0==name.length())
			return elements;
		
		//iterate over all elements
		HTMLCollection<HTMLElement> all=getAll();
		for(HTMLElement e : all){
			if(name.equals(e.getName()))
				elements.add(e);
		}
		return elements;
	}
	
	private HTMLCollection<HTMLElement> getAll(){
		HTMLAllCollectionImp all=new HTMLAllCollectionImp();
		all.addAll(getDocumentElement());
		return all;
	}

	/** Not implemented. */
	@Override public EventHandler getOnreadystatechange(){throw new UnsupportedOperationException();}
	
	/** Not implemented. */
	@Override public void setOnreadystatechange(EventHandler h){throw new UnsupportedOperationException();}

}
