/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;


class HTMLTitleElementImp extends HTMLElementImp implements HTMLTitleElement {
	private static final long serialVersionUID = 8086324277127007407L;

	HTMLTitleElementImp(HTMLDocumentImp owner){super(owner, "title");}

	@Override public HTMLTitleElementImp cloneNode(boolean deep){
		return (HTMLTitleElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

    @Override public String getText(){return super.getText();}
	@Override public void setText(String text){setTextContent(text);}

}
