/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import org.w3c.dom.NodeList;

import cat.inspiracio.dom.HTMLCollection;
import cat.inspiracio.dom.HTMLCollectionImp;

class HTMLMapElementImp extends HTMLElementImp implements HTMLMapElement {
	private static final long serialVersionUID = 3721702365769252368L;

	HTMLMapElementImp(HTMLDocumentImp owner){super(owner, "map");}

	@Override public HTMLMapElementImp cloneNode(boolean deep){
		return (HTMLMapElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public String getName(){return getAttribute("name");}
	@Override public void setName(String n){setAttribute("name", n);}

	/** The areas attribute must return an HTMLCollection rooted at the 
	 * map element, whose filter matches only area elements.*/
	@Override public HTMLCollectionImp<HTMLAreaElement> getAreas(){
		NodeList nodes=getElementsByTagName("area");
		HTMLCollectionImp<HTMLAreaElement>areas=new HTMLCollectionImp<>();
		for(int i=0; i<nodes.getLength(); i++)
			areas.add((HTMLAreaElement) nodes.item(i));
		return areas;
	}

	/** The images attribute must return an HTMLCollection rooted at 
	 * the Document node, whose filter matches only img and object 
	 * elements that are associated with this map element according 
	 * to the image map processing model.*/
	@Override public HTMLCollection<HTMLElement> getImages(){
		//<img usemap="#id">
		//<object usemap="#name">
		HTMLCollectionImp.Filter filter=new HTMLCollectionImp.Filter(){
			@Override public boolean accept(HTMLElement e){

				if(e instanceof HTMLImageElement){
					HTMLImageElement image=(HTMLImageElement)e;
					String use=use(image.getUseMap());
					return match(use);
				}
				
				else if(e instanceof HTMLObjectElement){
					HTMLObjectElement object=(HTMLObjectElement)e;
					String use=use(object.getUseMap());
					return match(use);
				}
				
				return false;
			}
			
			String use(String usemap){
				if(!usemap.startsWith("#"))return null;
				return usemap.substring(1);
			}
			
			boolean match(String use){
				if(use==null)return false;
				if(use.equals(HTMLMapElementImp.this.getId()))return true;
				return use.equalsIgnoreCase(HTMLMapElementImp.this.getName());
			}
		};
		
		HTMLDocument d=getOwnerDocument();
		if(d==null)return null;
		HTMLElement root=d.getDocumentElement();
		if(root==null)return null;
		HTMLCollectionImp<HTMLElement>images=new HTMLCollectionImp<HTMLElement>(root, filter);
		return images;
	}

}
