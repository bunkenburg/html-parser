/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

public class HTMLAudioElementImp extends HTMLMediaElementImp implements HTMLAudioElement {
	private static final long serialVersionUID = -3293691915865734331L;

	protected HTMLAudioElementImp(HTMLDocumentImp owner){super(owner, "audio");}

	@Override public HTMLAudioElementImp cloneNode(boolean deep){
		return (HTMLAudioElementImp)super.cloneNode(deep);
	}
	
}
