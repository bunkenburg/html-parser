/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

class HTMLMeterElementImp extends LabelableElementImp implements HTMLMeterElement {
	private static final long serialVersionUID = -5377790521629571744L;

	HTMLMeterElementImp(HTMLDocumentImp owner){super(owner, "meter");}

	@Override public HTMLMeterElementImp cloneNode(boolean deep){
		return (HTMLMeterElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public double getValue(){return getAttribute("value", 0.0);}
	@Override public void setValue(double d){setAttribute("value", d);}

	@Override public double getMin(){return getAttribute("min", 0.0);}
	@Override public void setMin(double d){setAttribute("min", d);}

	@Override public double getMax(){return getAttribute("max", 1.0);}
	@Override public void setMax(double d){setAttribute("max", d);}

	@Override public double getLow(){
		double min=getMin();
		return getAttribute("low", min);
	}
	@Override public void setLow(double d){setAttribute("low", d);}

	@Override public double getHigh(){
		double max=getMax();
		return getAttribute("high", max);
	}
	@Override public void setHigh(double d){setAttribute("high", d);}

	@Override public double getOptimum(){
		double min=getMin();
		double max=getMax();
		double mid=(min+max)/2.0;
		return getAttribute("optimum", mid);
	}
	@Override public void setOptimum(double d){setAttribute("optimum", d);}

}
