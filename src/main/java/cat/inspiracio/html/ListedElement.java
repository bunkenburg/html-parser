/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

/** Listed elements:
 * 
 * Denotes elements that are listed in the form.elements and fieldset.elements APIs.
 * button fieldset input keygen object output select textarea
 * 
 * <a href="http://www.w3.org/TR/html5/forms.html#category-listed">Spec</a>
 * */
interface ListedElement extends HTMLElement{}
