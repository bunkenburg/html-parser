/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import org.w3c.dom.Node;

import cat.inspiracio.dom.HTMLCollection;
import cat.inspiracio.dom.HTMLCollectionImp;
import cat.inspiracio.dom.IndexSizeError;

class HTMLTableSectionElementImp extends HTMLElementImp implements HTMLTableSectionElement {
	private static final long serialVersionUID = -7878825993239107440L;

	/** @param tag "tbody" "thead" "tfoot" */
	HTMLTableSectionElementImp(HTMLDocumentImp owner, String tag){super(owner, tag);}

	@Override public HTMLTableSectionElementImp cloneNode(boolean deep){
		return (HTMLTableSectionElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public HTMLCollection<HTMLTableRowElement> getRows(){
		HTMLCollectionImp<HTMLTableRowElement>rows=new HTMLCollectionImp<>();
		HTMLCollection<HTMLElement> nodes=getElementsByTagName("tr");
		for(HTMLElement e : nodes)
			rows.add((HTMLTableRowElement) e);
		return rows;
	}

	@Override public HTMLTableRowElement insertRow(){return insertRow(-1);}
	
	@Override public HTMLTableRowElement insertRow(int index){
		HTMLCollection<HTMLTableRowElement> rows=getRows();
		
		if(index<-1 || rows.getLength()<index)
			throw new IndexSizeError();
		
		HTMLTableRowElement row=createElement(HTMLTableRowElement.class);
		HTMLTableRowElement before= index==-1 ? null : rows.item(index);
		insertBefore(row, before);
		
		return row;
	}

	@Override public void deleteRow(int index){
		HTMLCollection<HTMLTableRowElement> rows=getRows();
		int length=rows.getLength();
		if(index==-1) index=length-1;//last one
		if(index<=0 || length-1<index)
			throw new IndexSizeError();
		HTMLTableRowElement row=rows.item(index);
		Node parent=row.getParentNode();
		parent.removeChild(row);
	}

}
