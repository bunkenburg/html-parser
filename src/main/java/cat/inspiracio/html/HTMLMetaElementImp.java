/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

public class HTMLMetaElementImp extends HTMLElementImp implements HTMLMetaElement {
	private static final long serialVersionUID = 6786515232979031848L;

	HTMLMetaElementImp(HTMLDocumentImp owner){super(owner, "meta");}

	@Override public HTMLMetaElementImp cloneNode(boolean deep){
		return (HTMLMetaElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public String getContent(){return getAttribute("content");}
	@Override public void setContent(String content){setAttribute("content", content);}

	@Override public String getHttpEquiv(){return getAttribute("http-equiv");}
	@Override public void setHttpEquiv(String httpEquiv){setAttribute("http-equiv", httpEquiv);}

	@Override public String getName(){return getAttribute("name");}
	@Override public void setName(String name){setAttribute("name", name);}

}
