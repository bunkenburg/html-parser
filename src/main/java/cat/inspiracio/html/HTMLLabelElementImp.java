/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import cat.inspiracio.dom.HTMLCollection;

class HTMLLabelElementImp extends HTMLElementImp implements HTMLLabelElement {
	private static final long serialVersionUID = -6947172960276305099L;

	HTMLLabelElementImp(HTMLDocumentImp owner){super(owner, "label");}

	@Override public HTMLLabelElementImp cloneNode(boolean deep){
		return (HTMLLabelElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

    /** First checks explicit form-attribute for a form-id, then looks for an enclosing form. */
	@Override public HTMLFormElement getForm(){
		if(hasAttribute("form")){
			String id=getAttribute("form");
			return (HTMLFormElement) getElementById(id);
		}
		return super.getForm();
	}

	@Override public String getHtmlFor(){return getAttribute("for");}
	@Override public void setHtmlFor(String s){setAttribute("for", s);}

	@Override public HTMLElement getControl(){
		if(hasAttribute("for")){
			HTMLDocument d=getOwnerDocument();
			if(d==null)return null;
			return d.getElementById(getHtmlFor());//may be null
		}
		return getFirstLabelableDescendant(this);//null
	}

	// helpers -----------------------------------------------
	
	/** Returns the first descendant that is is labelable, or null. */
	public LabelableElement getFirstLabelableDescendant(HTMLElement e){
		HTMLCollection<HTMLElement> children=e.getChildElements();
		for(HTMLElement child : children){
			if(child instanceof LabelableElement)
				return (LabelableElement)child;
			LabelableElement f=getFirstLabelableDescendant(child);//null
			if(f!=null)
				return f;
		}
		return null;
	}

}
