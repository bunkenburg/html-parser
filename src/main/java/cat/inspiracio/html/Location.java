/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import cat.inspiracio.url.URLUtils;

/** Draft.
 * 
 * <a href="http://www.w3.org/TR/html51/browsers.html#the-location-interface">spec</a> 
 * */
public interface Location extends URLUtils{

	/** Not implemented. 
	 * 
	 * Navigates to the given page. 
	 * @param url the URL */
	void assign(String url);
	
	/** Not implemented.
	 * 
	 * Removes the current page from the session history and 
	 * navigates to the given page.
	 * 
	 * @param url the URL 
	 * */
	void replace(String url);
	
	/** Not implemented.
	 * 
	 * Reloads the current page.
	 * */
	void reload();
	
	/** Not implemented.
	 * 
	 * [SameObject] readonly attribute DOMString[] ancestorOrigins; 
	 * 
	 * @return the ancestor origins */
	String[] getAncestorOrigins();
	
	Location clone();
	
	/** Just the URL. */
	@Override String toString();
}
