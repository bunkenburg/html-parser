/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import org.w3c.dom.stylesheets.LinkStyle;

/** <a href="http://www.w3.org/TR/html5/document-metadata.html#the-style-element">spec</a> 
 * 
 * For interface LinkStyle see http://dev.w3.org/csswg/cssom/. 
 * */
public interface HTMLStyleElement extends HTMLElement, LinkStyle {

	  public boolean getDisabled();
	  public void setDisabled(boolean disabled);

	  public String getMedia();
	  public void setMedia(String media);

	  public String getType();
	  public void setType(String type);
}
