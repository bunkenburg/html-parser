/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import cat.inspiracio.dom.DOMSettableTokenList;
import cat.inspiracio.dom.DOMSettableTokenListImp;

class HTMLIFrameElementImp extends HTMLElementImp implements HTMLIFrameElement {
	private static final long serialVersionUID = 4587922355316434152L;

	HTMLIFrameElementImp(HTMLDocumentImp owner){super(owner, "iframe");}

	@Override public HTMLIFrameElementImp cloneNode(boolean deep){
		return (HTMLIFrameElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public String getSrc(){return getAttribute("src");}
	@Override public void setSrc(String src){setAttribute("src", src);}

	@Override public String getSrcDoc(){return getAttribute("srcdoc");}
	@Override public void setSrcDoc(String s){setAttribute("srcdoc", s);}

	@Override public String getName(){return getAttribute("name");}
	@Override public void setName(String name){setAttribute("name", name);}

	/** The sandbox attribute, when specified, enables a set of extra restrictions 
	 * on any content hosted by the iframe. Its value must be an unordered set of 
	 * unique space-separated tokens that are ASCII case-insensitive. The allowed 
	 * values are allow-forms, allow-pointer-lock, allow-popups, allow-same-origin, 
	 * allow-scripts, and allow-top-navigation.*/
	@Override public DOMSettableTokenList getSandbox(){return new DOMSettableTokenListImp(this, "sandbox");}

	@Override public int getWidth(){return getAttributeInt("width");}
	@Override public void setWidth(int width){setAttribute("width", width);}

	@Override public int getHeight(){return getAttributeInt("height");}
	@Override public void setHeight(int height){setAttribute("height", height);}

	/** The contentDocument IDL attribute must return the Document object of the 
	 * active document of the iframe element's nested browsing context, if any 
	 * and if its effective script origin is the same origin as the effective 
	 * script origin specified by the incumbent settings object, or null otherwise.*/
	@Override public HTMLDocument getContentDocument(){throw new UnsupportedOperationException();}

	/** The contentWindow IDL attribute must return the WindowProxy object of the 
	 * iframe element's nested browsing context, if any, or null otherwise. */
	@Override public WindowProxy getContentWindow(){throw new UnsupportedOperationException();}

}
