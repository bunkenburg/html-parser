/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import static java.lang.Double.NaN;
import static java.lang.Double.isInfinite;
import static java.lang.Double.isNaN;
import static java.lang.Math.ceil;
import static java.lang.Math.floor;
import static java.lang.Math.round;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.w3c.dom.Element;

import cat.inspiracio.dom.InvalidStateError;
import cat.inspiracio.file.File;
import cat.inspiracio.script.TypeError;

class HTMLInputElementImp extends LabelableElementImp implements HTMLInputElement {
	private static final long serialVersionUID = -1758402136570933694L;
	
	private static final String DATE_PATTERN="yyyy-MM-dd";
	
	HTMLInputElementImp(HTMLDocumentImp owner){super(owner, "input");}

	@Override public HTMLInputElementImp cloneNode(boolean deep){
		return (HTMLInputElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public String getAccept(){return getAttribute("accept");}
	@Override public void setAccept(String accept){setAttribute("accept", accept);}

	@Override public String getAlt(){return getAttribute("alt");}
	@Override public void setAlt(String alt){setAttribute("alt", alt);}

	@Override public String getAutocomplete(){return getAttribute("autocomplete");}
	@Override public void setAutocomplete(String s){setAttribute("autocomplete", s);}

	@Override public boolean getAutofocus(){return getAttributeBoolean("autofocus");}
	@Override public void setAutofocus(boolean auto){setAttribute("autofocus", auto);}

	@Override public boolean getDefaultChecked(){return getAttributeBoolean("checked");}
	@Override public void setDefaultChecked(boolean defaultChecked){setAttribute("checked", defaultChecked);}

	@Override public boolean getChecked(){return getAttributeBoolean("checked");}
	@Override public void setChecked(boolean checked){setAttribute("checked", checked);}

	@Override public String getDirName(){return getAttribute("dirname");}
	@Override public void setDirName(String name){setAttribute("dirname", name);}
	
	@Override public boolean getDisabled(){return getAttributeBoolean("disabled");}
	@Override public void setDisabled(boolean disabled){setAttribute("disabled", disabled);}

    /** First checks explicit form-attribute for a form-id, then looks for an enclosing form. */
	@Override public HTMLFormElement getForm(){
		if(hasAttribute("form")){
			String id=getAttribute("form");
			return (HTMLFormElement)getElementById(id);
		}
		return super.getForm();
	}

	/** Returns just an empty list. */
	@Override public List<File> getFiles(){return new ArrayList<File>();}

	@Override public String getFormAction(){
		String action=getAttribute("formaction");
		if(action==null || 0==action.length()){
			//return document's address
			HTMLDocument d=getOwnerDocument();
			return d.getURL();
		}
		return action;
	}
	@Override public void setFormAction(String action){setAttribute("formaction", action);}

	@Override public String getFormEncType(){
		if(!hasAttribute("formenctype"))
			return "";//There is no missing value default for the formenctype attribute.
		String s=getAttribute("formenctype");
		boolean valid=EncType.isValid(s);
		if(valid)return s;
		return EncType.getInvalidDefault().toString();
	}

	@Override public void setFormEncType(String enc){
		boolean valid=EncType.isValid(enc);
		if(!valid)enc=EncType.getInvalidDefault().toString();
		setAttribute("formenctype", enc);
	}
	
	@Override public String getFormMethod(){return getAttribute("formmethod");}
	@Override public void setFormMethod(String method){setAttribute("formmethod", method);}

	@Override public boolean getFormNoValidate(){return getAttributeBoolean("formnovalidate");}
	@Override public void setFormNoValidate(boolean b){setAttribute("formnovalidate", b);}

	@Override public String getFormTarget(){return getAttribute("formtarget");}
	@Override public void setFormTarget(String target){setAttribute("formtarget", target);}

	@Override public int getHeight(){return getAttributeInt("height");}
	@Override public void setHeight(int height){setAttribute("height", height);}

	@Override public boolean getIndeterminate(){return getAttributeBoolean("indeterminate");}
	@Override public void setIndeterminate(boolean in){setAttribute("indeterminate", in);}

	/** The list IDL attribute must return the current suggestions source element, 
	 * if any, or null otherwise.
	 * 
	 * The suggestions source element is the first element in the document in tree order 
	 * to have an ID equal to the value of the list attribute, if that element is a 
	 * datalist element. If there is no list attribute, or if there is no element with 
	 * that ID, or if the first element with that ID is not a datalist element, then 
	 * there is no suggestions source element. 
	 * 
	 * Alex says: this API is not so good, because I would expect setList(s) and getList()
	 * for the attribute, and getDataList() for the HTML element. */
	@Override public HTMLElement getList(){
		String list=getAttribute("list");
		if(list==null || 0==list.length())return null;
		HTMLDocument d=getOwnerDocument();
		Element e=d.getElementById(list);//should be in tree order
		if(e==null)return null;
		if(e instanceof HTMLDataListElement)
			return (HTMLDataListElement)e;
		return null;
	}

	@Override public String getMax(){return getAttribute("max");}
	@Override public void setMax(String max){setAttribute("max", max);}

	@Override public int getMaxLength(){return getAttributeInt("maxlength");}
	@Override public void setMaxLength(int maxLength){setAttribute("maxlength", maxLength);}

	@Override public String getMin(){return getAttribute("min");}
	@Override public void setMin(String min){setAttribute("min", min);}

	@Override public int getMinLength(){return getAttributeInt("minlength");}
	@Override public void setMinLength(int length){setAttribute("minlength", length);}

	@Override public boolean getMultiple(){return getAttributeBoolean("multiple");}
	@Override public void setMultiple(boolean multiple){setAttribute("multiple", multiple);}

	@Override public String getName(){return getAttribute("name");}
	@Override public void setName(String name){setAttribute("name", name);}

	@Override public String getPattern(){return getAttribute("pattern");}
	@Override public void setPattern(String pattern){setAttribute("pattern", pattern);}

	@Override public String getPlaceHolder(){return getAttribute("placeholder");}
	@Override public void setPlaceHolder(String holder){setAttribute("placeholder", holder);}

	@Override public boolean getReadOnly(){return getAttributeBoolean("readonly");}
	@Override public void setReadOnly(boolean only){setAttribute("readonly", only);}
	
	@Override public boolean getRequired(){return getAttributeBoolean("required");}
	@Override public void setRequired(boolean required){setAttribute("required", required);}

	@Override public int getSize(){return getAttributeInt("size");}
	@Override public void setSize(int size){setAttribute("size", size);}

	@Override public String getSrc(){return getAttribute("src");}
	@Override public void setSrc(String src){setAttribute("src", src);}

	@Override public String getStep(){return getAttribute("step");}
	@Override public void setStep(String step){setAttribute("step", step);}

	/** Should return "" if type is not one of the enumerated values. */
	@Override public String getType(){return getAttribute("type");}
	
	@Override public void setType(String type){
		//Could check whether the argument really is a type.
		//At least put in lower case.
		if(null!=type)
			type=type.toLowerCase();
		setAttribute("type", type);
	}
    
	@SuppressWarnings("unused")
	private boolean isButton(){return "button".equals(getType());}
	@SuppressWarnings("unused")
    private boolean isCheckBox(){return "checkbox".equals(getType());}
	@SuppressWarnings("unused")
    private boolean isColor(){return "color".equals(getType());}
    private boolean isDate(){return "date".equals(getType());}
	@SuppressWarnings("unused")
    private boolean isEmail(){return "email".equals(getType());}
	@SuppressWarnings("unused")
    private boolean isFile(){return "file".equals(getType());}
	@SuppressWarnings("unused")
    private boolean isHidden(){return "hidden".equals(getType());}
	@SuppressWarnings("unused")
    private boolean isImage(){return "image".equals(getType());}
    private boolean isNumber(){return "number".equals(getType());}
	@SuppressWarnings("unused")
    private boolean isPassword(){return "password".equals(getType());}
	@SuppressWarnings("unused")
    private boolean isRadio(){return "radio".equals(getType());}
    private boolean isRange(){return "range".equals(getType());}
	@SuppressWarnings("unused")
    private boolean isReset(){return "reset".equals(getType());}
	@SuppressWarnings("unused")
    private boolean isSearch(){return "search".equals(getType());}
	@SuppressWarnings("unused")
    private boolean isSubmit(){return "submit".equals(getType());}
	@SuppressWarnings("unused")
    private boolean isTel(){return "tel".equals(getType());}
	@SuppressWarnings("unused")
    private boolean isText(){return "text".equals(getType());}
    private boolean isTime(){return "time".equals(getType());}
	@SuppressWarnings("unused")
    private boolean isURL(){return "url".equals(getType());}

	@Override public String getDefaultValue(){return getAttribute("value");}
	@Override public void setDefaultValue(String v){setAttribute("value", v);}
	
	@Override public String getValue(){return getAttribute("value");}
	
	/** Setting value to null or "" is removing it. */
	@Override public void setValue(String v){
		if(v==null || 0==v.length())
			removeAttribute("value");
		else
			setAttribute("value", v);
	}

	/** http://www.w3.org/TR/html5/forms.html#dom-input-valueasdate */
	@Override public Date getValueAsDate(){
		if(!isDate() && !isTime())return null;
		String value=getValue();
		if(isDate())
			return parseDate(value);
		if(isTime())
			return parseTime(value);
		return null;
	}
	@Override public void setValueAsDate(Date d){
		if(!isDate() && !isTime())
			throw new InvalidStateError();
		if(isDate()){
			String s=formatDate(d);
			setAttribute("value", s);
			return;
		}
		if(isTime()){
			String s=formatTime(d);
			setAttribute("value", s);
			return;
		}
	}

	/** http://www.w3.org/TR/html5/forms.html#dom-input-valueasnumber 
	 * Gets the value as double, according to type, or NaN. */
	@Override public double getValueAsNumber(){
	    if(!isDate() && !isTime() && !isNumber() && !isRange())
	        return NaN;
	    if(isDate() || isTime()){
	        Date d=getValueAsDate();
	        if(d==null)
	        	return NaN;
	        return d.getTime();
	    }
	    return getAttribute("value", NaN);
	}
	@Override public void setValueAsNumber(double v){
	    if(isInfinite(v))
	        throw new TypeError();
	    if(!isDate() && !isTime() && !isNumber() && !isRange())
	        throw new InvalidStateError();
	    if(isNaN(v)){
	        setValue("");
	        return;
	    }
	    if(isDate() || isTime()){
	        long l=round(v);
	        Date d=new Date(l);
	        setValueAsDate(d);
	        return;
	    }
	    setAttribute("value", v);
	}

	@Override public int getWidth(){return getAttributeInt("width");}
	@Override public void setWidth(int width){setAttribute("width", width);}

	// Stepping -----------------------------------------------------------------------
	/** Applies to types "date", "time", "number", "range" */
	
	/** http://www.w3.org/TR/html5/forms.html#dom-input-stepup */
	@Override public void stepDown(){step(-1);}//-1 steps upwards
	@Override public void stepDown(int n){step(-n);}//-n steps upwards
	@Override public void stepUp(){step(1);}
	@Override public void stepUp(int n){step(n);}
	
	/** Stepping up or down.
	 * 
	 * @param n How many upwards steps? 
	 * @throws InvalidStateError 
	 * 	The step() function does not apply to this type, or
	 * 	there is no allowed value step. */
	private void step(int n)throws InvalidStateError{
		if(n==0)return;
		boolean down=n<0;
		
		//Does step apply to this type?
		if(!stepApplies())throw new InvalidStateError();
		
		//Have we got a step?
		double step=step();//throws InvalidStateError

		//Check min and max. Must have min < max.
		//3. If the element has a minimum and a maximum and the minimum is greater 
		//than the maximum, then abort these steps.
		double min=min();
		double max=max();
		if(isNumber(min) && isNumber(max)){
			if(max<min)	//Also could use minValid() and maxValid().
				return;
		}
		
		//Get value.
		double value=getValueAsNumber();
		if(isNumber(value)){
			//This is not in spec, but implied by Mozilla's test cases, Firefox's behaviour,
			//and discussion on https://bugzilla.mozilla.org/show_bug.cgi?id=636627.
			//It is not changing the value in those situations:
			//- stepUp() is called and the value is already higher than the maximum valid one;
			//- stepDown() is called and the value is already lower than the minimum.
			if(down){
				double minValid=minValid();
				if(isNumber(minValid) && value<minValid)
					return;//Going downwards but value is already below min valid.
			}
			else{
				double maxValid=maxValid();
				if(isNumber(maxValid) && maxValid<value)
					return;//Going upwards but value is already above max valid.
			}
		}
		else
			value=defaultValue();//Usually 0, except for type=range.

		//Not as spec:
		//If out of step, get into step in the right direction. Reduce n by 1.
		//Then add steps.
		boolean inStep=isInStep(value);
		if(!inStep){
			value=inStep(value, down);
			n = down ? n+1 : n-1;//Reduce n.
		}
		
		// Otherwise (value subtracted from the step base is an integral multiple of 
		//the allowed value step), run the following substeps:
		if(n!=0){
			//1. Let n be the argument.
			//2. Let delta be the allowed value step multiplied by n.
			double delta=step*n;

			//3. If the method invoked was the stepDown() method, negate delta.
			//if(down)delta=-delta;//Don't need that: n is already always upwards.

			//4. Let value be the result of adding delta to value.
			value+=delta;
		}

		// For date inputs, the value can hold a string that is not a day. We do not
		// want to round it, as it might result in a step mismatch. Instead we want to
		// clamp to the next valid value.
		if(isDate() && NS_floorModulo(value-base(), factor()) !=0){
			//assert 0 < step();
			double validStep = EuclidLCM((long)floor(step()), (long)floor(factor()));
			if (0<n) {
				value -= NS_floorModulo(value - base(), validStep);
				value += validStep;
			} else if (n < 0) {
				value -= NS_floorModulo(value - base(), validStep);
			}
		}

		value=adjustBelowMin(value);
		value=adjustAboveMax(value);

		//9. Let value as string be the result of running the algorithm to convert a 
		//number to a string, as defined for the input element's type attribute's 
		//current state, on value.
		//10. Set the value of the element to value as string.
		setValueAsNumber(value);
	}

	/** Least Common Multiple.
	 * from https://dxr.mozilla.org/mozilla-central/source/mfbt/MathAlgorithms.h */
	private long EuclidLCM(long aA, long aB){
		// Divide first to reduce overflow risk.
		return (aA / EuclidGCD(aA, aB)) * aB;
	}
	
	/** Greatest Common Divisor
	 * from https://dxr.mozilla.org/mozilla-central/source/mfbt/MathAlgorithms.h */
	private long EuclidGCD(long aA, long aB){
		// Euclid's algorithm; O(N) in the worst case.  (There are better
		// ways, but we don't need them for the current use of this algorithm.)
		assert 0<aA;
		assert 0<aB;
		while (aA!=aB) {
			if (aB<aA) 
				aA = aA - aB;
			else 
				aB = aB - aA;
		}
		return aA;
	}
	
	/** For a stepping input without initial value, what should the initial value be?
	 * For type=range, the middle, otherwise zero. */
	private double defaultValue(){
		if(isRange()){
			//The default value is the minimum plus half the difference 
			//between the minimum and the maximum, unless the maximum is 
			//less than the minimum, in which case the default value is the minimum.
			double min=min();//or maxValid?
			double max=max();//or minValid?
			if(max<min)
				return min;
			double diff=max-min;
			return min + diff/2;
		}
		return 0;
	}

	private double NS_floorModulo(double x, double y){
		return x - y * floor(x / y);
	}
	
	/** If the value is not in step, find adjust it so that it is in step.
	 * If we are going up, raise the value.
	 * If we are going down, lower it.
	 * 
	 * Adjust step mismatch, upwards or downwards.
	 * @param value Current value. If it's in step, no change.
	 * @param down Should we go downwards? 
	 * @return The new value, may be unchanged. */
	private double inStep(double value, boolean down){
		//6. If value subtracted from the step base is not an integral multiple of 
		//the allowed value step, ...
		//... then set value to the nearest value that, when 
		//subtracted from the step base, is an integral multiple of the allowed value 
		//step, and that is less than value if the method invoked was the stepDown() 
		//and more than value otherwise.
		return down ? lower(value) : raise(value);
	}
	
	/** Returns the smallest value that is in range and in step, or NaN.
	 * If there is no min, returns NaN. 
	 * If there is a min, returns the smallest number that is at least min
	 * and that is in step. This number may be equal to min or higher. */
	private double minValid(){
		double min=min();
		if(isNaN(min))
			return NaN;
		return raise(min);
	}

	/** Returns the highest value that is in range and in step, or NaN.
	 * If there is no max, returns NaN. 
	 * If there is a max, returns the highest number that is at most max
	 * and that is in step. This number may be equal to max or smaller. */
	private double maxValid(){
		double max=max();
		if(isNaN(max))
			return NaN;
		return lower(max);
	}

	/** Returns the smallest number that is at least d and in step. */
	private double raise(double d){
		double base=base();
		if(base==d)
			return d;//d is trivially in step.
		double step=step();//Assume it doesn't fail.
		double diff=d-base;//difference in units
		double steps=diff/step;//difference in steps
		if(isIntegral(steps))
			return d;//same d
		steps=ceil(steps);//raises
		diff=steps*step;
		d=base+diff;
		return d;
	}
	
	private boolean isInStep(double d){
		double base=base();
		if(base==d)
			return true;//d is trivially in-step.
		double step=step();//Assume it doesn't fail.
		double diff=d-base;//difference in units
		double steps=diff/step;//difference in steps
		return isIntegral(steps);
	}
	
	/** Returns the highest number that is at most d and in step. */
	private double lower(double d){
		double base=base();
		if(base==d)
			return d;//d is trivially in step.
		double step=step();//Assume it doesn't fail.
		double diff=d-base;//difference in units
		double steps=diff/step;//difference in steps
		if(isIntegral(steps))
			return d;//same d
		steps=floor(steps);//lowers
		diff=steps*step;
		d=base+diff;
		return d;
	}
	
	/** After stepping, maybe we went below the minimum.
	 * This method adjusts. 
	 * @param value
	 * @return The adjusted value, maybe unchanged. */
	private double adjustBelowMin(double value){
		//Went below minimum.
		//7. If the element has a minimum, and value is less than that minimum, then
		double min=min();
		if(isNumber(min) && value<min){
			//set value to the smallest value that, when subtracted from the step base, 
			//is an integral multiple of the allowed value step, and that is more than 
			//or equal to minimum.
			value=minValid();
		}
		return value;
	}
	
	/** After stepping, maybe we went above the maximum.
	 * This method adjusts. 
	 * @param value
	 * @return The adjusted value, maybe unchanged. */
	private double adjustAboveMax(double value){
		//Went above maximum.
		//8. If the element has a maximum, and value is greater than that maximum, 
		double max=max();
		if(isNumber(max) && max<value){
			//then set value to the largest value that, when subtracted from the step base, 
			//is an integral multiple of the allowed value step, and that is less than 
			//or equal to maximum.
			value=maxValid();
		}
		return value;
	}
	
	/** Check whether step() applies to this type. */
	private boolean stepApplies(){
		return isDate() || isTime() || isNumber() || isRange();
	}
	
	/** Converts a string to double, according to type. 
	 * Throws NumberFormatException if the string cannot be parsed. */
	private double convertToDouble(String s)throws NumberFormatException{
		if(isNumber() || isRange())
			return parseDouble(s);
		
		if(isDate()){
			Date d=parseDate(s);
			if(d==null)
				throw new NumberFormatException(s);
			long ms=d.getTime();
			return ms;
		}
			
		if(isTime()){
			//Parse a time
			//http://www.w3.org/TR/html5/infrastructure.html#parse-a-time-string
			Date d=parseTime(s);
			if(d==null)
				throw new NumberFormatException(s);
			long ms=d.getTime();
			return ms;
		}
		
		//default
		return parseDouble(s);
	}
	
	private double parseDouble(String s){return Double.parseDouble(s);}
	private boolean isIntegral(double d){return d==Math.rint(d);}
	private boolean isNumber(double d){return !Double.isNaN(d);}
	
	/** http://www.w3.org/TR/html5/forms.html#concept-input-min-zero */
	private double base(){
		//Try min.
		//If the element has a min content attribute, and the result of applying 
		//the algorithm to convert a string to a number to the value of the min 
		//content attribute is not an error, then return that result and abort 
		//these steps.
		double min=min();
		if(isNumber(min))
			return min;
		
		//Try value.
		//If the element has a value content attribute, and the result of applying 
		//the algorithm to convert a string to a number to the value of the value 
		//content attribute is not an error, then return that result and abort 
		//these steps.
		double v=getValueAsNumber();
		if(isNumber(v))
			return v;
		
		//If a default step base is defined for this element given its type attribute's
		//state, then return it and abort these steps.
		
		return 0;//Return zero.
	}
	
	/** http://www.w3.org/TR/html5/forms.html#concept-input-min */
	private double min(){
		try{
			String min=getMin();
			return convertToDouble(min);
		}catch(NumberFormatException e){
			return defaultMinimum();
		}
	}
	
	/** http://www.w3.org/TR/html5/forms.html#concept-input-max */
	private double max(){
		try{
			String max=getMax();
			return convertToDouble(max);
		}catch(NumberFormatException e){
			return defaultMaximum();
		}
	}
	
	private double defaultMaximum(){
		if(isRange())
			return 100;
		return NaN;
	}

	private double defaultMinimum(){
		if(isRange())
			return 0;
		return NaN;
	}

	/** http://www.w3.org/TR/html5/forms.html#attr-input-step 
	 * http://www.w3.org/TR/html5/forms.html#concept-input-step
	 * Returns the allowed value step. 
	 * @throws InvalidStateError There is no allowed value step. */
	private double step()throws InvalidStateError{
 		if(!has("step"))
			return defaultStep() * factor();
                                                                                                                      		
 		String s=getStep();
		if("any".equalsIgnoreCase(s))
			throw new InvalidStateError();//no allowed value step

		double step;
		try{
			step=parseDouble(s);
			if(step<=0)
				return defaultStep() * factor();
			return step * factor();
		}
		catch(NumberFormatException e){
			return defaultStep() * factor();
		}
	}
	
	/** http://www.w3.org/TR/html5/forms.html#concept-input-step-default */
	private double defaultStep(){
		if(isDate())
			return 1;//The default step is 1 day.
		
		if(isTime())
			return 60;//The default step is 60 seconds.
		
		if(isNumber())
			return 1;
		
		if(isRange())
			return 1;
		
		throw new UnsupportedOperationException();
	}
	
	private int factor(){ 
		if(isDate())
			return 86400000;//converts days to milliseconds
		
		if(isTime())
			return 1000;//converts the seconds to milliseconds
		
		if(isNumber())
			return 1;
		
		if(isRange())
			return 1;
		
		throw new UnsupportedOperationException();
	}
	
	//Validation: not implemented on server -------------------------------------------
	
	@Override public boolean getWillValidate(){throw new UnsupportedOperationException();}
	@Override public ValidityState getValidity(){throw new UnsupportedOperationException();}
	@Override public String getValidationMessage(){throw new UnsupportedOperationException();}
	@Override public boolean checkValidity(){throw new UnsupportedOperationException();}
	@Override public void setCustomValidity(String error){throw new UnsupportedOperationException();}

	//Selection: not implemented ------------------------------------------------------
	
	@Override public void select(){throw new UnsupportedOperationException();}
	@Override public int getSelectionStart(){throw new UnsupportedOperationException();}
	@Override public void setSelectionStart(int start){throw new UnsupportedOperationException();}
	@Override public int getSelectionEnd(){throw new UnsupportedOperationException();}
	@Override public void setSelectionEnd(int end){throw new UnsupportedOperationException();}
	@Override public String getSelectionDirection(){throw new UnsupportedOperationException();}
	@Override public void setSelectionDirection(String direction){throw new UnsupportedOperationException();}
	@Override public void setRangeText(String replacement){throw new UnsupportedOperationException();}
	@Override public void setRangeText(String replacement, int start, int end){throw new UnsupportedOperationException();}
	@Override public void setRangeText(String replacement, int start, int end, SelectionMode mode){throw new UnsupportedOperationException();}
	@Override public void setRangeText(String replacement, int start, int end, String direction){throw new UnsupportedOperationException();}

	// helpers ---------------------------------------------------------------

	/** http://www.w3.org/TR/html5/infrastructure.html#parse-a-date-string 
	 * Returns null if the string is not a date. */
	private Date parseDate(String s){
		try{
			DateFormat format=new SimpleDateFormat(DATE_PATTERN);
			TimeZone utf=TimeZone.getTimeZone("GMT");
			format.setTimeZone(utf);
			return format.parse(s);
		}catch(ParseException e){
			return null;
		}
	}

	/** http://www.w3.org/TR/html5/infrastructure.html#parse-a-time-string 
	 * Returns null if the string is not a time. */
	Date parseTime(String s){
		//hours  : 0..23 :
		//minutes: 0..59
		//opt. : seconds: 0..59
		//opt. . millis : one, two, or three digits
		if(s==null || 0==s.length())
			return null;
		TimeZone utf=TimeZone.getTimeZone("GMT");
		//Try HH:mm:ss.S
		try{
			DateFormat format=new SimpleDateFormat("HH:mm:ss.S");
			format.setTimeZone(utf);
			return format.parse(s);
		}catch(ParseException e){}
		//Try HH:mm:ss
		try{
			DateFormat format=new SimpleDateFormat("HH:mm:ss");
			format.setTimeZone(utf);
			return format.parse(s);
		}catch(ParseException e){}
		//Try HH:mm
		try{
			DateFormat format=new SimpleDateFormat("HH:mm");
			format.setTimeZone(utf);
			return format.parse(s);
		}catch(ParseException e){}
		return null;//Couldn't parse.
	}

	private String formatDate(Date date){
		DateFormat format=new SimpleDateFormat(DATE_PATTERN);
		TimeZone utf=TimeZone.getTimeZone("GMT");
		format.setTimeZone(utf);
		return format.format(date);
	}
	
	private String formatTime(Date date){
		TimeZone utf=TimeZone.getTimeZone("GMT");
		Calendar calendar=Calendar.getInstance(utf);
		calendar.setTime(date);
		int hours=calendar.get(Calendar.HOUR_OF_DAY);
		int minutes=calendar.get(Calendar.MINUTE);
		int seconds=calendar.get(Calendar.SECOND);
		int millis=calendar.get(Calendar.MILLISECOND);
		StringBuilder builder=new StringBuilder();
		if(hours<10)
			builder.append('0');
		builder.append(hours).append(':');
		if(minutes<10)
			builder.append('0');
		builder.append(minutes);
		if(0<seconds || 0<millis){
			builder.append(':');
			if(seconds<10)
				builder.append('0');
			builder.append(seconds);
			if(0<millis)
				builder.append('.').append(millis);
		}
		return builder.toString();
	}
}
