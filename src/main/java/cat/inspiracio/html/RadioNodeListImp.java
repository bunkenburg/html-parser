/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import cat.inspiracio.dom.HTMLCollectionImp;

class RadioNodeListImp extends HTMLCollectionImp<HTMLElement> implements RadioNodeList {
	private static final long serialVersionUID = -7562939909781098425L;

	RadioNodeListImp(){super();}
	
	// construction -----------------------------

	/** Returns the value of the first checked radio button represented by the object. */
	@Override public String getValue(){
		for(HTMLElement e : elements){
			if(e instanceof HTMLInputElement){
				HTMLInputElement input=(HTMLInputElement)e;
				if("radio".equals(input.getType()) && input.getChecked())
					return input.getValue();
			}
		}
		return null;
	}

	/**  check the first radio button with the given value represented by the object. */
	@Override public void setValue(String v){
		for(HTMLElement e : elements){
			if(e instanceof HTMLInputElement){
				HTMLInputElement input=(HTMLInputElement)e;
				if("radio".equals(input.getType()))
					input.setChecked(v.equals(input.getValue()));
			}
		}
	}

}
