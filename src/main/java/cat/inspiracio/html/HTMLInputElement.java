/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import java.util.Date;
import java.util.List;

import cat.inspiracio.file.File;

/** <a href="http://www.w3.org/TR/html5/forms.html#the-input-element">Spec</a> */
public interface HTMLInputElement extends LabelableElement, ResettableElement, ReassociateableElement {

	String getAccept();
	void setAccept(String accept);
	
	String getAlt();
	void setAlt(String alt);
	
	String getAutocomplete();
	void setAutocomplete(String auto);
	
	boolean getAutofocus();
	void setAutofocus(boolean auto);
	
	boolean getDefaultChecked();
	void setDefaultChecked(boolean b);
	
	boolean getChecked();
	void setChecked(boolean b);
	
	String getDirName();
	void setDirName(String name);
	
	boolean getDisabled();
	void setDisabled(boolean b);
	
	/** @return Returns an empty list of files. */
	List<File> getFiles();
	
    @Override HTMLFormElement getForm();
    
	String getFormAction();
	void setFormAction(String action);
	
	String getFormEncType();
	void setFormEncType(String enc);
	
	String getFormMethod();
	void setFormMethod(String method);
	
	boolean getFormNoValidate();
	void setFormNoValidate(boolean b);
	
	String getFormTarget();
	void setFormTarget(String target);
	
	int getHeight();
	void setHeight(int height);
	
	boolean getIndeterminate();
	void setIndeterminate(boolean in);
	
	HTMLElement getList();
	
	String getMax();
	void setMax(String max);
	
	int getMaxLength();
	void setMaxLength(int length);
	
	String getMin();
	void setMin(String min);
	
	int getMinLength();
	void setMinLength(int length);
	
	boolean getMultiple();
	void setMultiple(boolean multiple);
	
	String getPattern();
	void setPattern(String pattern);
	
	String getPlaceHolder();
	void setPlaceHolder(String holder);
	
	boolean getReadOnly();
	void setReadOnly(boolean b);
	
	boolean getRequired();
	void setRequired(boolean required);
	
	int getSize();
	void setSize(int size);
	
	String getSrc();
	void setSrc(String src);
	
	String getStep();
	void setStep(String step);
	
	String getType();
	void setType(String type);
	
	String getDefaultValue();
	void setDefaultValue(String v);
	
	String getValue();
	void setValue(String v);
	
	Date getValueAsDate();
	void setValueAsDate(Date d);
	
	double getValueAsNumber();
	void setValueAsNumber(double value);
	
	int getWidth();
	void setWidth(int width);
	
	void stepUp();
	void stepUp(int n);
	void stepDown();
	void stepDown(int n);
	
	boolean getWillValidate();
	
	ValidityState getValidity();	

	String getValidationMessage();
	
	boolean checkValidity();
	
	void setCustomValidity(String error);
	
	int getSelectionStart();
	void setSelectionStart(int start);
	
	int getSelectionEnd();
	void setSelectionEnd(int end);
	
	String getSelectionDirection();
	void setSelectionDirection(String direction);
	
	void select();
	
	void setRangeText(String replacement);
	void setRangeText(String replacement, int start, int end);
	void setRangeText(String replacement, int start, int end, SelectionMode mode);
	void setRangeText(String replacement, int start, int end, String direction);
	
}
