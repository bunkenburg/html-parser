/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import cat.inspiracio.dom.HTMLCollection;
import cat.inspiracio.dom.ScriptArray;

/**
 * <a href="http://www.w3.org/TR/html5/forms.html#the-select-element">Spec</a> 
 * 
 * The select element should also act as array of option elements:
 * 
 * 		select[index] = option;
 * 		option = select[index]
 * 
 * This interface extends ScriptArray&lt;HTMLOptionElement&gt; in order to 
 * signal that it wants array notation to work. But whether and how
 * array notation works depends in the script engine that one uses,
 * and probably will use classes of the script engine that are not
 * public API. 
 * */
public interface HTMLSelectElement extends LabelableElement, ScriptArray<HTMLOptionElement>, ResettableElement, ReassociateableElement{
	
	boolean getAutofocus();
	void setAutofocus(boolean auto);
	
	boolean getDisabled();
	void setDisabled(boolean b);
	
	boolean getMultiple();
	void setMultiple(boolean b);
	
	boolean getRequired();
	void setRequired(boolean b);

	int getSize();
	void setSize(int size);

	String getType();
	
	HTMLOptionsCollection getOptions();
	
	@Override int getLength();
	void setLength(int length);
	
	/** Returns the item with index index from the list of options. 
	 * The items are sorted in tree order. 
	 * The same as the getter select[index]. 
	 * @param index the index 
	 * @return the option element */
	HTMLOptionElement item(int index);
	
	HTMLOptionElement namedItem(String name);
	
	void add(HTMLOptionElement option);
	void add(HTMLOptionElement option, HTMLElement before);
	void add(HTMLOptionElement option, int before);
	void add(HTMLOptGroupElement group);
	void add(HTMLOptGroupElement group, HTMLElement before);
	void add(HTMLOptGroupElement group, int before);
	
	/** Removes the element from its parent. 
	 * <a href="http://www.w3.org/TR/dom/#dom-childnode-remove">Spec</a> */
	void remove();
	
	/** Removes an option. 
	 * @param index the index */
	void remove(int index);
	
	/** Inserts an option element, either replacing an existing one or
	 * extending the list of options.
	 * 
	 * setter creator void (unsigned long index, HTMLOptionElement? option); 
	 * @param index the index 
	 * @param option the option */
	@Override void set(int index, HTMLOptionElement option);
	
	@Override boolean has(int index);
	@Override HTMLOptionElement get(int index);
	@Override void delete(int index);
	
	HTMLCollection<HTMLOptionElement> getSelectedOptions();
	
	int getSelectedIndex();
	void setSelectedIndex(int index);

	String getValue();
	void setValue(String string);

	boolean getWillValidate();
	ValidityState getValidity();
	String getValidationMessage();
	boolean checkValidity();
	void setCustomValidity(String error);
	
}
