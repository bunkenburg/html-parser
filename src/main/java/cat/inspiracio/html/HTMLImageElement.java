/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

/** <a href="http://www.w3.org/TR/html5/embedded-content-0.html#the-img-element">spec</a> */
public interface HTMLImageElement extends HTMLElement, FormAssociatedElement {

	String getAlt();
	void setAlt(String alt);
	
	String getSrc();
	void setSrc(String src);
	
	String getCrossOrigin();
	void setCrossOrigin(String c);
	
	String getUseMap();
	void setUseMap(String map);
	
	boolean getIsMap();
	void setIsMap(boolean ismap);
	
	int getWidth();
	void setWidth(int width);
	
	int getHeight();
	void setHeight(int height);

	int getNaturalHeight();

	int getNaturalWidth();
	
	boolean getComplete();

	String getSrcset();
	void setSrcset(String srcset);
	
}
