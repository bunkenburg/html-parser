/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

/** Implementation class for image elements.
 * 
 * It has public visibility so that Image can extend it.
 * */
public class HTMLImageElementImp extends HTMLElementImp implements HTMLImageElement {
	private static final long serialVersionUID = 1449723625809174477L;

	protected HTMLImageElementImp(HTMLDocumentImp owner){super(owner, "img");}

	@Override public HTMLImageElementImp cloneNode(boolean deep){
		return (HTMLImageElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public String getAlt(){return getAttribute("alt");}
	@Override public void setAlt(String alt){setAttribute("alt", alt);}

	@Override public String getSrc(){return getAttribute("src");}
	@Override public void setSrc(String src){setAttribute("src", src);}

	@Override public String getCrossOrigin(){return getAttribute("crossorigin");}
	@Override public void setCrossOrigin(String c){setAttribute("crossorigin", c);}

	@Override public String getUseMap(){return getAttribute("usemap");}
	@Override public void setUseMap(String map){setAttribute("usemap", map);}

	/** The ismap attribute, when used on an element that is a descendant of an 
	 * a element with an href attribute, indicates by its presence that the 
	 * element provides access to a server-side image map. This affects how 
	 * events are handled on the corresponding a element.*/
	@Override public boolean getIsMap(){return getAttributeBoolean("ismap");}
	@Override public void setIsMap(boolean ismap){setAttribute("ismap", ismap);}

	/** Just returns the set value. */
	@Override public int getWidth(){return getAttributeInt("width");}
	@Override public void setWidth(int width){setAttribute("width", width);}

	/** Just returns the set value. */
	@Override public int getHeight(){return getAttributeInt("height");}
	@Override public void setHeight(int height){setAttribute("height", height);}

	/** Not implemented.
	 * 
	 * These attributes return the intrinsic dimensions of the image, 
	 * or zero if the dimensions are not known. */
	@Override public int getNaturalHeight(){throw new UnsupportedOperationException();}

	/** Not implemented.
	 * 
	 * These attributes return the intrinsic dimensions of the image, 
	 * or zero if the dimensions are not known. */
	@Override public int getNaturalWidth(){throw new UnsupportedOperationException();}

	/** Not implemented.
	 * 
	 * Returns true if the image has been completely downloaded or if no image is specified; 
	 * otherwise, returns false. */
	@Override public boolean getComplete(){throw new UnsupportedOperationException();}

	@Override public String getSrcset(){return getAttribute("srcset");}
	@Override public void setSrcset(String srcset){setAttribute("srcset", srcset);}

}
