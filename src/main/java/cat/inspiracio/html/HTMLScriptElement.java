/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

/** <a href="http://www.w3.org/TR/html5/scripting-1.html#the-script-element">spec</a> */
public interface HTMLScriptElement extends HTMLElement {

	String getSrc();
	void setSrc(String src);
	
	String getType();
	void setType(String type);
	
	String getCharset();
	void setCharset(String charset);
	
	boolean getAsync();
	void setAsync(boolean b);
	
	boolean getDefer();
	void setDefer(boolean b);
	
	String getCrossOrigin();
	void setCrossOrigin(String cross);
	
	String getText();
	void setText(String text);
}
