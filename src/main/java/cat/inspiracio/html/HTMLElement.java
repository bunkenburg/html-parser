/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import java.io.Serializable;

import cat.inspiracio.dom.DOMStringMap;
import cat.inspiracio.dom.HTMLCollection;
import cat.inspiracio.script.ScriptMap;

/** 
 * <a href="http://www.w3.org/TR/html5/dom.html#htmlelement">Spec</a>
 * 
 * An html element should also work with map notation:
 * <code>
 * 		element[key] = value;
 * 		value = element[key];
 * </code>
 * I write this aim in this interface by extending ScriptMap&lt;String&gt;,
 * but whether and how it works depends in the details of the script
 * engine you use.
 * */
public interface HTMLElement extends org.w3c.dom.html.HTMLElement, ScriptMap<String>, Serializable {

	// DOM tree manipulation -------------------------------------------
	
	@Override HTMLDocument getOwnerDocument();

	/** Convenience: get all children that are elements. 
	 * @return all elements */
	HTMLCollection<HTMLElement> getChildElements();

    /** Add an element as first child.
     * @param element the element */
    void prepend(HTMLElement element);
    
	// html element -----------------------------------------------
	
	@Override String getTitle();
	@Override void setTitle(String title);

	@Override String getLang();
	@Override void setLang(String lang);

	boolean getTranslate();
	void setTranslate(boolean t);

	@Override String getDir();
	@Override void setDir(String dir);

	DOMStringMap getDataset();
	
    String getStyle();
    void setStyle(String style);
    
    String getName();
    void setName(String n);

    // User interaction ------------------------------------------
	
	boolean getHidden();
	void setHidden(boolean h);

	/** not implemented */
	void click();
	
	int getTabIndex();
	void setTabIndex(int t);
	
	/** not implemented */
	void focus();
	
	/** not implemented */
	void blur();
	
	String getAccessKey();
	void setAccessKey(String key);
	String getAccessKeyLabel();
	
	String getContentEditable();
	
	void setContentEditable(String s);
	
	boolean isContentEditable();
	
	boolean getSpellcheck();
	
	void setSpellcheck(boolean b);
	
}