/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

class HTMLTrackElementImp extends HTMLElementImp implements HTMLTrackElement {
	private static final long serialVersionUID = 5392938104482491572L;

	HTMLTrackElementImp(HTMLDocumentImp owner){super(owner, "track");}

	@Override public HTMLTrackElementImp cloneNode(boolean deep){
		return (HTMLTrackElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public String getKind(){return getAttribute("kind");}
	@Override public void setKind(String s){setAttribute("kind", s);}

	@Override public String getSrc(){return getAttribute("src");}
	@Override public void setSrc(String s){setAttribute("src", s);}

	@Override public String getScrLang(){return getAttribute("srclang");}
	@Override public void setSrcLang(String language){setAttribute("srclang", language);}

	@Override public String getLabel(){return getAttribute("label");}
	@Override public void setLabel(String label){setAttribute("label", label);}

	@Override public boolean getDefault(){return getAttributeBoolean("default");}
	@Override public void setDefault(boolean b){setAttribute("default", b);}

	@Override public int getReadyState(){throw new UnsupportedOperationException();}

	@Override public TextTrack getTrack(){throw new UnsupportedOperationException();}

}
