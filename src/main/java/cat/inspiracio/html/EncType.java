/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

enum EncType {

	application("application/x-www-form-urlencoded"),
	multipart("multipart/form-data"),
	text("text/plain");
	
	// construction --------------------------------
	
	private String value;
	private EncType(String s){value=s;}
	
	// methods -------------------------------------
	
	static EncType getInvalidDefault(){return application;}
	
	static boolean isValid(String s){
		for(EncType type : EncType.values())
			if(type.toString().equals(s))
				return true;
		return false;
	}
	
	@Override public String toString(){return value;}
}
