/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

class HTMLKeygenElementImp extends LabelableElementImp implements HTMLKeygenElement {
	private static final long serialVersionUID = 2813045124940520462L;

	HTMLKeygenElementImp(HTMLDocumentImp owner){super(owner, "keygen");}

	@Override public HTMLKeygenElementImp cloneNode(boolean deep){
		return (HTMLKeygenElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public boolean getAutofocus(){return getAttributeBoolean("autofocus");}
	@Override public void setAutofocus(boolean auto){setAttribute("autofocus", auto);}

	@Override public String getChallenge(){return getAttribute("challenge");}
	@Override public void setChallenge(String challenge){setAttribute("challenge", challenge);}

	@Override public boolean getDisabled(){return getAttributeBoolean("disabled");}
	@Override public void setDisabled(boolean b){setAttribute("disabled", b);}

    /** First checks explicit form-attribute for a form-id, then looks for an enclosing form. */
	@Override public HTMLFormElement getForm(){
		if(hasAttribute("form")){
			String id=getAttribute("form");
			return (HTMLFormElement) getElementById(id);
		}
		return super.getForm();
	}
	
	@Override public String getKeyType(){return getAttribute("keytype");}
	@Override public void setKeyType(String key){setAttribute("keytype", key);}

	@Override public String getName(){return getAttribute("name");}
	@Override public void setName(String name){setAttribute("name", name);}
	
	@Override public String getType(){return "keygen";}

	// validation -------------------------------------------------------------------
	
	@Override public boolean getWillValidate(){throw new UnsupportedOperationException();}
	@Override public ValidityState getValidity(){throw new UnsupportedOperationException();}
	@Override public String getValidationMessage(){throw new UnsupportedOperationException();}
	@Override public boolean checkValidity(){throw new UnsupportedOperationException();}
	@Override public void setCustomValidity(String error){throw new UnsupportedOperationException();}

}
