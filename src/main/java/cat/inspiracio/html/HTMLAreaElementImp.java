/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import cat.inspiracio.dom.DOMTokenList;
import cat.inspiracio.dom.DOMTokenListImp;

/** No state in this class. */
class HTMLAreaElementImp extends URLUtilsImp implements HTMLAreaElement {
	private static final long serialVersionUID = -86166238420872255L;

	HTMLAreaElementImp(HTMLDocumentImp owner){super(owner, "area");}

	@Override public HTMLAreaElementImp cloneNode(boolean deep){
		return (HTMLAreaElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public String getAlt(){return getAttribute("alt");}
	@Override public void setAlt(String s){setAttribute("alt", s);}

	@Override public String getCoords(){return getAttribute("coords");}
	@Override public void setCoords(String s){setAttribute("coords", s);}

	@Override public String getShape(){return getAttribute("shape");}
	@Override public void setShape(String shape){setAttribute("shape", shape);}

	@Override public String getTarget(){return getAttribute("target");}
	@Override public void setTarget(String s){setAttribute("target", s);}

	@Override public String getDownload(){return getAttribute("download");}
	@Override public void setDownload(String s){setAttribute("download", s);}

	@Override public String getRel(){return getAttribute("rel");}
	@Override public void setRel(String rel){setAttribute("rel", rel);}

	@Override public DOMTokenList getRelList(){return new DOMTokenListImp(this, "rel");}

	@Override public String getHrefLang(){return getAttribute("hreflang");}
	@Override public void setHrefLang(String language){setAttribute("hreflang", language);}

	@Override public String getType(){return getAttribute("type");}
	@Override public void setType(String type){setAttribute("type", type);}

}
