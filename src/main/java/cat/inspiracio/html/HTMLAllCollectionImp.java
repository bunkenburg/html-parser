/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import cat.inspiracio.dom.HTMLCollection;
import cat.inspiracio.dom.HTMLCollectionImp;

/** This class should not be public.
 * It should not become an implementation of the obsolete HTMLAllCollection interface. */
class HTMLAllCollectionImp extends HTMLCollectionImp<HTMLElement> {
	private static final long serialVersionUID = -6066260256776189323L;

	// construction -------------------------------------------------------
	
	HTMLAllCollectionImp(){super();}

	/** Adds this elements and all its descendants, in tree order. */
	public void addAll(HTMLElement element){
		if(element==null)return;
		add(element);
		HTMLCollection<HTMLElement>children=element.getChildElements();
		for(HTMLElement child : children)
			addAll(child);
	}

}
