/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

class HTMLTemplateElementImp extends HTMLElementImp implements  HTMLTemplateElement {
	private static final long serialVersionUID = -4826991845357917543L;

	HTMLTemplateElementImp(HTMLDocumentImp owner){super(owner, "template");}

	@Override public HTMLTemplateElementImp cloneNode(boolean deep){
		return (HTMLTemplateElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	/** Returns the contents of the template, as a document fragment.
	 * 
	 * The fragment will be created by this same document,
	 * so that the contents can be inserted without having to 
	 * adopt them. */
	@Override public DocumentFragment getContent(){
		HTMLDocument document=getOwnerDocument();		
		DocumentFragment fragment=document.createDocumentFragment();
		NodeList children=getChildNodes();
		for(int i=0; i<children.getLength(); i++){
			Node n=children.item(i);
			fragment.appendChild(n);
		}
		return fragment;
	}

}
