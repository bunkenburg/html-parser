/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

/** <a href="http://www.w3.org/TR/html5/forms.html#the-keygen-element">Spec</a> */
public interface HTMLKeygenElement extends LabelableElement, ResettableElement, ReassociateableElement {

	boolean getAutofocus();
	void setAutofocus(boolean auto);
	
	String getChallenge();
	void setChallenge(String challenge);
	
	boolean getDisabled();
	void setDisabled(boolean b);
	
	String getKeyType();
	void setKeyType(String key);
	
	String getType();
	
	boolean getWillValidate();
	
	ValidityState getValidity();
	
	String getValidationMessage();
	
	boolean checkValidity();
	
	void setCustomValidity(String error);
	
}
