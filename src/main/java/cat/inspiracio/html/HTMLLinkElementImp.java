/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import org.w3c.dom.stylesheets.StyleSheet;

import stylesheets.StyleSheetImp;
import cat.inspiracio.dom.DOMSettableTokenList;
import cat.inspiracio.dom.DOMSettableTokenListImp;
import cat.inspiracio.dom.DOMTokenList;
import cat.inspiracio.dom.DOMTokenListImp;

class HTMLLinkElementImp extends HTMLElementImp implements HTMLLinkElement {
	private static final long serialVersionUID = 880561487440924242L;

	HTMLLinkElementImp(HTMLDocumentImp document){super(document, "link");}

	@Override public HTMLLinkElementImp cloneNode(boolean deep){
		return (HTMLLinkElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

    @Override public boolean getDisabled(){return getAttributeBoolean("disabled");}
    @Override public void setDisabled(boolean disabled){setAttribute("disabled", disabled);}
    
    @Override public String getHref(){return getAttribute("href");}
    @Override public void setHref(String href){setAttribute("href", href);}
   
	@Override public String getCrossOrigin(){return getAttribute("crossorigin");}
	@Override public void setCrossOrigin(String cross){setAttribute("crossorigin", cross);}

    @Override public String getRel(){return getAttribute("rel");}
    @Override public void setRel( String rel){setAttribute("rel", rel);}
    
    @Override public String getRev(){return getAttribute("rev");}
    @Override public void setRev( String rev){setAttribute("rev", rev);}
    
	@Override public DOMTokenList getRelList(){return new DOMTokenListImp(this, "rel");}

    @Override public String getMedia(){return getAttribute("media");}
    @Override public void setMedia( String media){setAttribute("media", media );}
  
    @Override public String getHreflang(){return getAttribute("hreflang");}
    @Override public void setHreflang( String hreflang ){setAttribute("hreflang", hreflang);}
    
    @Override public String getType(){return getAttribute("type");}
    @Override public void setType(String type){setAttribute("type", type);}

	@Override public DOMSettableTokenList getSizes(){return new DOMSettableTokenListImp(this, "sizes");}

	// interface LinkStyle -------------------------------------------------------
	
	@Override public StyleSheet getSheet(){
        //node must be in a document.
	    if(!isInDocument())
	        return null;
	    
	    //Must have href.
	    String href=getHref();
	    if(null==href || 0==href.length())
	        return null;
	    
	    //Must have rel.
	    String rel=getRel();
        if(null==rel || 0==rel.length())
            return null;
	    
	    //rel contains "stylesheet"
        if(!rel.contains("stylesheet"))
            return null;
        
	    //if rel has "alternate", then must have title
        String title=getTitle();
        if(rel.contains("alternate"))
            if(title==null || 0==title.length())
                return null;
        
	    //type=text/css ---that's what we support
        String type=getType();
        if(type!=null && 0<type.length()){
            if(!"text/css".equals(type))
                return null;
        }
        
        //All good: return a StyleSheet object.
        StyleSheetImp sheet=new StyleSheetImp();
        sheet.setDisabled(false);
        sheet.setHref(href);
        String media=getMedia();
        if(media!=null && 0<media.length())
            sheet.setMedia(media);
        sheet.setOwnerNode(this);
        sheet.setParentStyleSheet(null);
        if(title!=null && 0<title.length())
            sheet.setTitle(title);
        if(type!=null && 0<type.length())
            sheet.setType(type);
        
        return sheet;
	}

}
