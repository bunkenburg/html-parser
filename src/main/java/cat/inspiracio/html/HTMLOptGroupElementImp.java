/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

class HTMLOptGroupElementImp extends HTMLElementImp implements HTMLOptGroupElement {
	private static final long serialVersionUID = -6944525376287382209L;

	HTMLOptGroupElementImp(HTMLDocumentImp owner){super(owner, "optgroup");}

	@Override public HTMLOptGroupElementImp cloneNode(boolean deep){
		return (HTMLOptGroupElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public boolean getDisabled(){return getAttributeBoolean("disabled");}
	@Override public void setDisabled(boolean b){setAttribute("disabled", b);}

	@Override public String getLabel(){return getAttribute("label");}
	@Override public void setLabel(String s){setAttribute("label", s);}

}
