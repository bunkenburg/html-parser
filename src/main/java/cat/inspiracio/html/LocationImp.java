/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import java.io.Serializable;

import cat.inspiracio.url.InitialURLUtils;
import cat.inspiracio.url.URL;
import cat.inspiracio.url.URLSearchParams;

/** Implementation for Location. */
class LocationImp extends InitialURLUtils implements Location, Serializable{
	private static final long serialVersionUID = 1011517266359319691L;
	
	// state --------------------------------
	
	private String location;
	
	// construction -------------------------
	
	public LocationImp(String location){this.location=location;}
	
	// interface Location -------------------------
	
	@Override public void assign(String url){throw new UnsupportedOperationException();}
	@Override public void replace(String url){throw new UnsupportedOperationException();}
	@Override public void reload(){throw new UnsupportedOperationException();}

	@Override public String[] getAncestorOrigins(){throw new UnsupportedOperationException();}

	/** Just returns the URL as string. */
	@Override public String toString(){return location;}
	
	@Override public Location clone(){return new LocationImp(location);}

    // interface URLUtils --------------------------------------------

    private URL getURL(){return new URL(getHref());}
    
    @Override public String getHref(){return location;}
    @Override public void setHref(String href){location=href;}
    private void setHref(URL u){setHref(u.toString());}

    /** http://www.w3.org/TR/url/#concept-url-origin
     * Returns scheme://host:port */
    @Override public String getOrigin(){return getURL().getOrigin();}

    @Override public String getProtocol(){return getURL().getProtocol();}
    @Override public void setProtocol(String protocol){setHref(getURL().protocol(protocol));}
    
    @Override public String getUsername(){return getURL().getUsername();}
    @Override public void setUsername(String u){setHref(getURL().username(u));}
    
    @Override public String getPassword(){return getURL().getPassword();}
    @Override public void setPassword(String p){setHref(getURL().password(p));}
    
    @Override public String getHost(){return getURL().getHost();}
    @Override public void setHost(String host){
        URL u=getURL();
        u.setHost(host);
        setHref(u);
    }
    
    @Override public String getHostname(){return getURL().getHostname();}
    @Override public void setHostname(String name){
        URL u=getURL();
        u.setHostname(name);
        setHref(u);
    }
    
    @Override public String getPort(){return getURL().getPort();}
    @Override public int port(){return getURL().port();}
    @Override public void setPort(int port){setHref(getURL().port(port));}
    @Override public void setPort(String port){
        URL u=getURL();
        u.setPort(port);
        setHref(u);
    }
    
    @Override public String getPathname(){return getURL().getPathname();}
    @Override public void setPathname(String path){
        URL u=getURL();
        u.setPathname(path);
        setHref(u);
    }
    
    @Override public String getSearch(){return getURL().getSearch();}
    @Override public void setSearch(String search){
        URL u=getURL();
        u.setSearch(search);
        setHref(u);
    }
    
    @Override public URLSearchParams getSearchParams(){return getURL().getSearchParams();}
    @Override public void setSearchParams(URLSearchParams params){
        URL u=getURL();
        u.setSearchParams(params);
        setHref(u);
    }
    
    @Override public String getHash(){return getURL().getHash();}
    @Override public void setHash(String hash){
        URL u=getURL();
        u.setHash(hash);
        setHref(u);
    }
}
