/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

class TrackImp implements Track{

    /** Returns the ID of the given track. This is the ID that can be used with 
     * a fragment identifier if the format supports the Media Fragments URI syntax, 
     * and that can be used with the getTrackById() method.
     * 
     * If the track has no identifier, the empty string. */
    private String id = "";

    /** The category of the track, if it has one, or the empty string otherwise.
     * 
     * Only certain values are acceptable:
     * http://www.w3.org/TR/html5/embedded-content-0.html#dom-TrackList-getKind-categories
     * */
    private String kind = "";
    
    /** the label of the track, if it has one, or the empty string otherwise.*/
    private String label = "";
    
    /** he language of the given track, if known, or the empty string otherwise.*/
    private String language;

    protected TrackImp(){}
    
    @Override public String getId(){return id;}
    void setId(String id){this.id=id;}
    
    @Override public String getKind(){return kind;}
    void setKind(String kind){this.kind=kind;}
    
    @Override public String getLabel(){return label;}
    void setLabel(String label){this.label=label;}

    @Override public String getLanguage(){return language;}
    void setLanguage(String language){this.language=language;}

}
