/*
Copyright 2017 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import java.io.StringWriter;

import org.apache.xerces.dom.CoreDocumentImpl;
import org.apache.xerces.dom.DocumentFragmentImpl;
import org.w3c.dom.DocumentFragment;

public class HTMLDocumentFragmentImp extends DocumentFragmentImpl implements DocumentFragment {
	private static final long serialVersionUID = -5528520992709874533L;

	/** Make a new HTML element.
	 * @param owner The document
	 * */
	HTMLDocumentFragmentImp(CoreDocumentImpl owner){
		super(owner);
	}

	@Override public HTMLDocumentFragmentImp cloneNode(boolean deep) {
		return (HTMLDocumentFragmentImp)super.cloneNode(deep);
	}
	
	/** Formats the fragment. Not efficient.
	 * @return all descendants as html string */
	@Override public String toString(){
		try{
			StringWriter writer=new StringWriter();
			new DocumentWriter(writer).fragment(this);
			return writer.toString();
		}
		catch(Exception e){
		   return "[HTMLDocumentFragmentImp]";
		}
	}

}
