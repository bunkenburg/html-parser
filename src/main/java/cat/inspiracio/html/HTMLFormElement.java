/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import org.w3c.dom.Element;

/** <a href="http://www.w3.org/TR/html5/forms.html#the-form-element">Spec</a> 
 * 
 * Javascript-array style access and javascript-map-style access are not implemented.
 * They are equivalent to method item(index) and namedItem(name).
 * */
public interface HTMLFormElement extends HTMLElement {

	String getAcceptCharset();
	void setAcceptCharset(String accept);
	
	String getAction();
	void setAction(String action);
	
	String getAutocomplete();
	void setAutocomplete(String auto);
	
	String getEnctype();
	void setEnctype(String s);
	
	String getEncoding();
	void setEncoding(String s);
	
	String getMethod();
	void setMethod(String s);
	
	boolean getNoValidate();
	void setNoValidate(boolean b);
	
	String getTarget();
	void setTarget(String s);
	
	HTMLFormControlsCollection getElements();
	
	int getLength();
	
	/** getter Element (unsigned long index); 
	 * @param index the index
	 * @return element */
	Element item(int index);
	
	/** getter (RadioNodeList or Element) (DOMString name);
	 * @param name the name 
	 * @return the object */
	Object namedItem(String name);
	
	/** does nothing */
	void submit();
	
	/** does nothing */
	void reset();
	
	boolean checkValidity();
}
