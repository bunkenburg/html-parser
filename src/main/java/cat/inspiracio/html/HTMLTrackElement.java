/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

/** <a href="http://www.w3.org/TR/html5/embedded-content-0.html#the-track-element">spec</a> */
public interface HTMLTrackElement extends HTMLElement{

	String getKind();
	void setKind(String s);
	
	String getSrc();
	void setSrc(String s);
	
	String getScrLang();
	void setSrcLang(String language);
	
	String getLabel();
	void setLabel(String label);
	
	boolean getDefault();
	void setDefault(boolean b);
	
	static final int NONE=0;
	static final int LOADING=1;
	static final int LOADED=2;
	static final int ERROR=3;
	
	int getReadyState();
	
	TextTrack getTrack();
}
