/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import cat.inspiracio.dom.HTMLCollection;
import cat.inspiracio.script.ScriptMap;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.Serializable;

/**
 * <a href="http://www.w3.org/TR/html5/dom.html#the-document-object">Spec</a>
 * <p>
 * A document also acts as a map from string to html element.
 */
public interface HTMLDocument 
	extends org.w3c.dom.html.HTMLDocument, //need this for HTMLDOMImplementation, maybe remove it
	org.w3c.dom.Document,
	ScriptMap<HTMLElement>,
	Serializable
{
	// construction ----------------------------------

	@Override HTMLElement createElement(String tag);

	/** Create an element of this interface.
	 * 
	 * Convenience during java programming: the return type is exact.
	 * 
	 * Not implemented for all element classes.
	 * Some element classes are used to several tags,
	 * so this method would have to choose one. 
	 * 
	 * @param <T> A subinterface of HTMLElement that represents one html element
	 * @param c The class object
	 * @return A fresh instance of the element
	 * @throws UnsupportedOperationException Not (yet) implemented for this interface.
	 * */
	<T extends HTMLElement> T createElement(Class<T> c);
	
	// resource metadata management -------------------
	
	/** The location attribute of the Document interface must 
	 * return the Location object for that Document object, 
	 * if it is in a browsing context, and null otherwise. 
	 * @return the location or null */
	Location getLocation();
	
    /** Just sets the location, does not navigate. 
     * @param location the location */
	void setLocation(String location);
	
	@Override String getDomain();
	void setDomain(String domain);
	
	@Override String getReferrer();
	
	@Override String getCookie();
	@Override void setCookie(String cookie);
	
	String getLastModified();
	
	DocumentReadyState getReadyState();

	// properties --------------------------------------
	
	String getDir();
	void setDir(String dir);
	
	// DOM tree accessors ------------------------------

	@Override public HTMLHtmlElement getDocumentElement();
	HTMLHtmlElement getHtml();
	
	/** Returns the first element with this name. 
	 * Just the first, not all, as spec demands.
	 * If you want all, getElementsByName(name). */
	@Override HTMLElement get(String name);
	
	@Override boolean has(String name);
	
	/** Not in spec. Not implemented. */
	@Override void set(String name, HTMLElement element);
	
	/** Not in spec. Deletes a named element. */
	@Override void deleter(String name);

	@Override String getTitle();
	@Override void setTitle(String title);
	
	HTMLHeadElement getHead();
	
	@Override HTMLBodyElement getBody();
	void setBody(HTMLBodyElement body);
	
	@Override HTMLCollection<HTMLImageElement> getImages();
	HTMLCollection<HTMLEmbedElement> getEmbeds();
	HTMLCollection<HTMLEmbedElement> getPlugins();
	@Override HTMLCollection<HTMLElement> getLinks();
	@Override HTMLCollection<HTMLFormElement> getForms();
	HTMLCollection<HTMLScriptElement> getScripts();
	
    @Override HTMLElement getElementById(String id);
    @Override NodeList getElementsByTagName(String tag);
    
    /** The first element with this tag, or null.
     * Convenience, not in API. 
     * @param tag the tag
     * @return the first element with this tag or null */
    HTMLElement getElementByTagName(String tag);
    
	// dynamic markup insertion ------------------------
	
	/** Not implemented.
	 * Document open(optional DOMString type = "text/html", optional DOMString replace = ""); 
	 * Super class already has open() with return type void. */
	//HTMLDocument open();

	/** Not implemented. 
	 * Document open(optional DOMString type = "text/html", optional DOMString replace = ""); 
	 * @param type "text/html"
	 * @return the document */
	HTMLDocument open(String type);
	
	/** Not implemented.
	 * Document open(optional DOMString type = "text/html", optional DOMString replace = "");
	 * @param type "text/html"
	 * @param replace the string to replace with
	 * @return the document */
	HTMLDocument open(String type, String replace);
	
    void write(String... text);
    
    void writeln(String... text);
    
    @Override void close();
    
	/** Not implemented.
	 * 
	 * Opens a new window.
	 * 
	 * WindowProxy open(DOMString url, DOMString name, DOMString features, optional boolean replace = false); 
	 * @param url the URL
	 * @param name the name
	 * @param features the features
	 * @return the window proxy 
	 * */
	WindowProxy open(String url, String name, String features);
	
	/** Not implemented.
	 * 
	 * Opens a new window.
	 * 
	 * WindowProxy open(DOMString url, DOMString name, DOMString features, optional boolean replace = false); 
	 * @param url the url
	 * @param name the name
	 * @param features the features
	 * @param replace whether to replace
	 * @return the window proxy */
	WindowProxy open(String url, String name, String features, boolean replace);
	
	// user interaction --------------------------------
	
	WindowProxy getDefaultView();

	Element getActiveElement();
	
	boolean hasFocus();
	
	String getDesignMode();

	void setDesignMode(String mode);
	
	boolean execCommand(String commandId);
	boolean execCommand(String commandId, boolean showId);
	boolean execCommand(String commandId, boolean showId, String value);
	
	boolean queryCommandEnabled(String commandId);

	boolean queryCommandIndeterm(String commandId);
	
	boolean queryCommandState(String commandId);
	
	boolean queryCommandSupported(String commandId);
	
	String queryCommandValue(String commandId);
	
	// Event handlers ---------------------------------
	
	EventHandler getOnreadystatechange();
	void setOnreadystatechange(EventHandler h);
}
