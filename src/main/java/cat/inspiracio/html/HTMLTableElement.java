/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import cat.inspiracio.dom.HTMLCollection;

/** <a href="http://www.w3.org/TR/html5/tabular-data.html#the-table-element">spec</a> */
public interface HTMLTableElement extends HTMLElement {
	
	HTMLTableCaptionElement getCaption();
	void setCaption(HTMLTableCaptionElement caption);
	
	HTMLTableCaptionElement createCaption();
	void deleteCaption();
	
	HTMLTableSectionElement getTHead();
	void setTHead(HTMLTableSectionElement head);
	
	HTMLTableSectionElement createTHead();
	void deleteTHead();
	
	HTMLTableSectionElement getTFoot();
	void setTFoot(HTMLTableSectionElement foot);
	
	HTMLTableSectionElement createTFoot();
	void deleteTFoot();
	
	HTMLCollection<HTMLTableSectionElement> getTBodies();
	
	HTMLTableSectionElement createTBody();
	
	HTMLCollection<HTMLTableRowElement> getRows();
	
	HTMLTableRowElement insertRow();
	HTMLTableRowElement insertRow(int index);
	
	void deleteRow(int index);
	
	String getBorder();
	void setBorder(String border);
}
