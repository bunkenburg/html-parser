/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

class HTMLOListElementImp extends HTMLElementImp implements HTMLOListElement {
	private static final long serialVersionUID = 7352768721085030371L;

	HTMLOListElementImp(HTMLDocumentImp owner){super(owner, "ol");}

	@Override public HTMLOListElementImp cloneNode(boolean deep){
		return (HTMLOListElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public void setReversed(boolean reversed){setAttribute("reversed", reversed);}
	@Override public boolean getReversed(){return getAttributeBoolean("reversed");}

	@Override public int getStart(){return getAttributeInt("start");}
	@Override public void setStart(int start){setAttribute("start", start);}

	@Override public String getType(){return getAttribute("type");}
	@Override public void setType(String type){setAttribute("type", type);}

}
