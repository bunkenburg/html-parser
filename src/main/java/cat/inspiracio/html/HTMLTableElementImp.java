/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import cat.inspiracio.dom.HTMLCollection;
import cat.inspiracio.dom.HTMLCollectionImp;
import cat.inspiracio.dom.HierarchyRequestError;
import cat.inspiracio.dom.IndexSizeError;

class HTMLTableElementImp extends HTMLElementImp implements HTMLTableElement {
	private static final long serialVersionUID = -3013705892481191802L;

	HTMLTableElementImp(HTMLDocumentImp owner){super(owner, "table");}

	@Override public HTMLTableElementImp cloneNode(boolean deep){
		return (HTMLTableElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public HTMLTableCaptionElement getCaption(){
		return (HTMLTableCaptionElement)getElementByTagName("caption");
	}

	@Override public synchronized void setCaption(HTMLTableCaptionElement caption){
		if(caption==null)throw new HierarchyRequestError();
        deleteCaption();
        prepend(caption);//caption should be first child
	}

	@Override public synchronized HTMLTableCaptionElement createCaption(){
		HTMLTableCaptionElement caption=getCaption();
		if(caption!=null)return caption;
		caption=createElement(HTMLTableCaptionElement.class);
		prepend(caption);//caption should be first child
		return caption;
	}

	@Override public synchronized void deleteCaption(){
		HTMLTableCaptionElement caption=getCaption();
		if(caption!=null)removeChild(caption);
	}

	
	@Override public HTMLTableSectionElement getTHead(){
		return (HTMLTableSectionElement)getElementByTagName("thead");
	}

	@Override public synchronized void setTHead(HTMLTableSectionElement head){
		if(head==null)throw new HierarchyRequestError();
		deleteTHead();
		insertHead(head);
	}

	@Override public synchronized HTMLTableSectionElement createTHead(){
		HTMLTableSectionElement head=getTHead();
		if(head!=null)return head;
		head=createElement(HTMLTableHeadElement.class);
		insertHead(head);
		return head;
	}

	@Override public synchronized void deleteTHead(){
		HTMLTableSectionElement head=getTHead();
		if(head!=null)removeChild(head);
	}

	/** Inserts a head into the right place,
	 * assuming the table has no head. */
	private void insertHead(HTMLTableSectionElement thead){
		appendChild(thead);//This is not always the right place.
	}

	
	@Override public HTMLTableSectionElement getTFoot() {
		return (HTMLTableSectionElement)getElementByTagName("tfoot");
	}

	@Override public synchronized void setTFoot(HTMLTableSectionElement foot){
		if(foot==null)throw new HierarchyRequestError();
		deleteTFoot();
		insertFoot(foot);
	}

	@Override public synchronized HTMLTableSectionElement createTFoot(){
		HTMLTableSectionElement foot=getTFoot();
		if(foot!=null)return foot;
		foot=createElement(HTMLTableFootElement.class);
		insertFoot(foot);
		return foot;
	}

	@Override public synchronized void deleteTFoot(){
		HTMLTableSectionElement foot=getTFoot();
		if(foot!=null)removeChild(foot);
	}

	/** Insert a foot into the table.
	 * assuming that it has none. */
	private void insertFoot(HTMLTableSectionElement foot){
		appendChild(foot);//Not really the right place.
	}
	
	
	@Override public HTMLCollection<HTMLTableSectionElement> getTBodies(){
		HTMLCollectionImp<HTMLTableSectionElement>bodies=new HTMLCollectionImp<>();
		NodeList nodes=getElementsByTagName("tbody");
		for(int i=0; i<nodes.getLength(); i++){
			HTMLTableSectionElement e=(HTMLTableSectionElement)nodes.item(i);
			bodies.add(e);
		}
		return bodies;
	}

	@Override public synchronized HTMLTableSectionElement createTBody(){
		HTMLTableSectionElement body=createElement(HTMLTableBodyElement.class);
		insertBody(body);
		return body;
	}
	
	/** Inserts a tbody into the table. */
	private void insertBody(HTMLTableSectionElement body){
		appendChild(body);//should control better the right place
	}

	
	@Override public HTMLCollection<HTMLTableRowElement> getRows(){
		HTMLCollectionImp<HTMLTableRowElement>rows=new HTMLCollectionImp<>();
		NodeList nodes=getElementsByTagName("tr");
		for(int i=0; i<nodes.getLength(); i++){
			HTMLTableRowElement e=(HTMLTableRowElement)nodes.item(i);
			rows.add(e);
		}
		return rows;
	}

	/** Creates a tr element, along with a tbody if required, inserts them into 
	 * the table at the position given by the argument, and returns the tr.
	 * 
	 * The position is relative to the rows in the table. The index −1, which 
	 * is the default if the argument is omitted, is equivalent to inserting 
	 * at the end of the table.
	 * 
	 * If the given position is less than −1 or greater than the number of 
	 * rows, throws an IndexSizeError exception.
	 * */
	@Override public HTMLTableRowElement insertRow(){return insertRow(-1);}

	/** Creates a tr element, along with a tbody if required, inserts them into 
	 * the table at the position given by the argument, and returns the tr.
	 * 
	 * The position is relative to the rows in the table. The index −1, which 
	 * is the default if the argument is omitted, is equivalent to inserting 
	 * at the end of the table.
	 * 
	 * If the given position is less than −1 or greater than the number of 
	 * rows, throws an IndexSizeError exception.
	 * */
	@Override public synchronized HTMLTableRowElement insertRow(int index){
		//find or create body
		HTMLTableSectionElement body=null;
		HTMLCollection<HTMLTableSectionElement> bodies=getTBodies();
		if(bodies.getLength()==0)
			body=createTBody();
		else
			body=bodies.item(0);
		HTMLCollection<HTMLTableRowElement> rows=body.getRows();
		
		if(index<-1 || rows.getLength()<index)
			throw new IndexSizeError();
		
		HTMLTableRowElement row=createElement(HTMLTableRowElement.class);
		HTMLTableRowElement before= index==-1 ? null : rows.item(index);
		body.insertBefore(row, before);
		
		return row;
	}

	@Override public synchronized void deleteRow(int index){
		HTMLCollection<HTMLTableRowElement> rows=getRows();
		int length=rows.getLength();
		if(index==-1) index=length-1;//last one
		if(index<=0 || length-1<index)
			throw new IndexSizeError();
		HTMLTableRowElement row=rows.item(index);
		Node parent=row.getParentNode();
		parent.removeChild(row);
	}

	
	@Override public String getBorder(){return getAttribute("border");}
	@Override public void setBorder(String border){setAttribute("border", border);}

}
