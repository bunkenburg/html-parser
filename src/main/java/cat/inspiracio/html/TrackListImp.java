/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import java.util.ArrayList;
import java.util.List;

import cat.inspiracio.dom.EventTargetImp;

class TrackListImp<T extends Track> extends EventTargetImp implements TrackList<T>{

    private List<T> tracks=new ArrayList<T>();
    
    protected TrackListImp(){}

    @Override public int getLength(){return tracks.size();}

    @Override public T item(int index){return tracks.get(index);}

    protected void add(T t){tracks.add(t);}

    @Override public T getTrackById(String id){
        for(T t : tracks)
            if(equals(id, t.getId()))
                return t;
        return null;
    }

    @Override public EventHandler getOnchange() {return null;}
    @Override public void setOnchange(EventHandler h) {}

    @Override public EventHandler getOnaddtrack() {return null;}
    @Override public void setOnaddtrack(EventHandler h) {}

    @Override public EventHandler getOnremovetrack() {return null;}
    @Override public void setOnremovetrack(EventHandler h) {}

    /** equals that works with null */
    protected boolean equals(Object a, Object b) {
        if(a==null)return b==null;
        return a.equals(b);
    }

}
