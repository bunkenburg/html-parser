/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

class HTMLObjectElementImp extends HTMLElementImp implements HTMLObjectElement {
	private static final long serialVersionUID = 3779627927907231679L;

	HTMLObjectElementImp(HTMLDocumentImp owner){super(owner, "object");}

	@Override public HTMLObjectElementImp cloneNode(boolean deep){
		return (HTMLObjectElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public String getData(){return getAttribute("data");}
	@Override public void setData(String data){setAttribute("data", data);}

	@Override public String getType(){return getAttribute("type");}
	@Override public void setType(String type){setAttribute("type", type);}

	@Override public boolean getTypeMustMatch(){return getAttributeBoolean("typemustmatch");}
	@Override public void setTypeMustMatch(boolean match){setAttribute("typemustmatch", match);}

	@Override public String getName(){return getAttribute("name");}
	@Override public void setName(String name){setAttribute("name", name);}

	@Override public String getUseMap(){return getAttribute("usemap");}
	@Override public void setUseMap(String map){setAttribute("usemap", map);}
	
	@Override public String getWidth(){return getAttribute("width");}
	@Override public void setWidth(String w){setAttribute("width", w);}

	@Override public String getHeight(){return getAttribute("height");}
	@Override public void setHeight(String h){setAttribute("height", h);}

	@Override public HTMLDocument getContentDocument(){throw new UnsupportedOperationException();}
	@Override public WindowProxy getContentWindow(){throw new UnsupportedOperationException();}

	// validation ----------------------------------------------------------------
	
	@Override public boolean getWillValidate(){throw new UnsupportedOperationException();}
	@Override public ValidityState getValidity(){throw new UnsupportedOperationException();}
	@Override public String getValidationMessage(){throw new UnsupportedOperationException();}
	@Override public boolean checkValidity(){throw new UnsupportedOperationException();}
	@Override public void setCustomValidity(){throw new UnsupportedOperationException();}

	//  form ---------------------------------------------------------------------
	
    /** First checks explicit form-attribute for a form-id, then looks for an enclosing form. */
	@Override public HTMLFormElement getForm(){
		if(hasAttribute("form")){
			String id=getAttribute("form");
			return (HTMLFormElement) getElementById(id);
		}
		return super.getForm();
	}

}
