/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import cat.inspiracio.dom.ScriptObject;

/** <a href="http://www.w3.org/TR/html5/embedded-content-0.html#the-object-element">spec</a> 
 * 
 * An object element may have arbitrary additional methods.
 **/
public interface HTMLObjectElement extends HTMLElement, ScriptObject, SubmittableElement, ReassociateableElement {

	String getData();
	void setData(String data);
	
	String getType();
	void setType(String type);
	
	boolean getTypeMustMatch();
	void setTypeMustMatch(boolean match);
	
	String getUseMap();
	void setUseMap(String map);
	
	String getWidth();
	void setWidth(String w);
	
	String getHeight();
	void setHeight(String h);
	
	HTMLDocument getContentDocument();

	WindowProxy getContentWindow();
	
	boolean getWillValidate();
	
	ValidityState getValidity();
	
	String getValidationMessage();
	
	boolean checkValidity();
	
	void setCustomValidity();
	
	/** Arbitrary additional methods.
	 * legacycaller any (any... arguments); */
}
