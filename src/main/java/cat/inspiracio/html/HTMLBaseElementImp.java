/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

class HTMLBaseElementImp extends HTMLElementImp implements HTMLBaseElement{
	private static final long serialVersionUID = 7635694307736294758L;

	HTMLBaseElementImp(HTMLDocumentImp document){super(document, "base");}

	@Override public HTMLBaseElementImp cloneNode(boolean deep){
		return (HTMLBaseElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public String getHref(){return getAttribute("href");}
	@Override public void setHref(String href){setAttribute("href", href);}

	@Override public String getTarget(){return getAttribute("target");}
	@Override public void setTarget(String target){setAttribute("target", target);}
}
