/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

/** Not implemented. 
 * 
 * <a href="http://www.w3.org/TR/html5/embedded-content-0.html#texttrack">spec</a>
 **/
public interface TextTrack {

	TextTrackKind getKind();
	String getLabel();
	String getLanguage();

	String getId();
	String getInBandMetadataTrackDispatchType();

	TextTrackMode getMode();

	TextTrackCueList getCues();
	TextTrackCueList getActiveCues();

	void addCue(TextTrackCue cue);
	void removeCue(TextTrackCue cue);

	EventHandler getOncuechange();
}
