/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import java.util.Iterator;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import cat.inspiracio.dom.HierarchyRequestError;
import cat.inspiracio.dom.NotFoundError;

/** A collection view of the option elements in a select element.
 * The collection is live: changing the collection changes the 
 * children of the select element. */
class HTMLOptionsCollectionImp implements HTMLOptionsCollection {
	private static final long serialVersionUID = 3214081886714905353L;
	
	// state ------------------------------------------
	private HTMLSelectElement select;
	
	// construction -----------------------------------
	
	HTMLOptionsCollectionImp(HTMLSelectElement s){select=s;}
	
	// methods -----------------------------------------
	
	@Override public Iterator<HTMLOptionElement> iterator(){
		final NodeList options=getOptions();
		return new Iterator<HTMLOptionElement>(){
			
			/** The next to be delivered */
			int index=0;

			@Override public boolean hasNext(){return index<options.getLength();}

			@Override public HTMLOptionElement next(){return (HTMLOptionElement)options.item(index++);}

			/** Optional method of Iterator that is not implemented here. */
			@Override public void remove(){throw new UnsupportedOperationException();}
		};
	}

	@Override public HTMLOptionElement item(int index){
		return (HTMLOptionElement)getOptions().item(index);
	}

	@Override public int getLength(){return getOptions().getLength();}

	@Override public HTMLOptionElement get(String name){return namedItem(name);}

	/** Modifies the associated select element by inserting an option. 
	 * 
	 * <a href="http://www.w3.org/TR/html5/infrastructure.html#dom-htmloptionscollection-setter">Spec</a>
	 * */
	@Override public void set(int index, HTMLOptionElement option){
		if(option==null){
			remove(index);
			return;
		}
		
		NodeList options=getOptions();
		int length=options.getLength();
		int n=index-length;
		
		if(0<n){
			//append n new option elements
			HTMLDocument document=select.getOwnerDocument();
			for(int i=0; i<n; i++){
				HTMLElement o=document.createElement("option");
				select.appendChild(o);
			}
		}
		
		if(0<=n){
			select.appendChild(option);
		}else{
			Node o=options.item(index);//to be replaced
			select.replaceChild(option, o);
		}
	}

	/** On setting, the behaviour depends on whether the new value is equal to, 
	 * greater than, or less than the number of nodes represented by the collection 
	 * at that time. If the number is the same, then setting the attribute must do 
	 * nothing. If the new value is greater, then n new option elements with no attributes 
	 * and no child nodes must be appended to the select element on which the 
	 * HTMLOptionsCollection is rooted, where n is the difference between the two numbers 
	 * (new value minus old value).
	 * 
	 * [From spec, but this implementation does not fire events:
	 * Mutation events must be fired as if a DocumentFragment containing the new option 
	 * elements had been inserted. 
	 * ]
	 * 
	 * If the new value is lower, then the last n nodes in the collection must be removed 
	 * from their parent nodes, where n is the difference between the two numbers 
	 * (old value minus new value).
	 * 
	 * Setting length never removes or adds any optgroup elements, and never adds new 
	 * children to existing optgroup elements (though it can remove children from them).
	 * */
	@Override public void setLength(int length){
		NodeList options=getOptions();
		int old=options.getLength();
		
		if(old==length)return;
		
		else if(old<length){
			int n=length-old;
			//Append n new option elements
			HTMLDocument d=select.getOwnerDocument();
			for(int i=0; i<n; i++){
				HTMLElement o=d.createElement("option");
				select.appendChild(o);
			}
		}
		
		else if(length<old){
			int n=old-length;
			//Delete last n options elements
			for(int i=0; i<n; i++){
				int last=old-i-1;
				Node o=options.item(last);
				Node parent=o.getParentNode();
				parent.removeChild(o);
			}
		}
	}

	/** Gets the option element with this name, or null. */
	@Override public HTMLOptionElement namedItem(String name){
		if(name==null || 0==name.length())return null;
		NodeList options=getOptions();
		int length=options.getLength();
		for(int i=0; i<length; i++){
			HTMLOptionElement o=(HTMLOptionElement)options.item(i);
			String n=o.getName();
			if(name.equals(n))return o;
		}
		return null;
	}

	/** <a href="http://www.w3.org/TR/html5/infrastructure.html#dom-htmloptionscollection-add">Spec</a> */
	@Override public void add(HTMLOptionElement option){add(option, null);}
	
	/** Tests whether an element is an ancestor of another. 
	 * 
	 * Equals: false. (This is probably another special case for the client.)
	 * 
	 * @param ancestor If null, return false.
	 * @param descendant If null, return false.
	 * 
	 * @return Whether ancestor really is an ancestor of descendant. 
	 * */
	private boolean isAncestor(HTMLElement ancestor, Node descendant){
		if(ancestor==null)return false;
		if(descendant==null)return false;
		if(ancestor==descendant)return false;
		Node parent=descendant.getParentNode();
		while(parent!=null){
			if(parent==ancestor)return true;
			parent=parent.getParentNode();
		}
		return false;
	}

	@Override public void add(HTMLOptionElement option, HTMLElement before){_add(option, before);}
	
	private void _add(HTMLElement element, Node before){
		if(element==null)return;
		
		//1. If element is an ancestor of the select, 
		//then throw a HierarchyRequestError exception and abort these steps.
		if(isAncestor(element, select))
			throw new HierarchyRequestError();

		//2. If before is an element, but that element isn't a descendant of the 
		//select element on which the HTMLOptionsCollection is rooted, then 
		//throw a NotFoundError exception and abort these steps.
		if(before!=null && !isAncestor(select, before))
			throw new NotFoundError();
		
		//3. If element and before are the same element, then return and abort 
		//these steps.
		if(element==before)return;
		
		//4. If before is a node, then let reference be that node. Otherwise, 
		//if before is an integer, and there is a before'th node in the 
		//collection, let reference be that node. Otherwise, let reference be 
		//null.
		Node reference=before;
		
		//5. If reference is not null, let parent be the parent node of 
		//reference. Otherwise, let parent be the select element on which the 
		//HTMLOptionsCollection is rooted.
		Node parent= reference!=null ? reference.getParentNode() : select;
		
		//6. Act as if the DOM insertBefore() method was invoked on the parent 
		//node, with element as the first argument and reference as the second 
		//argument.
		parent.insertBefore(element, reference);
	}

	@Override public void add(HTMLOptionElement option, int before){_add(option, before);}
	
	private void _add(HTMLElement element, int before){
		//4. If before is a node, then let reference be that node. Otherwise, 
		//if before is an integer, and there is a before'th node in the 
		//collection, let reference be that node. Otherwise, let reference be 
		//null.
		NodeList options=getOptions();
		Node reference=options.item(before);
		_add(element, reference);
	}

	@Override public void add(HTMLOptGroupElement group){add(group, null);}
	@Override public void add(HTMLOptGroupElement group, HTMLElement before){_add(group, before);}
	@Override public void add(HTMLOptGroupElement group, int before){_add(group, before);}

	/** Deletes the option at index. */
	@Override public void remove(int index){
		NodeList options=getOptions();
		Node option=options.item(index);//may be null
		if(option==null)return;
		Node parent=option.getParentNode();
		parent.removeChild(option);
	}

	@Override public int getSelectedIndex(){
        for(int i=0 ; i<getLength(); i++){
        	HTMLOptionElement o=item(i);
        	if(o.getSelected())return i;
        }
        return -1;
	}

	@Override public void setSelectedIndex(int index){
		unselect();
		HTMLOptionElement o=item(index);
		if(o!=null)o.setSelected(true);
	}

	// helpers -------------------------------------------------------------
	
	private void unselect(){for(HTMLOptionElement o:this)o.setSelected(false);}
	
	private NodeList getOptions(){return select.getElementsByTagName("option");}
	
	/** only for debug */
	@Override public String toString(){return select.toString();}

}
