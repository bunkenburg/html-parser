/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import org.w3c.dom.Node;

import cat.inspiracio.dom.DOMSettableTokenList;
import cat.inspiracio.dom.DOMSettableTokenListImp;
import cat.inspiracio.dom.HTMLCollection;

abstract class HTMLTableCellElementImp extends HTMLElementImp implements HTMLTableCellElement {
	private static final long serialVersionUID = -8672864259162436800L;

	/** @param tag "td" or "th" */
	protected HTMLTableCellElementImp(HTMLDocumentImp owner, String tag){super(owner, tag);}

	@Override public HTMLTableCellElementImp cloneNode(boolean deep){
		return (HTMLTableCellElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public int getColSpan(){return getAttribute("colspan", 1);}
	@Override public void setColSpan(int span){setAttribute("colspan", span);}

	@Override public int getRowSpan(){return getAttribute("rowspan", 1);}
	@Override public void setRowSpan(int span){setAttribute("rowspan", span);}

	/** string consisting of an unordered set of unique space-separated tokens 
	 * that are case-sensitive, each of which must have the value of an ID */
	@Override public DOMSettableTokenList getHeaders(){
		return new DOMSettableTokenListImp(this, "headers");
	}

	/** The cellIndex IDL attribute must, if the element has a parent tr 
	 * element, return the index of the cell's element in the parent element's 
	 * cells collection. If there is no such parent element, then the attribute must return −1.
	 * */
	@Override public int getCellIndex(){
		Node parent=getParentNode();
		if(parent==null)return -1;
		if(!(parent instanceof HTMLTableRowElement))return -1;
		HTMLTableRowElement row=(HTMLTableRowElement)parent;
		HTMLCollection<HTMLElement>children=row.getChildElements();
		for(int i=0; i<children.getLength(); i++){
			HTMLElement e=children.item(i);
			if(e==this)
				return i;
		}
		return -1;
	}
}
