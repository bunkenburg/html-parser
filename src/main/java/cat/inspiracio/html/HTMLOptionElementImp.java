/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/** Public visibility so that it can be extended by Option. */
public class HTMLOptionElementImp extends HTMLElementImp implements HTMLOptionElement {
	private static final long serialVersionUID = -4950918825566416223L;

	protected HTMLOptionElementImp(HTMLDocumentImp owner){super(owner, "option");}

	@Override public HTMLOptionElementImp cloneNode(boolean deep){
		return (HTMLOptionElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------
	
	@Override public boolean getDisabled(){return getAttributeBoolean("disabled");}
	@Override public void setDisabled(boolean disabled){setAttribute("disabled", disabled);}

	@Override public HTMLFormElement getForm(){return super.getForm();}

	@Override public String getLabel(){return getAttribute("label");}
	@Override public void setLabel(String label){setAttribute("label", label);}

	@Override public boolean getDefaultSelected(){return getAttributeBoolean("selected");}
	@Override public void setDefaultSelected(boolean selected){setAttribute("selected", selected);}
	
	@Override public boolean getSelected(){return getAttributeBoolean("selected");}
	@Override public void setSelected(boolean selected){setAttribute("selected", selected);}

	/** If the element does not have value-attribute, 
	 * returns the text content.*/
	@Override public String getValue(){
		if(hasAttribute("value"))
			return getAttribute( "value" );
		return getText();
	}
	@Override public void setValue(String value){setAttribute("value", value);}
	
	@Override public String getText(){return super.getText();}
    @Override public void setText(String text){setTextContent(text);}
	
    @Override public int getIndex(){
        // Locate the parent SELECT. Note that this OPTION might be inside a
        // OPTGROUP inside the SELECT. Or it might not have a parent SELECT.
        // Everything is possible. If no parent is found, return -1.
        Node parent=getParentNode();
        while(parent!=null && !(parent instanceof HTMLSelectElement))
            parent=parent.getParentNode();
        if(parent!=null){
            // Use getElementsByTagName() which creates a snapshot of all the
            // OPTION elements under the SELECT. Access to the returned NodeList
            // is very fast and the snapshot solves many synchronisation problems.
            NodeList options=((HTMLElement)parent).getElementsByTagName("option");
            for(int i=0 ; i<options.getLength() ; ++i )
                if(options.item(i)==this)
                    return i;
        }
        return -1;
	}

}
