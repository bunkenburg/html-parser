/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

class HTMLScriptElementImp extends HTMLElementImp implements HTMLScriptElement {
	private static final long serialVersionUID = 3251548100366931804L;

	HTMLScriptElementImp(HTMLDocumentImp owner){super(owner, "script");}

	@Override public HTMLScriptElementImp cloneNode(boolean deep){
		return (HTMLScriptElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public String getSrc(){return getAttribute("src");}
	@Override public void setSrc(String src){setAttribute("src", src);}

	@Override public String getType(){
		if(hasAttribute("type"))
			return getAttribute("type");
		return "text/javascript";
	}
	@Override public void setType(String type){setAttribute("type", type);}

	@Override public String getCharset(){return getAttribute("charset");}
	@Override public void setCharset(String charset){setAttribute("charset", charset);}

	@Override public boolean getAsync(){return getAttributeBoolean("async");}
	@Override public void setAsync(boolean b){setAttribute("async", b);}

	@Override public boolean getDefer(){return getAttributeBoolean("defer");}
	@Override public void setDefer(boolean b){setAttribute("defer", b);}

	@Override public String getCrossOrigin(){return getAttribute("crossorigin");}
	@Override public void setCrossOrigin(String cross){setAttribute("crossorigin", cross);}

	@Override public String getText(){return super.getText();}
	@Override public void setText(String text){setTextContent(text);}

}
