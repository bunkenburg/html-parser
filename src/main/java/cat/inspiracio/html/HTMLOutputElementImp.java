/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import cat.inspiracio.dom.DOMSettableTokenList;
import cat.inspiracio.dom.DOMSettableTokenListImp;

class HTMLOutputElementImp extends LabelableElementImp implements HTMLOutputElement {
	private static final long serialVersionUID = -156421940611050534L;

	HTMLOutputElementImp(HTMLDocumentImp owner){super(owner, "output");}

	@Override public HTMLOutputElementImp cloneNode(boolean deep){
		return (HTMLOutputElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	/** reflects "for" attribute */
	@Override public DOMSettableTokenList getHtmlFor(){
		return new DOMSettableTokenListImp(this, "for");
	}
	
    /** First checks explicit form-attribute for a form-id, then looks for an enclosing form. */
	@Override public HTMLFormElement getForm(){
		if(hasAttribute("form")){
			String id=getAttribute("form");
			return (HTMLFormElement) getElementById(id);
		}
		return super.getForm();
	}
	
	@Override public String getType(){return "output";}

	// validation ----------------------------------------------------------
	
	@Override public boolean getWillValidate(){throw new UnsupportedOperationException();}
	@Override public ValidityState getValidity(){throw new UnsupportedOperationException();}
	@Override public String getValidationMessage(){throw new UnsupportedOperationException();}
	@Override public boolean checkValidity(){throw new UnsupportedOperationException();}
	@Override public void setCustomValidity(String error){throw new UnsupportedOperationException();}

}
