/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

/** <a href="http://www.w3.org/TR/html5/forms.html#the-textarea-element">spec</a> */
public interface HTMLTextAreaElement extends LabelableElement, ResettableElement, ReassociateableElement {
	
	String getAutocomplete();
	void setAutocomplete(String s);
	
	boolean getAutofocus();
	void setAutofocus(boolean b);
	
	int getCols();
	void setCols(int cols);
	
	String getDirName();
	void setDirName(String dir);
	
	boolean getDisabled();
	void setDisabled(boolean b);
	
	int getMaxLength();
	void setMaxLength(int max);
	
	int getMinLength();
	void setMinLength(int min);
	
	String getPlaceholder();
	void setPlaceholder(String s);
	
	boolean getReadOnly();
	void setReadOnly(boolean b);
	
	boolean getRequired();
	void setRequired(boolean b);
	
	int getRows();
	void setRows(int rows);
	
	String getWrap();
	void setWrap(String wrap);
	
	String getType();
	
	String getDefaultValue();
	void setDefaultValue(String value);
	
	String getValue();
	void setValue(String value);
	
	int getTextLength();
	
	boolean getWillValidate();
	ValidityState getValidity();
	String getValidationMessage();
	boolean checkValidity();
	void setCustomValidity(String error);
	
	void select();

	int getSelectionStart();
	void setSelectionStart(int start);
	
	int getSelectionEnd();
	void setSelectionEnd(int end);

	void setRangeText(String replacement);
	void setRangeText(String replacement, int start, int end);
	
	void setRangeText(String replacement, int start, int end, SelectionMode mode);

	void setSelectionRange(int start, int end);
	void setSelectionRange(int start, int end, String direction);

}
