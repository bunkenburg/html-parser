/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

/** Not implemented. 
 * 
 * Even this interface is incomplete. 
 * 
 * <a href="http://www.w3.org/TR/html5/embedded-content-0.html#mediacontroller">spec</a> 
 * */
public interface MediaController {

	/** Uses HTMLMediaElement.readyState's values
	 * @return ready state  */
	int getReadyState();

	TimeRanges getBuffered();
	TimeRanges getSeekable();
	double getDuration();
	double getCurrentTime();

	boolean getPaused();
	MediaControllerPlaybackState getPlaybackState();
	TimeRanges getPlayed();
	void pause();
	void unpause();

	/** calls play() on all media elements as well */
	void play();

	double getDefaultPlaybackRate();
	void setDefaultPlaybackRate(double d);
	
	double getPlaybackRate();
	void setPlaybackRate(double d);

	double getVolume();
	void setVolume(double d);
	
	boolean getMuted();
	void setMuted(boolean b);

	//	           attribute EventHandler onemptied;
	//	           attribute EventHandler onloadedmetadata;
	//	           attribute EventHandler onloadeddata;
	//	           attribute EventHandler oncanplay;
	//	           attribute EventHandler oncanplaythrough;
	//	           attribute EventHandler onplaying;
	//	           attribute EventHandler onended;
	//	           attribute EventHandler onwaiting;
	//
	//	           attribute EventHandler ondurationchange;
	//	           attribute EventHandler ontimeupdate;
	//	           attribute EventHandler onplay;
	//	           attribute EventHandler onpause;
	//	           attribute EventHandler onratechange;
	//	           attribute EventHandler onvolumechange;
}
