/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import cat.inspiracio.dom.HTMLCollection;
import cat.inspiracio.dom.HTMLCollectionImp;

/** The select element also is like an array of option elements. */
class HTMLSelectElementImp extends LabelableElementImp implements HTMLSelectElement{
	private static final long serialVersionUID = -124125383328444347L;

	HTMLSelectElementImp(HTMLDocumentImp owner){super(owner, "select");}

	@Override public HTMLSelectElementImp cloneNode(boolean deep){
		return (HTMLSelectElementImp)super.cloneNode(deep);
	}
	
	// interface ScriptArray<HTMLOptionElement> ---------------
	
	@Override public boolean has(int index){
		Object option=getOptions().item(index);
		return option!=null;
	}
	@Override public HTMLOptionElement get(int index){return getOptions().item(index);}
	@Override public void set(int index, HTMLOptionElement option){getOptions().set(index, option);}
	@Override public void delete(int index){getOptions().remove(index);}

	// accessors -----------------------------------------
	
	@Override public boolean getAutofocus(){return getAttributeBoolean("autofocus");}
	@Override public void setAutofocus(boolean auto){setAttribute("autofocus", auto);}
	
	@Override public boolean getDisabled(){return getAttributeBoolean("disabled");}
	@Override public void setDisabled(boolean disabled){setAttribute("disabled", disabled);}
	
    /** First checks explicit form-attribute for a form-id, then looks for an enclosing form. */
	@Override public HTMLFormElement getForm(){
		if(hasAttribute("form")){
			String id=getAttribute("form");
			return (HTMLFormElement) getElementById(id);
		}
		return super.getForm();
	}

	@Override public boolean getMultiple(){return getAttributeBoolean("multiple");}
	@Override public void setMultiple(boolean b){setAttribute("multiple", b);}

	@Override public String getName(){return getAttribute("name");}
	@Override public void setName(String name){setAttribute("name", name);}

	@Override public boolean getRequired(){return getAttributeBoolean("required");}
	@Override public void setRequired(boolean b){setAttribute("required", b);}

	@Override public int getSize(){return getAttributeInt("size");}
	@Override public void setSize(int size){setAttribute("size", size);}

	@Override public String getType(){
		boolean multiple=getAttributeBoolean("multiple");
		return multiple ? "select-multiple" : "select-one";
	}
	
	@Override public HTMLOptionsCollection getOptions(){
		return new HTMLOptionsCollectionImp(this);
	}

	@Override public int getLength(){return getOptions().getLength();}
	@Override public void setLength(int length){getOptions().setLength(length);}
	@Override public HTMLOptionElement item(int index){return getOptions().item(index);}
	@Override public HTMLOptionElement namedItem(String name){return getOptions().namedItem(name);}

	@Override public void add(HTMLOptionElement option){getOptions().add(option);}
	@Override public void add(HTMLOptionElement option, int before){getOptions().add(option, before);}
	@Override public void add(HTMLOptionElement option, HTMLElement before){getOptions().add(option, before);}	
	@Override public void add(HTMLOptGroupElement group){getOptions().add(group);}
	@Override public void add(HTMLOptGroupElement group, int before){getOptions().add(group, before);}
	@Override public void add(HTMLOptGroupElement group, HTMLElement before){getOptions().add(group, before);}

	@Override public void remove(){super.remove();}
	@Override public void remove(int index){getOptions().remove(index);}

	@Override public HTMLCollection<HTMLOptionElement> getSelectedOptions(){
		HTMLCollectionImp<HTMLOptionElement> selected=new HTMLCollectionImp<>();
		for(HTMLOptionElement o : getOptions())
			if(o.getSelected())
				selected.add(o);
		return selected;
	}
	
	@Override public int getSelectedIndex(){return getOptions().getSelectedIndex();}
	@Override public void setSelectedIndex(int index){getOptions().setSelectedIndex(index);}
	
	/** @return the value of the first option that is selected */
	@Override public String getValue(){
		for(HTMLOptionElement option : getOptions())
			if(option.getSelected())
				return option.getValue();
		return "";//default value
	}

	/** Sets the selected attributes of the options. 
	 * Does not check whether there really is an option with this value. 
	 * @param value Does nothing if value is null. */
	@Override public void setValue(String value){
		if(value==null)return;
		HTMLOptionsCollection options=getOptions();
		for(HTMLOptionElement o : options){
			boolean selected=value.equals(o.getValue());
			o.setSelected(selected);
		}
	}
	
	// validation ----------------------------------------------------
	
	@Override public boolean getWillValidate(){throw new UnsupportedOperationException();}
	@Override public ValidityState getValidity(){throw new UnsupportedOperationException();}
	@Override public String getValidationMessage(){throw new UnsupportedOperationException();}
	@Override public boolean checkValidity(){throw new UnsupportedOperationException();}
	@Override public void setCustomValidity(String error){throw new UnsupportedOperationException();}

}
