/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import cat.inspiracio.dom.DOMTokenList;
import cat.inspiracio.dom.DOMTokenListImp;

/** No state in this class. */
class HTMLAnchorElementImp extends URLUtilsImp implements HTMLAnchorElement {
	private static final long serialVersionUID = -6446691590838249254L;

	// construction ----------------------------------------
	
	HTMLAnchorElementImp(HTMLDocumentImp owner){super(owner, "a");}

	@Override public HTMLAnchorElementImp cloneNode(boolean deep){
		return (HTMLAnchorElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------
	
	@Override public String getHreflang(){return getAttribute("hreflang");}
	@Override public void setHreflang(String hreflang){setAttribute("hreflang", hreflang);}

	@Override public String getRel(){return getAttribute("rel");}
	@Override public void setRel(String rel){setAttribute("rel", rel);}

	@Override public String getRev(){return getAttribute("rev");}
	@Override public void setRev(String rev){setAttribute("rev", rev);}

	@Override public String getTarget(){return getAttribute("target");}
	@Override public void setTarget(String target){setAttribute("target", target);}

	@Override public String getType(){return getAttribute("type");}
	@Override public void setType(String type){setAttribute("type", type);}
	
	@Override public String getDownload(){return getAttribute("download");}
	@Override public void setDownload(String d){setAttribute("download", d);}

	@Override public DOMTokenList getRelList(){return new DOMTokenListImp(this, "rel");}

	@Override public String getText(){return getTextContent();}
	@Override public void setText(String text){setTextContent(text);}

}
