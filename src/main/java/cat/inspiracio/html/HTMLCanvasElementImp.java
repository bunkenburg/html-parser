/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

class HTMLCanvasElementImp extends HTMLElementImp implements HTMLCanvasElement {
	private static final long serialVersionUID = 7089308486982465966L;

	HTMLCanvasElementImp(HTMLDocumentImp owner){super(owner, "canvas");}

	@Override public HTMLCanvasElementImp cloneNode(boolean deep){
		return (HTMLCanvasElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public int getWidth(){return getAttribute("width", 0);}
	@Override public void setWidth(int width){setAttribute("width", width);}

	@Override public int getHeight(){return getAttribute("height", 0);}
	@Override public void setHeight(int height){setAttribute("height", height);}

	@Override public RenderingContext getContext(String contextId, Object... arguments){throw new UnsupportedOperationException();}

	@Override public String toDataURL(){throw new UnsupportedOperationException();}
	@Override public String toDataURL(String type, Object... arguments){throw new UnsupportedOperationException();}

	@Override public void toBlob(FileCallback _callback){throw new UnsupportedOperationException();}
	@Override public void toBlob(FileCallback _callback, String type, Object... arguments){throw new UnsupportedOperationException();}

}
