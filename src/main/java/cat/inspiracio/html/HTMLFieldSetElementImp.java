/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

class HTMLFieldSetElementImp extends HTMLElementImp implements HTMLFieldSetElement {
	private static final long serialVersionUID = 2970791014673109465L;

	HTMLFieldSetElementImp(HTMLDocumentImp owner){super(owner, "fieldset");}

	@Override public HTMLFieldSetElementImp cloneNode(boolean deep){
		return (HTMLFieldSetElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public boolean getDisabled(){return getAttributeBoolean("disabled");}
	@Override public void setDisabled(boolean disabled){setAttribute("disabled", disabled);}

    /** First checks explicit form-attribute for a form-id, then looks for an enclosing form. */
	@Override public HTMLFormElement getForm(){
		if(hasAttribute("form")){
			String id=getAttribute("form");
			return (HTMLFormElement) getElementById(id);
		}
		return super.getForm();
	}

	@Override public String getName(){return getAttribute("name");}
	@Override public void setName(String name){setAttribute("name", name);}

	@Override public String getType(){return "fieldset";}

	/** The elements IDL attribute must return an HTMLFormControlsCollection rooted at the fieldset element, whose filter matches listed elements. 
	 * 
	 * Listed elements:
	 * Denotes elements that are listed in the form.elements and fieldset.elements APIs.
	 * button fieldset input keygen object output select textarea
	 * */
	@Override public HTMLFormControlsCollection getElements(){
		return new HTMLFormControlsCollectionImp(this);
	}

	//Validation: not implemented on server ---------------------------------------
	
	@Override public boolean getWillValidate(){throw new UnsupportedOperationException();}
	@Override public ValidityState getValidity(){throw new UnsupportedOperationException();}
	@Override public String getValidationMessage(){throw new UnsupportedOperationException();}
	@Override public boolean checkValidity(){throw new UnsupportedOperationException();}
	@Override public void setCustomValidity(String error){throw new UnsupportedOperationException();}

}
