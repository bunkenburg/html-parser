/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

/** Not implemented.
 * 
 * <a href="http://www.w3.org/TR/html5/embedded-content-0.html#texttrackcue">spec</a>
 * */
public interface TextTrackCue {

	TextTrack getTrack();

	String getId();
	void setId(String id);

	double getStartTime();
	void setStartTime(double e);
	
	double getEndTime();
	void setEndTime(double d);
	
	boolean getPauseOnExit();
	void setPauseOnExit(boolean b);

	EventHandler getOnenter();
	void setOnenter(EventHandler h);

	EventHandler getOnexit();
	void setOnexit(EventHandler h);
}
