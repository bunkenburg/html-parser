/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

class HTMLVideoElementImp extends HTMLMediaElementImp implements HTMLVideoElement {
	private static final long serialVersionUID = -2876383387582211682L;

	HTMLVideoElementImp(HTMLDocumentImp owner){super(owner, "video");}

	@Override public HTMLVideoElementImp cloneNode(boolean deep){
		return (HTMLVideoElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public int getWidth(){return getAttributeInt("width");}
	@Override public void setWidth(int w){setAttribute("width", w);}

	@Override public int getHeight(){return getAttributeInt("height");}
	@Override public void setHeight(int h){setAttribute("height", h);}

	@Override public int getVideoWidth(){throw new UnsupportedOperationException();}
	@Override public int getVideoHeight(){throw new UnsupportedOperationException();}

	@Override public String getPoster(){return getAttribute("poster");}
	@Override public void setPoster(String poster){setAttribute("poster", poster);}

}
