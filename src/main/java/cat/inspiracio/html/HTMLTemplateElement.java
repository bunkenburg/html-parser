/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import org.w3c.dom.DocumentFragment;

/** <a href="http://www.w3.org/TR/html5/scripting-1.html#the-template-element">spec</a> */
public interface HTMLTemplateElement extends HTMLElement {

	/** Not implemented.
	 * 
	 * Returns the contents of the template, which are stored in a 
	 * DocumentFragment associated with a different Document so as 
	 * to avoid the template contents interfering with the main Document. 
	 * (For example, this avoids form controls from being submitted, 
	 * scripts from executing, and so forth.)
	 * 
	 * @return the document fragment */
	DocumentFragment getContent();
}
