/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import org.w3c.dom.stylesheets.LinkStyle;
import org.w3c.dom.stylesheets.StyleSheet;

import cat.inspiracio.dom.DOMSettableTokenList;
import cat.inspiracio.dom.DOMTokenList;

/** <a href="http://www.w3.org/TR/html5/document-metadata.html#the-link-element">spec</a> */
public interface HTMLLinkElement extends HTMLElement, LinkStyle{

	boolean getDisabled();
	void setDisabled(boolean disabled);
	
	String getHref();
	void setHref(String href);

	String getCrossOrigin();
	void setCrossOrigin(String cross);
	
	String getRel();
	void setRel(String rel);

	String getRev();
	void setRev(String rev);
	
	DOMTokenList getRelList();
	
	String getMedia();
	void setMedia(String media);
	
	String getHreflang();
	void setHreflang(String hreflang);
	
	String getType();
	void setType(String type);
	
	DOMSettableTokenList getSizes();
	
	@Override StyleSheet getSheet();
	
}
