/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cat.inspiracio.dom.HTMLCollection;

/** The HTMLFormControlsCollection interface is used for collections of listed elements in form and fieldset elements. */
class HTMLFormControlsCollectionImp implements HTMLFormControlsCollection {
	private static final long serialVersionUID = -2810559074217028999L;
/* It would be nice to let this extends HTMLCollectionImp<ListedElement>, but the return type of namedItem(name) is wrong. */
	
	// state ---------------------------------------------------
	
	protected List<ListedElement>elements=new ArrayList<>();
	
	private Filter filter;
	
	// construction --------------------------------------------
	
	/** The elements IDL attribute must return an HTMLFormControlsCollection rooted at the fieldset element, 
	 * whose filter matches listed elements. */
	HTMLFormControlsCollectionImp(HTMLFieldSetElement f){
		filter=new Filter(){
			@Override public boolean accept(HTMLElement e) {
				return e instanceof ListedElement;
			}};
		HTMLCollection<HTMLElement>children=f.getChildElements();
		for(int i=0; i<children.getLength(); i++){
			HTMLElement child=children.item(i);
			addAll(child);
		}
	}
	
	/** Returns an HTMLCollection of the form controls in the form (excluding image buttons for historical reasons). 
	 * 
	 * The elements IDL attribute must return an HTMLFormControlsCollection 
	 * rooted at the Document node while the form element is in a Document and 
	 * rooted at the form element itself when it is not, 
	 * whose filter matches listed elements whose form owner is the form element, 
	 * with the exception of input elements whose type attribute is in the Image Button state, 
	 * which must, for historical reasons, be excluded from this particular collection. */
	HTMLFormControlsCollectionImp(final HTMLFormElement form){
		filter=new Filter(){
			@Override public boolean accept(HTMLElement e){
				//exclude non-listed elements
				if(!(e instanceof ReassociateableElement))
					return false;
				
				//exclude elements not associated with this form
				//Only ReassociateableElement is associated with a form I think
				ReassociateableElement le=(ReassociateableElement)e;
				HTMLFormElement f=le.getForm();
				if(f!=form)
					return false;
				
				//exclude image buttons
				if(e instanceof HTMLInputElement){
					HTMLInputElement input=(HTMLInputElement)le;
					return !"image".equals(input.getType());
				}
				
				return true;
			}};
		HTMLDocument d=form.getOwnerDocument();
		if(d==null){
			addAll(form);
		}
		else{
			HTMLElement e=d.getDocumentElement();
			addAll(e);
		}
	}
	

	/** From this element and all its descendants, adds the listed elements. */
	private void addAll(HTMLElement element){
		if(filter.accept(element))
			add((ListedElement)element);
		HTMLCollection<HTMLElement>children=element.getChildElements();
		for(HTMLElement child : children)
			addAll(child);
	}

	public void add(ListedElement e){elements.add(e);}

	// accessors -----------------------------------------------

	@Override public int getLength(){return elements.size();}

	@Override public ListedElement item(int index){
		if(index<0 || elements.size()<=index)
			return null;
		return elements.get(index);
	}

	/** Returns null, an element, or a radio node list. */
	@Override public Object namedItem(String name){
		if(name==null || 0==name.length())return null;
		
		RadioNodeListImp radios=new RadioNodeListImp();
		for(ListedElement e : elements){
			boolean in= name.equals(e.getId()) || name.equals(e.getName());
			if(in)
				radios.add(e);
		}
		
		int length=radios.getLength();
		if(length==0)return null;
		if(length==1)return radios.item(0);
		return radios;
	}

	// helpers ----------------------------------------
	
	@Override public String toString(){return elements.toString();}
	@Override public Iterator<ListedElement> iterator(){return elements.iterator();}
	
	private static interface Filter{
		boolean accept(HTMLElement e);
	}

}
