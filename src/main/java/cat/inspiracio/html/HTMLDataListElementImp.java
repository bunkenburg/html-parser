/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import org.w3c.dom.NodeList;

import cat.inspiracio.dom.HTMLCollection;
import cat.inspiracio.dom.HTMLCollectionImp;

class HTMLDataListElementImp extends HTMLElementImp implements HTMLDataListElement {
	private static final long serialVersionUID = -6533712939619615730L;

	HTMLDataListElementImp(HTMLDocumentImp owner){super(owner, "datalist");}

	/** Return right type. */
	@Override public HTMLDataListElementImp cloneNode(boolean deep){
		return (HTMLDataListElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public HTMLCollection<HTMLOptionElement> getOptions(){
		NodeList list=getElementsByTagName("option");
		HTMLCollectionImp<HTMLOptionElement>options=new HTMLCollectionImp<>();
		for(int i=0; i<list.getLength(); i++){
			HTMLOptionElement o=(HTMLOptionElement)list.item(i);
			options.add(o);
		}
		return options;
	}

}
