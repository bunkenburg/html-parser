/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

/** Draft. Implementation or package may change. 
 * 
 * <a href="http://www.w3.org/TR/html-imports/">spec</a> 
 * */
public interface LinkImport {

	/** Not implemented.
	 * 
	 * http://www.w3.org/TR/html-imports/
	 * 
	 * http://www.html5rocks.com/en/tutorials/webcomponents/imports/ 
	 * 
	 * function supportsImports() {
	 *   return 'import' in document.createElement('link');
	 *   }
	 *   
	 * @return the document 
	 * */
	HTMLDocument getImport();

}
