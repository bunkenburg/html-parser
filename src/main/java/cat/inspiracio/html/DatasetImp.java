/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import java.io.Serializable;

import org.w3c.dom.Element;

import cat.inspiracio.dom.DOMStringMap;
import cat.inspiracio.dom.SyntaxError;

/** 
 * A DOMStringMap object for an element's data-* attributes. 
 * Hyphenated names become camel-cased. For example, data-foo-bar="" becomes element.dataset.fooBar.
 * 
 * <a href="http://www.w3.org/TR/html5/dom.html#embedding-custom-non-visible-data-with-the-data-*-attributes">Spec</a>
 * 
 * */
class DatasetImp implements DOMStringMap, Serializable{
	private static final long serialVersionUID = -372701818121063698L;

	// state ----------------------------------
	
	private Element element;
	
	// construction ---------------------------
	
	DatasetImp(Element e){element=e;}
	
	// business methods -----------------------

	@Override public boolean has(String name){
		String key=toKey(name);
		return element.hasAttribute(key);
	}
	
	@Override public String get(String name){
		String key=toKey(name);
		return element.getAttribute(key);
	}

    @Override public void set(String name, String value){
        String key=toKey(name);
        element.setAttribute(key, value);
    }
    
    @Override public void set(String name, int i){
        String key=toKey(name);
        String value=Integer.toString(i);
        element.setAttribute(key, value);
    }
    
	/** @param name like "fooBar" */
	@Override public void deleter(String name){
		String key=toKey(name);
		element.removeAttribute(key);
	}

	// helpers ----------------------------------------------------
	
	@SuppressWarnings("unused")
	private boolean containsUppercase(String key){
		for(int i=0; i<key.length(); i++){
			char c=key.charAt(i);
			if(Character.isUpperCase(c))return true;
		}
		return false;
	}

	/** Transforms "fooBar" to "data-foo-bar"
	 * and "elementCount" to "data-element-count".
	 * @param object Really works only for non-null String. 
	 * */
	private String toKey(Object object){
		String name=object.toString();
		if(name.contains("-"))throw new SyntaxError(name);
		
		StringBuilder builder=new StringBuilder("data-");
		for(int i=0; i<name.length(); i++){
			char c=name.charAt(i);
			if(Character.isUpperCase(c)){
				builder.append('-');
				char lower=Character.toLowerCase(c);
				builder.append(lower);
			}
			else builder.append(c);
		}
		return builder.toString();
	}

	/** Is this a valid key like "data-foo-bar"? */
	private boolean isKey(String key){
		return key.startsWith("data-");
	}
	
	/** If this key corresponds to a dataset name, return the name.
	 * Otherwise return null. 
	 * Transforms "data-foo-bar" to "fooBar".
	 * */
	@SuppressWarnings("unused")
	private String toName(String key){
		if(!isKey(key))return null;
		//if(containsUppercase(key))return null; Accept upper-case even though it is wrong.

		StringBuilder builder=new StringBuilder(key);
		int dash=key.indexOf('-');
		while(-1<dash){
			//If the next character is lowercase, make it uppercase and remove the dash.
			int length=builder.length();
			if(dash+1<length){
				char next=builder.charAt(dash+1);
				if(Character.isLowerCase(next)){
					String replace=Character.toUpperCase(next) + "";
					builder.replace(dash, dash+2, replace);
				}
			}
			dash=key.indexOf('-');
		}
		return builder.toString();
	}

}