/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

class HTMLModElementImp extends HTMLElementImp implements HTMLModElement {
	private static final long serialVersionUID = 3105939094375536029L;

	/** @param tag "ins" or "del" */
	HTMLModElementImp(HTMLDocumentImp owner, String tag){super(owner, tag);}

	@Override public HTMLModElementImp cloneNode(boolean deep){
		return (HTMLModElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public String getCite(){return getAttribute("cite");}
	@Override public void setCite(String cite){setAttribute("cite", cite);}

	@Override public String getDateTime(){return getAttribute("dateTime");}
	@Override public void setDateTime(String dateTime){setAttribute("dateTime", dateTime);}

}
