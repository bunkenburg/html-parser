/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

/** Java bean */
class AudioTrackImp extends TrackImp implements AudioTrack {

    private boolean enabled=false;
    
    AudioTrackImp(){}

    /** Returns true if the given track is active, and false otherwise.*/
    @Override public boolean getEnabled(){return enabled;}

    /** Can be set, to change whether the track is enabled or not. If multiple audio tracks are enabled simultaneously, they are mixed.*/
    @Override public void setEnabled(boolean b){this.enabled=b;}

}
