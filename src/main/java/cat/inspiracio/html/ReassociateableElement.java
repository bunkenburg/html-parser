/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

/** Denotes elements that have a form content attribute, 
 * and a matching form IDL attribute, that allow authors to specify an explicit form owner.
 * 
 * button fieldset input keygen label object output select textarea 
 * */
interface ReassociateableElement extends FormAssociatedElement {

	HTMLFormElement getForm();
}
