/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import java.util.Date;

import cat.inspiracio.dom.IndexSizeError;

abstract class HTMLMediaElementImp extends HTMLElementImp implements HTMLMediaElement {
	private static final long serialVersionUID = 7094382263286124769L;

	/** @param tag "audio" or "video" */
	HTMLMediaElementImp(HTMLDocumentImp owner, String tag){super(owner, tag);}

	@Override public HTMLMediaElementImp cloneNode(boolean deep){
		return (HTMLMediaElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public MediaError getError(){throw new UnsupportedOperationException();}
	
	@Override public String getSrc(){return getAttribute("src");}
	@Override public void setSrc(String src){setAttribute("src", src);}

	@Override public String getCurrentSrc(){throw new UnsupportedOperationException();}
	
	@Override public String getCrossOrigin(){return getAttribute("crossorigin");}
	@Override public void setCrossOrigin(String cross){setAttribute("crossorigin", cross);}

	@Override public int getNetworkState(){throw new UnsupportedOperationException();}
	
	@Override public String getPreload(){return getAttribute("preload");}
	@Override public void setPreload(String preload){setAttribute("preload", preload);}

	@Override public TimeRanges getBuffered(){throw new UnsupportedOperationException();}
	
	@Override public void load(){throw new UnsupportedOperationException();}
	@Override public CanPlayTypeEnum getCanPlayType(String type){throw new UnsupportedOperationException();}
	@Override public int getReadyState(){throw new UnsupportedOperationException();}
	@Override public boolean getSeeking(){throw new UnsupportedOperationException();}
	
	@Override public double getCurrentTime(){throw new UnsupportedOperationException();}
	@Override public void setCurrentTime(double time){throw new UnsupportedOperationException();}

	@Override public double getDuration(){throw new UnsupportedOperationException();}
	
	@Override public Date getStartDate(){throw new UnsupportedOperationException();}

	@Override public boolean getPaused(){throw new UnsupportedOperationException();}

	@Override public double getDefaultPlaybackRate(){
		if(this.hasAttribute("defaultplaybackrate"))
			return getAttributeDouble("defaultplaybackrate");
		return 1.0;
	}
	@Override public void setDefaultPlaybackRate(double d){setAttribute("defaultplaybackrate", d);}

	@Override public double getPlaybackRate(){
		if(this.hasAttribute("playbackrate"))
			return getAttributeDouble("playbackrate");
		return 1.0;
	}
	@Override public void setPlaybackRate(double d){setAttribute("playbackrate", d);}

	@Override public TimeRanges getPlayed(){throw new UnsupportedOperationException();}
	@Override public TimeRanges getSeekable(){throw new UnsupportedOperationException();}

	@Override public boolean getEnded(){throw new UnsupportedOperationException();}

	@Override public boolean getAutoplay(){return getAttributeBoolean("autoplay");}
	@Override public void setAutoplay(boolean b){setAttribute("autoplay", b);}

	@Override public boolean getLoop(){return getAttributeBoolean("loop");}
	@Override public void setLoop(boolean b){setAttribute("loop", b);}

	@Override public void play(){throw new UnsupportedOperationException();}
	@Override public void pause(){throw new UnsupportedOperationException();}

	@Override public String getMediaGroup(){return getAttribute("mediagroup");}
	@Override public void setMediaGroup(String g){setAttribute("mediagroup", g);}

	@Override public MediaController getController(){throw new UnsupportedOperationException();}
	@Override public void setController(MediaController c){throw new UnsupportedOperationException();}

	@Override public boolean getControls(){return getAttributeBoolean("controls");}
	@Override public void setControls(boolean b){setAttribute("controls", b);}

	@Override public double getVolume(){
		if(hasAttribute("volume"))
			return getAttributeDouble("volume");
		return 1.0;
	}
	@Override public void setVolume(double d){
		if(0.0<=d && d<=1.0)
			setAttribute("volume", d);
		throw new IndexSizeError();
	}

	@Override public boolean getMuted(){return getAttributeBoolean("muted");}
	@Override public void setMuted(boolean b){setAttribute("muted", b);}

	@Override public boolean getDefaultMuted(){return getAttributeBoolean("muted");}
	@Override public void setDefaultMuted(boolean b){setAttribute("muted", b);}

	@Override public AudioTrackList getAudioTracks(){throw new UnsupportedOperationException();}
	@Override public VideoTrackList getVideoTracks(){throw new UnsupportedOperationException();}

	@Override public TextTrackList getTextTracks(){throw new UnsupportedOperationException();}
	@Override public TextTrack addTextTrack(TextTrackKind kind){throw new UnsupportedOperationException();}
	@Override public TextTrack addTextTrack(TextTrackKind kind, String label){throw new UnsupportedOperationException();}
	@Override public TextTrack addTextTrack(TextTrackKind kind, String label, String language){throw new UnsupportedOperationException();}

}
