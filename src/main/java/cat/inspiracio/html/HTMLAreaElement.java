/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import cat.inspiracio.dom.DOMTokenList;
import cat.inspiracio.url.URLUtils;

/** 
 * <a href="http://www.w3.org/TR/html5/embedded-content-0.html#the-area-element">spec</a>
 * 
 *  The methods of URLUtils are not implemented. */
public interface HTMLAreaElement extends HTMLElement, URLUtils{

	String getAlt();
	void setAlt(String s);
	
	String getCoords();
	void setCoords(String s);
	
	String getShape();
	void setShape(String shape);
	
	String getTarget();
	void setTarget(String s);
	
	String getDownload();
	void setDownload(String s);
	
	String getRel();
	void setRel(String rel);
	
	DOMTokenList getRelList();
	
	String getHrefLang();
	void setHrefLang(String language);
	
	String getType();
	void setType(String type);
}
