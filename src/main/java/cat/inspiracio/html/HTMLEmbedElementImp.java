/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

class HTMLEmbedElementImp extends HTMLElementImp implements HTMLEmbedElement {
	private static final long serialVersionUID = -6439190614656907271L;

	HTMLEmbedElementImp(HTMLDocumentImp owner){super(owner, "embed");}

	@Override public HTMLEmbedElementImp cloneNode(boolean deep){
		return (HTMLEmbedElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public String getSrc(){return getAttribute("src");}
	@Override public void setSrc(String src){setAttribute("src", src);}

	@Override public String getType(){return getAttribute("type");}
	@Override public void setType(String type){setAttribute("type", type);}

	@Override public String getWidth(){return getAttribute("width");}
	@Override public void setWidth(String width){setAttribute("width", width);}

	@Override public String getHeight(){return getAttribute("height");}
	@Override public void setHeight(String height){setAttribute("height", height);}

}
