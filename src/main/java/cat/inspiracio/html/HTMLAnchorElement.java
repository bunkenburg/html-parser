/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import cat.inspiracio.dom.DOMTokenList;
import cat.inspiracio.url.URLUtils;

/** 
 * <a href="http://www.w3.org/TR/html5/text-level-semantics.html#the-a-element">spec</a>
 * 
 * The methods of URLUtils are not implemented.
 * */
public interface HTMLAnchorElement extends HTMLElement, URLUtils {

	String getTarget();
	void setTarget(String target);
	
	String getDownload();
	void setDownload(String d);
	
	String getRel();
	void setRel(String rel);
	
	String getRev();
	void setRev(String rev);
	
	DOMTokenList getRelList();
	
	String getHreflang();
	void setHreflang(String lang);
	
	String getType();
	void setType(String type);
	
	String getText();
	void setText(String text);
}
