/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import org.w3c.dom.stylesheets.StyleSheet;

import stylesheets.StyleSheetImp;

class HTMLStyleElementImp extends HTMLElementImp implements HTMLStyleElement {
	private static final long serialVersionUID = 708976532836797720L;

	HTMLStyleElementImp(HTMLDocumentImp owner){super(owner, "style");}

	@Override public HTMLStyleElementImp cloneNode(boolean deep){
		return (HTMLStyleElementImp)super.cloneNode(deep);
	}
	
	// methods ----------------------------------------------

	@Override public boolean getDisabled(){return getAttributeBoolean("disabled");}
	@Override public void setDisabled(boolean disabled){setAttribute("disabled", disabled);}

	@Override public String getMedia(){return getAttribute("media");}
	@Override public void setMedia(String media){setAttribute("media", media);}

	@Override public String getType(){return getAttribute("type");}
	@Override public void setType(String type){setAttribute("type", type);}

    // interface LinkStyle -------------------------------------------------------
    
    @Override public StyleSheet getSheet(){
        //node must be in a document.
        if(!isInDocument())
            return null;
        
        String title=getTitle();
        
        //type=text/css ---that's what we support
        String type=getType();
        if(type!=null && 0<type.length()){
            if(!"text/css".equals(type))
                return null;
        }
        else
            type="text/css";

        String media=getMedia();
        
        //All good: return a StyleSheet object.
        StyleSheetImp sheet=new StyleSheetImp();
        sheet.setDisabled(false);
        sheet.setHref(null);
        sheet.setMedia(media);
        sheet.setOwnerNode(this);
        sheet.setParentStyleSheet(null);
        sheet.setTitle(title);
        sheet.setType(type);
        
        return sheet;
    }

}
