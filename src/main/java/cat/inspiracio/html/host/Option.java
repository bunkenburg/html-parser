/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html.host;

import cat.inspiracio.html.HTMLOptionElementImp;

/** Host object for "new Option()" in javascript.
 * 
 * [NamedConstructor=Option(optional DOMString text = "", optional DOMString value, optional boolean defaultSelected = false, optional boolean selected = false)]
 * */
public class Option extends HTMLOptionElementImp{
	private static final long serialVersionUID = 6571291736844418352L;

	/** Makes a fresh option element.
	 * Its owner document is a fresh document. */
	public Option(){
		super(new Document());
	}
	
	/** Makes a fresh option element.
	 * Its owner document is a fresh document. 
	 * 
	 * @param text The text argument sets the contents of the element.
	 * */
	public Option(String text){
		this();
		setText(text);
	}
	
	/** Makes a fresh option element.
	 * Its owner document is a fresh document.
	 * 
	 * @param text The text argument sets the contents of the element.
	 * @param value The value argument sets the value attribute.
	 * */
	public Option(String text, String value){
		this(text);
		setValue(value);
	}
	
	/** Makes a fresh option element.
	 * Its owner document is a fresh document. 
	 * 
	 * @param text The text argument sets the contents of the element.
	 * @param value The value argument sets the value attribute.
	 * @param defaultSelected The defaultSelected argument sets the selected attribute.
	 * */
	public Option(String text, String value, boolean defaultSelected){
		this(text, value);
		setSelected(defaultSelected);
	}
	
	/** Makes a fresh option element.
	 * Its owner document is a fresh document.
	 * 
	 * @param text The text argument sets the contents of the element.
	 * @param value The value argument sets the value attribute.
	 * @param defaultSelected The defaultSelected argument sets the selected attribute.
	 * @param selected The selected argument sets whether or not the element is selected. 
	 * 	If it is omitted, even if the defaultSelected argument is true, 
	 * 	the element is not selected. --Ignored on server.
	 * */
	public Option(String text, String value, boolean defaultSelected, String selected){
		this(text, value, defaultSelected);
		//Parameter selected is ignored.
	}
	
	@Override public Option cloneNode(boolean deep){
		return (Option)super.cloneNode(deep);
	}
	
}
