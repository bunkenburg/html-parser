/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html.host;

import cat.inspiracio.html.HTMLImageElementImp;

/** This class exists so that we can call "new Image()" from javascript.
 * 
 * <a href="http://www.w3.org/TR/html5/embedded-content-0.html#the-img-element">spec</a>
 *
 * [NamedConstructor=Image(optional unsigned long width, optional unsigned long height)]
 */
public class Image extends HTMLImageElementImp {
	private static final long serialVersionUID = 2852721647912932646L;

	// construction ------------------------------------------------------
	
	/** Makes a new Image.
	 * Its owner document is a new document.
	 * 
	 * image = new Image()
	 * Returns a new img element.
	 * */
	public Image(){
		super(new Document());
	}
	
	/** Makes a new Image.
	 * Its owner document is a new document.
	 * 
	 * image = new Image(width)
	 * Returns a new img element, with the width and height attributes set to the 
	 * values passed in the relevant arguments, if applicable.
	 * 
	 * @param width the image width 
	 * */
	public Image(int width){
		this();
		setWidth(width);
	}

	/** Makes a new Image.
	 * Its owner document is a new document.
	 * 
	 * image = new Image(width, height)
	 * Returns a new img element, with the width and height attributes set to the 
	 * values passed in the relevant arguments, if applicable.
	 * 
	 * @param width the image width
	 * @param height the image height
	 * */
	public Image(int width, int height){
		this(width);
		setHeight(height);
	}

	@Override public Image cloneNode(boolean deep){
		return (Image)super.cloneNode(deep);
	}

}
