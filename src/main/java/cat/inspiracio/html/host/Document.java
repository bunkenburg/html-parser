/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html.host;

import cat.inspiracio.html.HTMLBodyElement;
import cat.inspiracio.html.HTMLBuilder;
import cat.inspiracio.html.HTMLDOMImplementation;
import cat.inspiracio.html.HTMLDocumentBuilder;
import cat.inspiracio.html.HTMLDocumentImp;
import cat.inspiracio.html.HTMLHeadElement;
import cat.inspiracio.html.HTMLHtmlElement;

/** Exists so that we can call "new Document()" in javascript. */
public class Document extends HTMLDocumentImp{
	private static final long serialVersionUID = 7275594217844015985L;

	/** Starts off with some elements: html, head, body. */
	public Document(){
		super(fabricateImplementation());
		HTMLHtmlElement html=createElement(HTMLHtmlElement.class);
		appendChild(html);
		HTMLHeadElement head=createElement(HTMLHeadElement.class);
		html.appendChild(head);
		HTMLBodyElement body=createElement(HTMLBodyElement.class);
		html.appendChild(body);
	}
	
	/** The zero-argument constructor needs a DOM implementation ---
	 * where should it come from? Need to think about the design more.
	 * 
	 * Here, just fabricates a fresh implementation (no custom elements). 
	 * 
	 * Maybe it could be instantiated and configured outside somehow
	 * and this method brings it in. Maybe a thread local variable?
	 * */
	private static HTMLDOMImplementation fabricateImplementation(){
		HTMLBuilder builder=new HTMLDocumentBuilder();
		return (HTMLDOMImplementation)builder.getDOMImplementation();
	}

	/** Makes a new Document without any elements. */
	private Document(HTMLDOMImplementation i){
		super(i);
	}
	
	@Override public Document cloneNode(boolean deep){
		//make an empty document
		Document clone=new Document(getImplementation());
//		HTMLElement html=clone.getHtml();
//		if(html!=null)
//			clone.removeChild(html);

		//copy the elements over
		clone.copy(this, deep);
		return clone;
	}

}
