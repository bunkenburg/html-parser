/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html.host;

import cat.inspiracio.html.HTMLAudioElementImp;

/** 
 * A constructor for an audio element, so that "new Audio()" can be called
 * in javascript. The new element will have a fresh owner document.
 * 
 * <a href="http://www.w3.org/TR/html5/embedded-content-0.html#dom-audio">Spec</a>
 * 
 * A constructor is provided for creating HTMLAudioElement objects 
 * (in addition to the factory methods from DOM such as createElement()):
 * Audio(src). 
 * 
 * When invoked as a constructor, it must return a new HTMLAudioElement 
 * object (a new audio element). The element must have its preload 
 * attribute set to the literal value "auto". If the src argument 
 * is present, the object created must have its src content attribute 
 * set to the provided value, and the user agent must invoke the object's 
 * resource selection algorithm before returning. The element's document 
 * must be the active document of the browsing context of the Window 
 * object on which the interface object of the invoked constructor is 
 * found.
 * */
public class Audio extends HTMLAudioElementImp{
	private static final long serialVersionUID = 5372319533703672002L;

	// construction ------------------------------------------

	/**
	 * When invoked as a constructor, it must return a new HTMLAudioElement 
	 * object (a new audio element). The element must have its preload 
	 * attribute set to the literal value "auto". 
	 * 
	 * The element's document is a fresh document.
	 * */
	public Audio(){
		super(new Document());
		setPreload("auto");
	}

	/**
	 * When invoked as a constructor, it must return a new HTMLAudioElement 
	 * object (a new audio element). The element must have its preload 
	 * attribute set to the literal value "auto". 
	 * 
	 * If the src argument 
	 * is present, the object created must have its src content attribute 
	 * set to the provided value, and the user agent must invoke the object's 
	 * resource selection algorithm before returning.
	 * 
	 * The element's document is a fresh document.
	 * 
	 * @param src the audio src
	 * */
	public Audio(String src){
		super(new Document());
		setPreload("auto");
		setSrc(src);
	}
	
	@Override public Audio cloneNode(boolean deep){
		return (Audio)super.cloneNode(deep);
	}
}
