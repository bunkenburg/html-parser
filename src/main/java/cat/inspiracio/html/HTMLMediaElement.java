/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.html;

import java.util.Date;

/** <a href="http://www.w3.org/TR/html5/embedded-content-0.html#htmlmediaelement">spec</a> */
public interface HTMLMediaElement extends HTMLElement {
	
	// error state -------------------------------------------------

	MediaError getError();
	
	// network state  ---------------------------------------------
	
	String getSrc();
	void setSrc(String src);
	
	String getCurrentSrc();
	
	String getCrossOrigin();
	void setCrossOrigin(String cross);

	static final int NETWORK_EMPTY=0;
	static final int NETWORK_IDLE=1;
	static final int NETWORK_LOADING=2;
	static final int NETWORK_NO_SOURCE=3;
	
	int getNetworkState();
	
	String getPreload();
	void setPreload(String preload);
	
	TimeRanges getBuffered();
	
	void load();
	
	CanPlayTypeEnum getCanPlayType(String type);
	
	// ready state --------------------------------------------------
	
	static final int HAVE_NOTHING=0;
	static final int HAVE_METADATA=1;
	static final int HAVE_CURRENT_DATA=2;
	static final int HAVE_FUTURE_DATA=3;
	static final int HAVE_ENOUGH_DATA=4;

	int getReadyState();
	
	boolean getSeeking();
	
	double getCurrentTime();
	void setCurrentTime(double time);
	
	double getDuration();
	
	Date getStartDate();
	
	boolean getPaused();
	
	double getDefaultPlaybackRate();
	void setDefaultPlaybackRate(double d);
	
	double getPlaybackRate();
	void setPlaybackRate(double d);
	
	TimeRanges getPlayed();
	
	TimeRanges getSeekable();
	
	boolean getEnded();
	
	boolean getAutoplay();
	void setAutoplay(boolean b);
	
	boolean getLoop();
	void setLoop(boolean b);
	
	void play();
	
	void pause();
	
	// media controller ---------------------------------------------------------------
	
	String getMediaGroup();
	void setMediaGroup(String g);
	
	MediaController getController();
	void setController(MediaController c);
	
	// controls -----------------------------------------------------------------------
	
	boolean getControls();
	void setControls(boolean b);
	
	double getVolume();
	void setVolume(double d);
	
	boolean getMuted();
	void setMuted(boolean b);
	
	boolean getDefaultMuted();
	void setDefaultMuted(boolean b);
	
	// tracks --------------------------------------------------------------------------
	
	AudioTrackList getAudioTracks();
	
	VideoTrackList getVideoTracks();
	
	TextTrackList getTextTracks();
	
	TextTrack addTextTrack(TextTrackKind kind);
	TextTrack addTextTrack(TextTrackKind kind, String label);
	TextTrack addTextTrack(TextTrackKind kind, String label, String language);
}
