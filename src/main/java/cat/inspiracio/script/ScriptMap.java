/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.script;

/** A class implements this interface
 * to signal that it is suitable to 
 * javascript-map notation and the _delete_ operator:
 * 
 * 		o[key] = value;
 * 		value = o[key];
 *      
 *      o.foo = "bar"
 *      delete o.for
 * 
 * Whether map notation really works
 * depends on the script engine and will
 * need some additional adaptation. This
 * interface only signals that the class
 * wants to have map notation.
 * */
public interface ScriptMap<T> {

	/**  
	 * @param key Name of the property
	 * @return Is there an element at this key?
	 * */
	boolean has(String key);
	
	/** If there is an element at this key, return it.
	 * Otherwise return null.
	 * @param key Name of the property
	 * @return T or null */
	T get(String key);
	
	/** Set the element at this key. 
	 * May substitute the element that was 
	 * previously at this index.
	 * 
	 * (The method could have been called "put".) 
	 * @param key Name of the property
	 * @param value Value of the property
	 * */
	void set(String key, T value);
    void set(String name, int value);
	
	/** Deletes the element at this key. 
	 * 
	 * (Not called "delete" because that is reserved word in JS.)
	 * @param key Name of the property 
	 * */
	void deleter(String key);
	
}
