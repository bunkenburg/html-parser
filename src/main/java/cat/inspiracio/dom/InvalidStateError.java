/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.dom;

import java.io.Serializable;

import org.w3c.dom.DOMException;

/** <a href="http://www.w3.org/TR/dom/#invalidstateerror">spec</a> */
public class InvalidStateError extends DOMException implements Serializable{
	private static final long serialVersionUID = -6454339199099312718L;

	public InvalidStateError(){
		super(INVALID_STATE_ERR, "InvalidStateError");
	}

	public InvalidStateError(String msg){
		super(INVALID_STATE_ERR, msg);
	}

}
