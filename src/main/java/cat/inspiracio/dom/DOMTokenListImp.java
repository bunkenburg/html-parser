/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.dom;

import java.util.ArrayList;
import java.util.List;

import cat.inspiracio.html.HTMLElement;

public class DOMTokenListImp implements DOMTokenList {
	private static final long serialVersionUID = -6284575687653982416L;
	
	// state ----------------------------------------------
	
	private List<String>tokens=new ArrayList<String>();
	private HTMLElement element;
	private String key;
	
	// construction ---------------------------------------
	
	/** Makes a token list for this element, for this attribute. 
	 * @param element the element
	 * @param key the key */
	public DOMTokenListImp(HTMLElement element, String key){
		this.element=element;
		this.key=key;
		String value=element.getAttribute(key);
		setValue(value);
	}

	protected void setValue(String value){
		tokens.clear();
		if(0<value.length()){
			String[]ts=value.split("\\s+");
			for(int i=0; i<ts.length; i++)
				tokens.add(ts[i]);
		}
	}
	
	// accessors ----------------------------------------
	
	@Override public int getLength(){return tokens.size();}
	@Override public String item(int index){return tokens.get(index);}
	@Override public boolean contains(String token){check(token);return tokens.contains(token);}

	@Override public void add(String... tokens){
		for(String token : tokens)check(token);
		for(String token : tokens){
			if(!this.tokens.contains(token))
				this.tokens.add(token);
		}
		update();
	}

	@Override public void remove(String... tokens){
		for(String token : tokens)check(token);
		for(String token : tokens)
			this.tokens.remove(token);
		update();
	}

	@Override public boolean toggle(String token){
		if(contains(token)){
			remove(token);
			return false;
		}
		add(token);
		return true;
	}

	@Override public boolean toggle(String token, boolean force){
		if(force)
			add(token);
		else
			remove(token);
		return force;
	}
	
	protected String getValue(){
		StringBuilder builder=new StringBuilder();
		boolean first=true;
		for(String token : tokens){
			if(!first)
				builder.append(' ');
			first=false;
			builder.append(token);
		}
		return builder.toString();
	}

	/** Writes back to the element's attribute. */
	private void update(){
		if(element==null || key==null)
			return;
		String value=getValue();
		element.setAttribute(key, value);
	}
	
	@Override public String toString(){return getValue();}
	
	// helpers ----------------------------------------------------------
	
	/** Does the string contain white space? */
	private boolean containsWhite(String s){return s.matches(".*\\s+.*");}
	
	private void check(String token){
		if(token==null || 0==token.length())
			throw new SyntaxError();
		if(containsWhite(token))
			throw new InvalidCharacterError();
	}
}
