/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.dom;

import org.w3c.dom.DOMException;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;

/** Initial implementation of DOMImplementation. */
public abstract class InitialDOMImplementation implements DOMImplementation {

	protected InitialDOMImplementation(){}

	@Override public Document createDocument(String namespaceURI, String qualifiedName, DocumentType doctype)throws DOMException{
		throw new UnsupportedOperationException();
	}

	@Override public DocumentType createDocumentType(String qualifiedName, String publicId, String systemId)throws DOMException{
		throw new UnsupportedOperationException();
	}

	/** @return null */
	@Override public Object getFeature(String feature, String version){
		return null;
	}

	/** @return false */
	@Override public boolean hasFeature(String feature, String version){
		return false;
	}

}
