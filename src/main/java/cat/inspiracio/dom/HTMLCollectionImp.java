/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.dom;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cat.inspiracio.html.HTMLElement;

/** Simple implementation of HTMLCollection, backed by a list. */
public class HTMLCollectionImp<T extends HTMLElement> implements HTMLCollection<T> {
	private static final long serialVersionUID = -7738853951098271927L;

	/** Should the element be added?
	 * If the filter accepts the element,
	 * it must be possible to cast it to T. */
	public static interface Filter {
		boolean accept(HTMLElement e);
	}


	// state -------------------------------------------

	protected List<T>elements=new ArrayList<>();
	
	// construction -----------------------------------
	
	public HTMLCollectionImp(){}
	
	/** Fills the collection with all descendant elements of the element
	 * that satisfy the filter. 
	 * @param element the element
	 * @param f the filter
	 * */
	public HTMLCollectionImp(HTMLElement element, Filter f){
		if(element==null)return;
		HTMLCollection<HTMLElement>children=element.getChildElements();
		for(int i=0; i<children.getLength(); i++){
			HTMLElement e=children.item(i);
			addAll(f, e);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void addAll(Filter f, HTMLElement element){
		if(f.accept(element))
			add((T) element);
		HTMLCollection<HTMLElement>children=element.getChildElements();
		for(int i=0; i<children.getLength(); i++){
			HTMLElement e=children.item(i);
			addAll(f, e);
		}
	}

	public void add(T e){elements.add(e);}
	
	// HTMLCollection ---------------------------------
	
	@Override public int getLength(){return elements.size();}

	@Override public T item(int index){
		if(index<0 || elements.size()<=index)
			return null;
		return elements.get(index);
	}

	@Override public T namedItem(String name){
		if(name==null || 0==name.length())return null;
		for(T e : elements){
			if(name.equals(e.getName()))
				return e;
		}
		return null;
	}
	
	// helpers ----------------------------------------
	
	@Override public String toString(){return elements.toString();}

	@Override public Iterator<T> iterator(){return elements.iterator();}

}
