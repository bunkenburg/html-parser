/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.dom;

/** A class implements this interface
 * to signal that it is suitable to 
 * javascript-array notation:
 * 
 * 	array[index] = value;
 * 	value = array[index];
 * 
 * Whether array-notation really works
 * depends on the script engine and will
 * need some additional adaptation. This
 * interface only signals that the class
 * wants to have array notation.
 * */
public interface ScriptArray<T> {

	/**  
	 * @param index the index 
	 * @return Is there an element at this index? */
	boolean has(int index);
	
	/** If there is an element at this index, return it.
	 * Otherwise return null. 
	 * @param index the index 
	 * @return the element */
	T get(int index);
	
	/** Set the element at this index. 
	 * May make the array longer.
	 * May substitute the element that was 
	 * previously at this index.
	 * 
	 * (The method could have been called "put".) 
	 * @param index the index
	 * @param element the element 
	 * */
	void set(int index, T element);
	
	/** Deletes the element at this index. 
	 * 
	 * (The method could have been called "remove".)
	 * 
	 * @param index the index */
	void delete(int index);
	
	/** What is the length of the instantiated array?
	 * That is, at what indexes can we expect there to be 
	 * elements? 
	 * @return the length */
	int getLength();
}
