/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.dom;

import java.io.Serializable;

/** <a href="http://www.w3.org/TR/dom/#domerror">spec</a> */
public class DOMError extends RuntimeException implements Serializable{
	private static final long serialVersionUID = -5413655344142287252L;

	// state -----------------------------------------------
	
	private String name;
	
	// construction -----------------------------------------
	
	public DOMError(){}

	public DOMError(String message){
		super(message);
	}
	
	public DOMError(String name, String message){
		super(message);
		this.name=name;
	}
	
	// accessors --------------------------------------------
	
	public String getName(){return name;}
	public void setName(String n){name=n;}

}
