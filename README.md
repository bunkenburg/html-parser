# html-parser

Parser that returns HTML5 objects.

Similar to nu.validator.htmlparser (and based on it), but returns objects
that implement the html5 interfaces like HTMLDocument, HTMLSelectElement, and so on.

## Use

	DocumentBuilder builder=new HTMLDocumentBuilder();
			
	HTMLDocument document;
			
	document=builder.newDocument();
	document=builder.parse(String url)
	document=builder.parse(InputSource)
	document=builder.parse(InputStream)
	document=builder.parse(InputStream, encoding)
	document=builder.parse(File)

## Details

The object-model is the DOM, as in package org.w3c.dom. 
The interfaces implement the interfaces in cat.inspiracio.html,
which aim to implement the HTML5 specification, at least as far
as the object tree is concerned, leaving events (like blur() and focus()) aside.

## versions

### 0.0.5

Fixes https://bitbucket.org/bunkenburg/html-parser/issues/2