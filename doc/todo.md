# To-do html-parser

## Tasks

* stop ignoring cdata sections

Optimisation:

* opt: DocumentWriter.escapeElement: let it write directly to output rather than StringBuilder

## Versions

### 0.0.4

* Register custom elements.
* DocumentRecurser.node becomes public.
* HTMLBuilder can parse just HTMLElement, Text, or HTMLDocumentFragment.

### 0.0.3

* protected DocumentWriter.writeln(String s) and writeln()

### 0.0.2

* DocumentRecurser and DocumentWriter: reusable document traversal
* Setting input.value of an element to null or "" is removing it.

### 0.0.1

* cat.inspiracio.script.ScriptMap for array notation and _delete_ operator
* Make compatible with Nashorn in Java 8
* AudioTrack, VideoTrack, AudioTrackList, VideoTrackList
* HTMLAnchorElementImpl implements URLUtils
* HTMLAreaElementImpl implements URLUtils
* HTMLDocument.location implements URLUtils
* HTMLElement.getAccessKeyLabel() always returns "".
* HTMLInputElement.valueAsDate
* HTMLInputElement.valueAsNumber
* HTMLInputElement stepping

### 0.0.0

## Not yet implemented

* HTMLLinkElement.getSheet()
* HTMLStyleElement.getSheet()

## Unimplemented: NotImplementedException. Doesn't make sense on server.

* Events
	HTMLDocument.readyState : EventHandler

* Files
	HTMLInputElement.getFiles()
	Blob
	File extends Blob

* HTMLCanvasElement
	toBlob(FileCallback)
	toDataURL()

* Form validation:
	HTMLFormElement.checkValidity()
	
	HTMLButtonElement
	HTMLFieldSetElement
	HTMLInputElement
	HTMLKeygenElement
	HTMLObjectElement
	HTMLOutputElement
	HTMLSelectElement
	HTMLTextAreaElement
	
	boolean getWillValidate();
	ValidityState getValidity();
	String getValidationMessage();
	boolean checkValidity();
	void setCustomValidity(String error);

* HTMLDocument Dynamic Markup Insertion.
	open()
	write()
	writeln()
	close()

* Navigation
	* Location assign replace reload
	* HTMLDocument.open(String url, String name, String features, boolean replace).
		Opens a fresh window or navigates an existing window.
	
* HTMLDocument User Interaction
	getDefaultView
	getActiveElement
	hasFocus()
	designMode
	execCommand()
	queryCommand()
	
* HTMLElement User interaction
	click()
	focus()
	blur()
	contentEditable
	spellcheck

* HTMLIFrameElement
	getSandbox
	getContentDocument
	gitContentWindow
	
* HTMLImageElement
	getNaturalHeight
	getNaturalWidth
	getComplete

* Selection
	HTMLInputElement
	HTMLTextAreaElement
	
* HTMLObjectElement
	getContentWindow()
	getContentDocument()

* Media	
	HTMLMediaElement: many methods
	HTMLTrackElement getReadyState() getTrack()
	HTMLVideoElement getVideoWidth getVideoHeight
	MediaController many methods
	EventTarget (VideoTrackList and TextTrackList extend EventTarget. 
	HTMLMediaElement has getTextTracks() and getVideoTracks())
	EventHandler (Media tracks also use it.)
