# Design

## Unit tests

I don't make unit tests for attribute accessors.
There are too many of them, and they are not interesting.


## Using native Java types

* long --> Java's int
* DOMString --> java.lang.String
* FileList --> List<File>. See http://www.w3.org/TR/FileAPI/#dfn-filelist


## Host objects in javascript

* Document extends HTMLDocumentImpl
* Image
* Audio


## Adoption

I've overwritten these methods

* public Node insertBefore(Node newChild, Node refChild) throws DOMException
* public Node appendChild(Node newChild) throws DOMException

in HTMLDocumentImpl and HTMLElementImpl
so that they always first adopt the node (calling ownerDocument.adoptNode(node))
and then insert it. This is so that scripts like

	i=new Image()
	element.appendChild(i)

can work. Otherwise, the new Image element would have a different owner document
and could not be inserted into element's owner document.


## Cloning

In any class that wraps the real instance or has additional state, we must override 

	Node cloneNode(boolean deep).
	
In other classes, we do not need to override it.
In particular, we do not need to override it just to get the right return type:
at the bottom of its implementation, there is a clone().


## Javascript array notation

Some html elements admit javascript array notation.
For example the select-element has:

	select[index] = option
	option = select[index]
	
Q. How can I write some methods in Java so that this notation
works?

A. It depends on the script engine. If the script engine is 
the Rhino that is built in to Java 7. then you can 
implement sun.org.mozilla.javascript.Scriptable.
	
This approach makes it possible, but has disadvantages:

* Forbidden dependency on internal classes of the script engine. Can that be avoided? The standard javascript engine in Java 8 will be different, and I want to use old Rhino in other programs.
* Must implement methods for correct javascript functioning that are irrelevant: getClassName extract prototype parentScope getIds getDefaultValue hasInstance. Maybe some of them can be pushed into super-class HTMLElementImp.

See cat.inspiracio.script.ScriptMap.
Also consider many DOM objects that have methods like item(n).


## Javascript _delete_ operator on DataSet

The "data-" attributes of an HTML element can be returned as a _DataSet_ object. It should be possible to delete an attribute with javascript's _delete_ operator, like so:

	e=...
	e.set("data-foo", 9)
	delete e.dataset.for	//does nothing
	
In order to implement this, I would have to rely on details of Rhino or
Nashorn implementation, like interface org.mozilla.javascript.Scriptable.

Similar problem to javascript array notation.

See cat.inspiracio.script.ScriptMap.


## Thread-safety

The classes are not thread-safe. I will access them with just one thread. On the server, multi-threading doesn't seem relevant here.


## Methods that are not implemented

They throw NotImplementedExcepton.

Some of them are methods that are not relevant on the server, like events.
Or they may be methods that I have not implemented yet, but that could be implemented.


## Element creation

* HTMLDocumentBuilder ---the API for the client programmer to build HTMLDocument instances from HTML5 texts.
  - Call register(class, tags...) to register a class for a tag.

* HTMLDOMImplementation ---operations independent of Document instance: getFeature, hasFeature, createDocumentType, createDocument
  - Call register(element, tags) to register a class for a tag.
  - has element creator. The element creator is part of the builder implementation, not part of a document instance. It will not be different for each document instance.

* HTMLDocumentImp
  - createElement(X extends HTMLElement): X  --> createElement(String tag)
  - createElement(String tag) --> ElementFactory.create(HTMLDocumentImp, tag)
  - createElementNS(_, tag) --> createElement(tag) We don't use this.
  - createElementNS(namespaceURI, qualifiedName, localPart): not implemented

* ElementCreator
  - Element create(d, tags)

* HTMLDocument ---the root node, accessors, factory methods
  - The factory methods need the registered custom element.

	class ElementCreator{
		HTMLElement create(HTMLDocumentImp d, String tag) throws DOMException;
	}
		
For a class that represents only one tag, for example HTMLAnchorElement:

	builder.register(HTMLAnchorElementImpl.class)		//if getTagName(){return "a";}
	builder.register(HTMLAnchorElementImpl.class, "a")	//no guessing of tag name by reflection

For a class that represents several  tags, for example HTMLMediaElement:

	builder.register(HTMLMediaElementImpl.class, "audio", "video")	//register it explicitly for several tags
	
Should it be possible to override the standard implementation? Yes.


## Element registration within javascript

How can I register a custom element class for this program?

		String program=""
				+ "d=new Document();"
				+ "e=d.createElement('fulton');"
				+ "e.id='e';"
				+ "d.body.appendChild(e);"
				+ "d";
		Object o=eval(program);
		
Instantiation here starts from:

* new Audio()
* new Document()
* new Image()
* new Option()

which all go back to new Document() which calls Document.fabricateImplemention().

Its comment is:

	The zero-argument constructor needs a DOM implementation ---
	where should it come from? Need to think about the design more.
	Here, just fabricates a fresh implementation (no custom elements). 
	Maybe it could be instantiated and configured outside somehow
	and this method brings it in. Maybe a thread local variable?

Keep in mind the program could be:

	var d=new Document()
	//
	var i=d.getImplementation()
	HTMLFultonElementImp=Packages.issues.issue.HTMLFultonElementImp
	i.register(HTMLFultonElementImp.class, 'fulton')
	//
	var e=d.createElement('fulton')
	e.id='e'
	d.body.appendChild(e)

Now e really is an instance of HTMLFultonElementImp.


## Parsing elements, texts, fragments

It would be useful if the parser could also parse a file that contains just
an HTML element, or a list of elements.
For example, if the file contains just

	<div>kuku</div>
	
it would be useful to have code like this:

	HTMLBuilder builder=new HTMLBuilder();
	Node n=builder.parse(file);
	//n : Element

That is what the class HTMLBuild does. It is a superclass of HTMLDocumentBuilder with method:

	Node parse(String s)

If the string is a complete document (with or without doctype, but starting with <html>), then it returns an HTMLDocument. If the string is a complete element (other than <html>), then it returns an HTMLElement. If the string is just text, then it returns Text. Otherwise, it returns a DocumentFragment with a series of texts and elements.